---
title: Industry 
subtitle: White Paper
date: 2022-03-01
section: whitepaper
comments: false
---

# Industry - White Paper


## Contents

- [Executive summary](#executive-summary)    
- [Background](#background)
- [Topic 1](#topic-1)
  - [Topic 1.1](#topic-1-1)
- [Conclusions](#conclusions)

---

## Executive Summary


## Background


## Topic 1
A hot topic/pain point (e.g. "The Sustainable Future of Apps" or "Apps, Personal Data and Sovereignty") 

### Topic 1.1


## Conclusions

