# OW2 - W3C memorandum of understanding support note

OW2 Quick App Initiative, @@@ Feb 2023

This note has been authored to support the creation of a _memorandum of understanding_ (**MOU**) to establish a [simple liaison](https://www.w3.org/2001/11/StdLiaison#simple) between the World Wide Web Consortium (**W3C**) based in Cambridge (MA), United States and the Object Web 2 (**OW2**) Consortium, based in Paris, France.

W3C uses the term liaison to coordinate activities with other organisations, which is not meant to substitute for W3C or OW2 membership.

## Context

### About OW2

[OW2](https://www.ow2.org) is an independent EU-based non-profit open source association. It promotes professional open source, hosts projects, and fosters a global community. OW2 is open to companies, public organizations, academia and individuals.

[OW2](https://www.ow2.org) has an open and transparent governance, with an elected Board of Directors acting as volunteers, and public records of all decisions.

Active for more than 15 years, [OW2](https://www.ow2.org) also collaborates with a rich network of open source associations in Europe and beyond, and is a recognized social and political supporter of open source, open science and open standards.

### About the OW2 Quick App Initiative (QAI)

In Q1 2021 Huawei gathered a group of interested parties excited by the idea of creating a forum to explore, promote and encourage a Quick App multi-party ecosystem in Europe.

> Quick Apps are an implementation subset of emerging W3C MiniApps specifications.

Whilst no open source code was available, it was clear that the creation of such a forum should be done within an independent open source organisation so as to best enable open dialogue and collaboration. [OW2](https://www.ow2.org) was selected as the most appropriate open source organisation, not least because it was widely respected, was based in Europe, and allowed the creation of _initiatives_ (forums) in advance of any open source code being submitted to the OW2 code base.

During Q1-Q2 2021 the group of interested parties (the _launch participants_) authored the [working charter](https://quick-app-initiative.ow2.io/docs/charter.pdf) for the [Quick App Initiative](https://quick-app-initiative.ow2.io/), as well as a [promotional whitepaper](https://quick-app-initiative.ow2.io/docs/Quick_App_White_Paper.pdf) (which also had additional co-authors). The initiative was validated by the OW2 board and then launched, along with the whitepaper, during the OW2 annual conference of June, 2021.

The launch participants of QAI were (alphabetical order): Alliance Tech, CTIC, Famobi, FRVR, Huawei, RSI Foundation, Santillana.

The initiative was launched with, and maintains, 4 key *collaborative* ambitions:
1. Raise awareness of Quick Apps with the wider developer community in Europe (and elsewhere), to businesses, and to end-users.
2. Share experiences, best practices, and even (open source) code examples of Quick Apps.
3. Build tools to help developers, publishers and device makers create, deploy and run Quick Apps.
4. Support the W3C MiniApp specifications process through _grassroots_ driven operational input and eventually even reference implementations and support tools as outputs.

It is in this context that we believe that an MOU between the [W3C](https://www.w3.org) and [OW2](https://www.ow2.org) makes natural sense.

## Scope of the liaison between W3C MiniApps Working Group and OW2 QAI

<!-- Christian Comment: is the word "create" good, or is there a better word (like "formalise")? -->

Since [W3C MiniApps Working Group](https://www.w3.org/2021/miniapps/) (WG) activities are focused on technical standardization, and [OW2 QAI](https://quick-app-initiative.ow2.io/) is focused on development and outreach, no overlap of standardization activities is expected. Both groups are considered complementary, and their scope of action clearly defined to avoid overlap.

Whilst the liaison between these groups does not commit any resource or action, the following list reflects some of the objectives and activities that could be considered within the scope of this collaboration.

- General co-promotion of each other’s activities.
- Opportunities for participation in outreach events related to MiniApp topics (e.g., co-organization of joint technical events, session for MiniApps at the OW2 annual conference, etc.).
- [OW2 QAI](https://quick-app-initiative.ow2.io/) support of the MiniApp standard ecosystem through promotional activities (i.e., events, publications, media, etc.) fostering the use of the standards defined by [W3C](https://www.w3.org).
- Collaboration, diffusion, and promotion of open source code projects related to MiniApp specifications. For instance:
  - Code examples & templates;
  - Developer documentation (under open licenses);
  - Tools to support app development, simulation, verification, and release.
  - Reference implementations for [W3C MiniApps WG specifications](https://www.w3.org/2021/miniapps/#specs).
  - Developing tests of implementation alignment to specifications.
- [OW2 QAI](https://quick-app-initiative.ow2.io/) provision of feedback, ideas and suggestions to the [W3C MiniApps WG](https://www.w3.org/2021/miniapps/) with the aim to improve the quality and usability of the standards.
- [W3C MiniApps WG](https://www.w3.org/2021/miniapps/#specs) availability to provide ad hoc technical advice if/as requested by [OW2 QAI](https://quick-app-initiative.ow2.io/).
