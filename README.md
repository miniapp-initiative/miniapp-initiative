# Quick App Initiative (QAI) Repository

> Welcome to the OW2 Quick App Initiative's main repository. 

Here you will find public documents, code and other resources made by the [Quick App Initiative](https://quick-app-initiative.ow2.io/) participants.

## Get Started

This initiative is open to any participant. So, feel free to browse the website and clone the repository. In case you want to join us, please follow these steps:

1. [Create your OW2 personal account](https://www.ow2.org/view/services/registration)
2. [Subscribe the mailing list](https://mail.ow2.org/wws/info/quickapp) and introduce yourself there.
3. The admin team will give you permissions to use all the [working tools](#working-tools).

### Working Tools

* [Chat](https://rocketchat.ow2.org/group/QAI-Town-Square/) (RocketChat).
* [Repository](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/) (GitLab).
* [Mailing List](https://mail.ow2.org/wws/info/quickapp) (Sympa).

---

Many thanks for your contributions (big or small). Know that they are valued.

