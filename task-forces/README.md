# Task Forces

Files relating to the Quick App Initiative's task forces.
As defined in the [Charter](charter/charter.md#task-forces)

"Activities of the initiative (e.g., outreach and promotion, test-beds, standardization process, etc.) will be performed within specific Task Forces that have concrete objectives, scopes and timelines and deliverables.

All Task Forces must be submitted for prior validation and approval by the [Steering Committee](charter/charter.md#steering-committee)"
