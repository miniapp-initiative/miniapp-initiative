---
home: true
heroImage: /images/quick-app.png
tagline: Quick Start and Reference Guide 
actionText: Quick Start →
actionLink: /guide/
features:
- title: Easy Coding
  details: Quick App is a Model-view-viewmodel (MVVM) framework with a declarative user interface and predefined components to design reactive apps using HTML-based markup language, CSS and JavaScript.
- title: Light and Fast
  details: Quick Apps are light (<4MB) and have optimized access to the native features of the device, using multiple computation threads (rendering and logic engines) to maximize efficiency during user interaction.
- title: Native Services
  details: Quick App makes the most of the user's device through direct access to native services such as push notifications, storage, calendar, and contacts whilst protecting the user's security and data privacy. 
footer: CC BY 4.0 - OW2 Quick App Initiative
---
