# `camera`

Uses the camera of the device.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/camera01.gif" alt="Camera management on a Quick App" /> 

([Example code](#example))

::: tip Note
You only can add one `camera` element in a page.
::: 

[[toc]]

## Children Elements

This element doesn't allow children elements.

## Attributes


In addition to the [common attributes](./attributes), this element may contain the following attributes.

- [`deviceposition`](#deviceposition)
- [`flash`](#flash)


### `deviceposition`

Attribute that indicates the camera to use (front or rear).

- Type: `string` (`front` | `back`)
- Default value: `back` 
- Mandatory: no 

### `flash`

Enables or disables the flash.

- Type: `string` (`auto` | `on` | `off` | `torch`)
- Default value: `auto` 
- Mandatory: no 


## CSS Properties

This element supports the [common styles](./styles).

## Events

In addition to the [common events](./events), this element supports the following event:

- [`error`](#error)

### `error` 

This event occurs when a user rejects the permissions request to use the camera on the device.


## Methods

This element has the following method:

### `takePhoto({ quality, cropimage, success, fail, complete})`

Takes a photo using the device's camera.

__Parameters__:
- `quality: string`. Image quality (`high` | `normal` (default) | `low`).
- `cropimage: boolean`. Crop the image to the container size (`false` by default).
- `success: function(res)`. Optional callback function corresponding to the successful execution.
  - `res.uri`	(`string`) with the path of the photo taken.
- `fail: function(res, code)`). Optional callback function corresponding to the failed execution.
  - `201`	when a user rejects the request for the camera permission.
- `complete` (`function()`). Optional callback function corresponding to the end of the execution.

## Example


``` html
<template>
  <div class="container">
    <div class="case-title mt-item">
      <text class="title">camera</text>
    </div>
    <div class="mlr-container">
      <camera class="camera" id="camera" flash="{{flash}}" deviceposition="{{deviceposition}}"></camera>
    </div>
    <list class="mlr-container btn-list">
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="switchCamera">Switch cameras</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="takePhoto">Take a photo</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="openTorch">Enable flash</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="closeFlash">Disable flash</text>
      </list-item>
    </list>
    <div class="case-title">
      <text class="title">Photo preview</text>
    </div>
    <div class="item-container">
      <image class="preview-image" src="{{photoUrl}}" alt="previrew"></image>
    </div>
  </div>
</template>

<style lang="sass">
  .camera {
    width: 100%;
    height: 400px;
  }
  .btn-list {
    columns: 2;
    height: 280px;
    .btn-item {
      height: 140px;
      align-items: center;
      justify-content: center;
    }
  }
  .preview-image{
    width: 100%;
    height: 400px;
  }
</style>

<script>
  const devicePositionMap = {
    back: "back",
    front: "front"
  };
  const flashMap = {
    off: "off",
    auto: "auto",
    torch: "torch",
  }
  module.exports = {
    public: {
      deviceposition: devicePositionMap.back,
      photoUrl: "",
      flash: flashMap.auto,
    },
    takePhoto(){
      let that = this;
      this.$element('camera').takePhoto({
        quality: 'normal',
        success: function (data) {
          that.photoUrl = data.uri;
        },
      })
    },
    switchCamera(){
      if(this.deviceposition === devicePositionMap.back)
      {
        this.deviceposition = devicePositionMap.front;
      }else{
        this.deviceposition = devicePositionMap.back;
      }
    },
    openTorch(){
      this.flash = flashMap.torch;
    },
    closeFlash(){
      this.flash = flashMap.off;
    }
  }
</script>
```
