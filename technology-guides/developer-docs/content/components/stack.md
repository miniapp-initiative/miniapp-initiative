# `stack`

Basic container that supports sub-elements that are successively visually presented on a stack.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/stack01.jpg" alt="Stack blocks on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

This element may support any children elements.

## Attributes

This element may contain the [common attributes](./attributes).

## CSS Properties

This element supports the [common styles](./styles).  

## Events

In addition to the [common events](./events), this element supports the following event:

- [`fullscreenchange`](#fullscreenchange)

### `fullscreenchange` 

The app changes the status of the [full-screen mode](../guide/manifest.html#fullscreen).

__Additional parameters__: 
- `{ fullscreen: string }`


## Methods

This element has the following method:

- [`requestFullscreen({screenOrientation})`](#requestfullscreen-screenorientation)

### `requestFullscreen({screenOrientation})`

Requests to enter the [full-screen mode](../guide/manifest.html#fullscreen).

__Parameters__:
- `object` with the following attributes:
  - `screenOrientation: string`. Orientation of the screen (`portrait` or `landscape`).

## Example

``` html
<template>
      <div class="container">
      <stack class="mt-item stack mlr-container">
        <image class="bro-l stack-img" src="/Common/img/quickapp_transparent.png" alt="stackImage"></image>
        <div class="top-container column-center">
          <div class="top">
            <div class="top-item bg-red"></div>
            <div class="top-item bg-gray"></div>
            <div class="top-item bg-green"></div>
          </div>
        </div>
      </stack>
    </div>
</template>

<style lang="sass">
  @import '../../../Common/css/common.scss';
  .stack{
    height: 700px;
    .stack-img{
      width: 100%;
      height: 100%;
    }
    .top-container{
      .top{
        margin-left: 12px;
        width: 500px;
        height: 200px;
      }
      .top-item{
        flex: 1;
        height: 100%;
        opacity: 0.9;
      }
    }
  }
</style>

```