# `switch`

Form input acting as a toggle switch.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/switch01.jpg" alt="Switch components on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

This element doesn't support children elements.

## Attributes

In addition to the [common attributes](./attributes), this element may contain the following attribute.

- [`checked`](#checked)


### `checked`

This attribute indicates the status of the switch (checked or not). The CSS `:checked` pseudo-class is related with the value of this attribute.

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no 


## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling properties:

- [`thumb-color`](#thumb-color)
- [`track-color`](#track-color)


This element supports the [`:active` pseudo-class](../guide/styling.html#css-selectors).

### `thumb-color` 

Color of the switch button. 

- Type: `<color>`  
- Default value: -
- Mandatory: no 

### `track-color` 

Color of the track of the switch (background). 

- Type: `<color>`  
- Default value: -
- Mandatory: no 

## Events

This element supports the [common events](./events) and the following event:

- [`change`](#change)


### `change` 

This event is triggered when the current value of a switch changes. 

__Additional parameters__: 
- `{ checked: boolean }`

## Methods

This element does not have additional methods.

## Example

``` html
<template>
  <div class="container">
    <div class="case-title mt-item">
      <text class="title">Recommended display style</text>
    </div>
    <div class="info-container">
      <div class="info-item border-bottom-1">
        <label class="title">on</label>
        <div class="result">
          <switch checked="true"></switch>
        </div>
      </div>
      <div class="info-item">
        <label class="title">off</label>
        <div class="result">
          <switch></switch>
        </div>
      </div>
    </div>
  </div>
</template>
```