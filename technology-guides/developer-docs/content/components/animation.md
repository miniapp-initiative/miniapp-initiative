# animations

Content coming soon...

Visit our [repository](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative) and [raise an issue](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative) if you want to contribute. Thanks you for your collaboration

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/animation01.gif" alt="Animations on a Quick App" /> 

([Example code](#example))



## Event Binding 

You can bind an event handler to control the animations using the `addEventListener` method. This method is implemented by all the [elements](../components).

This method has two arguments: 
- `type` (`string`): Indicating the type of the event to listen: [`animationstart`](#animationstart), [`animationiteration`](#animationiteration), or [`animationend`](#animationend).
- `callback` (`function`): The callback function, run after the event is triggered.

::: tip
To have a successful result, you must declare `addEventListener` and, dynamically modify the `class` to trigger the CSS-based animation effect.
:::

Example:

``` js
let element = $element("id")
 
element.addEventListener("animationstart", (event) => { 
    prompt.showToast({ 
        message: 'type: ' + event.type + ', animationName: ' + event.animationName + ', elapsedTime: ' + event.elapsedTime 
    }) 
}) 
element.addEventListener("animationend", (event) => { 
    prompt.showToast({ 
        message: 'type: ' + event.type + ', animationName: ' + event.animationName + ', elapsedTime: ' + event.elapsedTime 
    }) 
}) 
element.addEventListener("animationiteration", (event) => { 
    prompt.showToast({ 
        message: 'type: ' + event.type + ', animationName: ' + event.animationName + ', elapsedTime: ' + event.elapsedTime 
    }) 
})
```


## Example

``` html
<template>
  <div class="container">
    <div class="item-container">
      <div class="animation-container row-center column-center">
        <div class="show {{animation}}"></div>
      </div>
    </div>
    <div class="mlr-container mt-item row-center">
      <input type="button" value="show" class="btn-blue" onclick="showAnimation" />
    </div>
  </div> 
</template>

<style lang="sass">
  .animation-container {
    height: 400px;
  }
  .show {
    width: 50px;
    height: 200px;
    background-color: green;
  }
  .animation {
    animation-name: Color, ang, size;
    animation-duration: 6000ms;
  }
  @keyframes Color {
    0% {
      background-color: green;
    }
    30% {
      background-color: red;
    }
  }
  @keyframes ang {
    0% {
      transform: rotate(0deg);
    }
    30% {
      transform: rotate(0deg);
    }
    60% {
      transform: rotate(-90deg);
    }
  }
  @keyframes size {
    0% {
      transform: scale(1);
    }
    65% {
      transform: scale(1);
    }
    100% {
      transform: scale(2);
    }
  }
</style>

<script>
  module.exports = {
    public: {
      animation: ""
    },
    showAnimation() {
      this.animation = "";
      this.animation = "animation";
    }
  }
</script>