# `select`

Form control enabling users to select items in a list of [options](./option).

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/select01.gif" alt="Select component on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

This element only supports [`option` elements](./option) as children nodes.

## Attributes

This element supports the [common attributes](./attributes).


## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling properties:

- [`color`](#color)
- [`font-size`](#font-size)
- [`font-weight`](#font-weight)
- [`text-decoration`](#text-decoration)
- [`font-style`](#font-style)
- [`font-family`](#font-family)

### `color`	

Color of the font.

- Type: `<color>`  
- Default value: (system)
- Mandatory: no 

### `font-size`	

Font size.

- Type: `<length>`  
- Default value: (system) 
- Mandatory: no 

### `font-weight`	

Weight of the font.

- Type: `string` (`lighter` | `100` | `200` | `300` | `400` | `500` | `600` | `700` | `800` | `900` | `normal` | `bold` | `bolder`)  
- Default value: `normal` 
- Mandatory: no 


### `text-decoration`	

Decoration options to the text.

- Type: `string` (`underline` | `line-through` | `none`)  
- Default value: `none` 
- Mandatory: no 

### `font-style`

Font style selected from the [font family](#font-family).

- Type: `string` (`italic` |	`normal`)  
- Default value: `normal` 
- Mandatory: no 


### `font-family`

Font family of for the text.

- Type: `string`  
- Default value: -
- Mandatory: no 

To customize fonts, please refer [font-face style](styles.html#font-face).


## Events

This element support the [common events](./events), with the exception of [`click`](./events#click), [`longpress`](./events#longpress), and [`swipe`](./events#swipe). Apart from this, the element supports the following event:

- [`change`](#change)

### `change` 

This event is triggered when the selected value in the list changes.

__Additional parameters__: 
- `{ newValue: string }`. A string with the new value in the selection. 


## Methods

This element does not have additional methods.

## Example

``` html
<template>
  <div class="container">
    <div class="case-title mt-item">
      <text class="title">What is your favourite fruit?</text>
    </div>
    <div class="mlr-container select-container bro-s">
      <select class="select">
        <option value="item1">Orange</option>
        <option value="item2" selected="true">Banana</option>
        <option value="item3">Lemon</option>
        <option value="item4">Strawberry</option>
        <option value="item5">Blueberry</option>
      </select>
    </div>
  </div>
</template>

<style lang="sass">
  .select-container{
    background-color: #fff;
    padding-right: 14px;
  }
  .select{
    width: 100%;
    height: 100px;
  }
</style>
```