# Common Events

All the [essential elements](./) implement the following events that can be [handled](../guide/events.html#event-listeners) by app components.

::: tip
Read more about [event binding and events handlers](../guide/events). 
:::

[[toc]]

## Base Events

This interface is the event interface implemented by all the events supported by the quick app elements.

This interface has the following attributes:
- `type` (`string`): Indicates the event type (e.g., `click`, `touchstart`, `touchcancel`,...).
- `timeStamp` (`integer`): Timestamp when an event is triggered.
- `target` (`object`): The element that triggers an event. The object contains the following attributes:
  - `id` (`string`): Element's identifier or empty if the `id` attribute is not set.
  - `type` (`string`):	Element's tag name.
- `currentTarget` (`object`): Information about the current element.
  - `id` (`string`): Element's identifier or empty if the `id` attribute is not set.
  - `type` (`string`):	Element's tag name.


### `click` 

- Interface: [`Base Event`](#base-event)

The event is triggered when a user taps or interact with a pointer on an element.

### `longpress`

- Type: `longpress`
- Interface: [`Base Event`](#base-event)

The event is triggered when a user performs a long-press action.


### `focus`

- Type: `focus`
- Interface: [`Base Event`](#base-event)

The event is triggered when an element gets the focus. 

### `blur`

- Type: `blur`
- Interface: [`Base Event`](#base-event)

The event is triggered when an element loses the focus. 

### `appear`

- Type: `appear`
- Interface: [`Base Event`](#base-event)

The event is triggered when an element is shown on the screen during user interaction.

### `disappear`

- Type: `disappear`
- Interface: [`Base Event`](#base-event)

The event is triggered when an element disappears during user interaction.

### `swipe`

- Type: `swipe`
- Interface: [`Base Event`](#base-event)

The event is triggered when	the user performs a swiping movement on an element. This event is not supported when other touch or gesture events are performed on an element.  

Apart from the attributes of the [`Base Event` interface](#base-event), this type of event includes another attribute: 
- `string` that indicates the swipe direction. Values can be: 
  - `left`: swipe left
  - `right`: swipe right
  - `up`: swipe up (this value only applies to the [`list`](./list) element)
  - `down`: swipe down (this value only applies to the [`list`](./list) element)

### `resize`

- Type: `resize`
- Interface: [`Base Event`](#base-event)

This event is triggered with the size of an element changes.

Apart from the attributes of the [`Base Event` interface](#base-event), this type of event includes the following attributes with information about the new size and the position of an element relative to its parent element:
  - `offsetWidth` (`integer`)
  - `offsetHeight` (`integer`)
  - `offsetLeft` (`integer`)
  - `offsetTop` (`integer`)

## Animation Events

This interface is the event interface implemented by the events caused by CSS animations.

This extends the [`Base Event`](#base-event) interface. So, apart from the basic attributes, this interface defines the following attributes:
- `animationName` (`string`): contains the name of the animation. 
- `elapsedTime` (`decimal`): indicates the duration (in seconds) of the animation when an event occurs. 

There are four event types that implement this interface:

- [`animationstart`](#animationstart)
- [`animationiteration`](#animationiteration)
- [`animationend`](#animationend)

### `animationstart`

- Type: `animationstart`
- Interface: [`Animation Event`](#animation-events)

Event triggered with a CSS-based animation starts on an element.

### `animationiteration`

- Type: `animationiteration`
- Interface: [`Animation Event`](#animation-events)

Event triggered with a CSS-based animation restarts on an element.

### `animationend`

- Type: `animationend`
- Interface: [`Animation Event`](#animation-events)

Event triggered with a CSS-based animation ends.

## Touch Events

This interface is the event interface implemented by the events triggered after pointer or gesture interactions.

This extends the [`BaseEvent` interface](#interface-baseevent). So, apart from the basic attributes, this interface defines the following attributes:
- `touches` (`array<object>`): contains information about the touch points that are currently in contact with the pointer surface.
- `changedTouches` (`array<object>`): contains information about the touch changes. 

The `object`s that indicate the touch points have the following attributes:
- `pageX` (`number`): Distance from the upper left corner of the document on the horizontal axis.
- `pageY` (`number`): Distance from the upper left corner of the document on the vertical axis.
- `clientX` (`number`): Distance from the upper left corner of the display area on the page (excluding the navigation bar on the screen) on the horizontal axis.
- `clientY` (`number`):	Distance from the upper left corner of the display area on the page (excluding the navigation bar on the screen) on the vertical axis.
- `offsetX` (`number`):	Distance from the upper left corner of the active (touched) element on the horizontal axis.
- `offsetY` (`number`):	Distance from the upper left corner of the active (touched) element on the vertical axis.

There are four event types that implement this interface:

- [`touchstart`](#touchstart)
- [`touchmove`](#touchmove)
- [`touchcancel`](#touchcancel)
- [`touchend`](#touchend)

### `touchstart`

- Type: `touchstart`
- Interface: [`Touch Event`](#touch-events).

Event triggered with a pointer interaction starts (e.g., a user touches the element on the screen). 

In this event, the `changedTouches` attribute contains a list of the touch points that become active with the event. 

### `touchmove`

- Type: `touchmove`
- Interface: [`Touch Event`](#touch-events). 

Event triggered with a pointer interaction continues and the pointer is moved on the screen.

In this event, the `changedTouches` attribute contains a list of the touch points that have changed since the last event. 

### `touchcancel`

- Type: `touchcancel`
- Interface: [`Touch Event`](#touch-events).

Event triggered with a pointer interaction is canceled (e.g., an incoming call or a pop-up dialog is shown).

In this event, `touches` is an empty array. `changedTouches` contains information about the touch changes (the points released by the user). 

### `touchend`

- Type: `touchend`
- Interface: [`Touch Event`](#touch-events).

Event triggered with a pointer interaction finishes (e.g., a user stops touching the screen).

In this event, `touches` is an empty array. `changedTouches` contains information about the touch changes (the points released after the cancellation). 



## Example

``` html
<template> 
  <div class="container" ontouchstart="onTouchStart" id="mainContainer"> 
    <text>{{text}}</text> 
  </div> 
</template>
<script>
  export default {
    private: { 
      text: 'my text'
    },       
    onTouchStart(e) {
      console.log(e)
    } 
  }
</script>
```

After touching the container, the console output is: 

``` json
{
  "touches": [
    {
      "offsetX": 47,
      "identifier": 0,
      "offsetY": 20,
      "clientY": 717,
      "clientX": 47,
      "pageY": 717,
      "pageX": 47
    }
  ],
  "changedTouches": [
    {
      "offsetX": 47,
      "identifier": 0,
      "offsetY": 20,
      "clientY": 717,
      "clientX": 47,
      "pageY": 717,
      "pageX": 47
    }
  ],
  "type": "touchstart",
  "target": {
    "ref": "4",
    "type": "text",
    "attr": {
      "value": "my text"
    },
    "style": {
      "animationName": null,
      "fontFamily": []
    }
  },
  "currentTarget": {
    "ref": "_root",
    "type": "div",
    "attr": {
      "id": "mainContainer"
    },
    "style": {
      "flexDirection": "column",
      "justifyContent": "center",
      "animationName": null,
      "fontFamily": []
    },
    "event": [
      "viewappear",
      "viewdisappear",
      "onConfigurationChanged",
      "viewsizechanged",
      "touchstart",
      "_mediaquery_show"
    ],
    "children": [
      {
        "ref": "4",
        "type": "text",
        "attr": {
          "value": "my text"
        },
        "style": {
          "animationName": null,
          "fontFamily": []
        }
      }
    ],
    "id": "mainContainer"
  },
  "timeStamp": 1634209764701
}
```