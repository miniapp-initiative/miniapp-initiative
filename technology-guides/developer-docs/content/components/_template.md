
<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/list01.gif" alt="List on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

Children elements.

## Attributes


In addition to the [common attributes](./attributes), this element may contain the following attributes.

- [`attribute`](#attribute)

### `attribute`

Description.

- Type: `type` 
- Default value: `value` 
- Mandatory: yes/no 

Description


## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling properties:

- [`property`](#property)


### `property` 

Description 

- Type: `type`  
- Default value: `value` 
- Mandatory: yes/no 


## Events

In addition to the [common events](./events), this element supports the following events:

- [`eventname`](#eventname)


### `eventname` 

Description of the event. 

__Additional parameters__: 
- `{ param1: type, param2: type, param3: type }`


## Methods

This element has the following methods:

### `method`

Description of the method.

__Parameters__:
- `parameter: type` (mandatory). Description (`value` by default).
- `parameter: type` (mandatory). Description (`value` by default).

## Example
``` html
Code
```

