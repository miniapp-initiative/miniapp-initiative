# `image`

Display images.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/image01.jpg" alt="Images on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

This element doesn't support children elements.

## Attributes

In addition to the [common attributes](./attributes), this element may contain the following attributes.

- [`src`](#src)
- [`alt`](#alt)


### `src`

Source of the image. This attribute specifies the URI that could be a local or external resource. The platform supports _PNG_, _JPG_, _SVG_ and _GIF_ formats


- Type: `URI` 
- Default value: - 
- Mandatory: yes 

::: tip
The `border-radius` CSS property cannot be applied to SVG and GIF images.
You can use Base64 encoded images on the `src` attribute (e.g, `data:image/png;base64,iVBORw0K...`).
:::

### `alt`

Placeholder image displayed during the loading phase. Only local image resources are supported.

- Type: `type` 
- Default value: `value` 
- Mandatory: yes/no 


## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling property:

- [`object-fit`](#object-fit)

This element supports the [`:active` pseudo-class](../guide/styling.html#css-selectors).

### `object-fit` 

Scaling mode of the image. 

- Type: `string` (`contain` | `cover` | `fill` | `none` | `scale-down`)  
- Default value: `cover` 
- Mandatory: no 

The values for this property are:
- `cover`: Resize an image while keeping its aspect ratio. As a result, the image's dimensions could exceed the view's dimensions, and the image will be centered.
- `contain`: Resize an image while keeping its aspect ratio within the view's dimensions and centering it.
- `fill`:	Resize an image without keeping its aspect ratio to fill the view.
- `none`:	Resize an image while keeping its original aspect ratio.
- `scale-down`: The effect of this style is similar to applying `none` and `contain` in sequence. The result will be a smaller image.


## Events

In addition to the [common events](./events), this element supports the following events:

- [`complete`](#complete)
- [`error`](#error)


### `complete` 

Event triggered when an image is successfully loaded.

### `error` 

Event triggered when an exception was thrown during the image loading. 

__Additional parameters__: 
- `{ errorCode: number }`

::: warning
This event won't be triggered if the value of `src` is incorrect.
:::

## Methods

This element does not have additional methods.

## Example

``` html
<template>
      <div class="container">
      <div class="case-title mt-item">
          <text class="title">object-fit: contain</text>
        </div>
      <div class="item-container">
        <image src="/Common/img/quickapp_transparent.png" alt="Quick App" class="object-fit-image image-contain"></image>
      </div>
      <div class="case-title mt-item">
          <text class="title">object-fit: cover</text>
        </div>
      <div class="item-container">
        <image src="/Common/img/quickapp_transparent.png" alt="Quick App" class="object-fit-image image-cover"></image>
      </div>
      <div class="case-title mt-item">
          <text class="title">object-fit: fill</text>
        </div>
      <div class="item-container">
        <image src="/Common/img/quickapp_transparent.png" alt="Quick App" class="object-fit-image image-fill"></image>
      </div>
    </div>
</template>

<style lang="sass">
  .object-fit-image{
    width: 70%;
    height: 250px;
    background-color: #f1f1f1;
    margin: 0 auto;
  }
  .image-contain{
  }
  .image-cover{
    object-fit: cover;
  }
  .image-fill{
    object-fit: fill;
  }
  .image-none{
    object-fit: none;
  }
  .image-scale-down{
    object-fit: scale-down;
  }
</style>
```