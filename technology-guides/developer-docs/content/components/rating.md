# `rating`

Star-based rating input field.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/rating01.gif" alt="Rating stars on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

This element doesn't support any element as child node.

## Attributes

In addition to the [common attributes](./attributes), this element may contain the following attributes.

- [`numstars`](#numstars)
- [`rating`](#rating)
- [`stepsize`](#stepsize)
- [`indicator`](#indicator)

### `numstars`

This attribute indicates the number of star elements to be displayed on a rating bar.

- Type: `number` 
- Default value: `5` 
- Mandatory: no 

### `rating`

The current value with the number of selected stars.

- Type: `number` 
- Default value: `5` 
- Mandatory: no 

### `stepsize`

This attribute indicates the size of the steps for the rating.

- Type: `number` 
- Default value: `0.5` 
- Mandatory: no 

### `indicator`

Flag to indicate if rating field is editable (`false`) or not (`true`).

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no 


## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling properties:

- [`star-background`](#star-background)
- [`star-foreground`](#star-foreground)
- [`star-secondary`](#star-secondary)
- [`width`](#width)
- [`height`](#height)

This element supports the [`:active` pseudo-class](../guide/styling.html#css-selectors).

### `star-background`	

Background image of the selectable element (when it is not selected). The URI specified in this element may contain either a local resource or a base64-encoded image. 

- Type: `uri`  
- Default value: - 
- Mandatory: no 

### `star-foreground`	

Image of the selectable element when it's selected. The URI specified in this element may contain either a local resource or a base64-encoded image.

- Type: `uri`  
- Default value: - 
- Mandatory: no 

### `star-secondary`	

Secondary image of the selectable element when it's selected. The URI specified in this element may contain either a local resource or a base64-encoded image.

- Type: `uri`  
- Default value: - 
- Mandatory: no 


### `width`

Attribute that indicates the width of the stars rating bar. 

- Type: `string` (`<length>` | `<percentage>`)  
- Default value: (`numstars` value x width of a star)
- Mandatory: no 

### `height`	

Height of a star.

- Type: `string` (`<length>` | `<percentage>`)  
- Default value: (height of a star)
- Mandatory: no 

## Events

This element supports the [common events](./events), with the exception of [`click`](events.html#click), [`longpress`](events.html#longpress), and [`swipe`](events.html#swipe). In addition to this, this element supports the following event:

- [`change`](#change)

### `change` 

This event is triggered when the current value of a rating input changes. 

__Additional parameters__: 
- `{ rating: number }`

## Methods

This element does not have additional methods.

## Example

``` html
<template>
  <div class="container">
    <div class="case-title mt-item">
      <text class="title">Slide on the stars to rate</text>
    </div>
    <div class="item-container rating-container row-center column-center">
      <rating class="ratingStyle" rating="2"></rating>
    </div>
  </div>
</template>

<style lang="sass">
  .rating-container {
    height: 320px;
  }
  .ratingStyle {
    star-background: url(/Common/img/ic_stars_gray_mid.png);
    star-foreground: url(/Common/img/ic_stars_blue_mid.png);
    star-secondary: url(/Common/img/ic_stars_blue_half_mid.png);
    height: 64px;
  }
</style>
```