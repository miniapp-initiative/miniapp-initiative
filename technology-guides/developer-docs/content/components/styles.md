# Common Styles

All the [essential elements](./) may be rendered using CSS stylesheets. This section shows the common CSS properties that could be applied to all the elements.    

[[toc]]

## Common CSS Properties

### `width`	

The `width` CSS property sets an element's width. If this style is not set, the width will be adjusted to the element content width.

Values:
- `<length>`: Defines the width as an absolute value.
- `<percentage>`: Defines the width as a percentage of the containing block's width

::: tip
The display `width` for the `flex` element is affected by the parent container.
:::

### `height`

The `height` CSS property specifies the height of an element. If this style is not set, the height will be adjusted to the element content height.

Values:
- `<length>`: Defines the height as an absolute value.
- `<percentage>`: Defines the height as a percentage of the containing block's width

::: tip
The actual display height for the flex element is affected by the parent container.
:::

### `padding`	

The `padding` shorthand property sets the padding area on all four sides of an element at once.

This property is a shorthand for the following CSS properties:

- [`padding-bottom`](#padding-top-right-bottom-left)
- [`padding-left`](#padding-top-right-bottom-left)
- [`padding-right`](#padding-top-right-bottom-left)
- [`padding-top`](#padding-top-right-bottom-left)

Values:
- `<length>`: The size of the padding as a fixed value (initial value: `0`) 

### `padding-[top|right|bottom|left]`

This property sets the padding area of a specific side of the element. This attribute cannot be negative.

Values:
- `<length>`: The size of the padding of the specific side of the element as a fixed value (initial value: `0`) 

### `margin`

The `margin` CSS shorthand property sets the margin area on all four sides of an element.

Values:
- `<length>`: The size of the margin area of the element as a fixed value (initial value: `0`) 
- `auto`: The system selects a suitable margin to use.

This property is a shorthand for the following CSS properties:

- [`margin-bottom`](#margin-top-right-bottom-left)
- [`margin-left`](#margin-top-right-bottom-left)
- [`margin-right`](#margin-top-right-bottom-left)
- [`margin-top`](#margin-top-right-bottom-left)

### `margin-[top|right|bottom|left]`	

This property sets the margin area of a specific side of the element. This attribute cannot be negative.

Values:
- `<length>`: The size of the margin of the specific side of the element as a fixed value (initial value: `0`) 
- `auto`: The system selects a suitable margin to use for this side of the element.

### `border`

The `border` shorthand property sets an element's border. It sets the values of `border-width`, `border-style`, and `border-color`.

Values:
- [`<line-width>`](#border-width): Sets the thickness of the border. 
- [`<line-style>`](#border-style): Sets the style of the border. 
- [`<color>`](#border-color): Sets the color of the border. 

This property is a shorthand for the following properties:

- [`border-bottom`](#border-top-right-bottom-left)
- [`border-left`](#border-top-right-bottom-left)
- [`border-right`](#border-top-right-bottom-left)
- [`border-top`](#border-top-right-bottom-left)

### `border-[top|right|bottom|left]`

This property sets the values of `border-width`, `border-style`, and `border-color` of the border in a specific side of the element. 

Values:
- [`<line-width>`](#border-width): Sets the thickness of the border. 
- [`<line-style>`](#border-style): Sets the style of the border. 
- [`<color>`](#border-color): Sets the color of the border. 

### `border-style`

This shorthand property sets the line style of an element's bottom border.

Values: 
- `dotted` | `dashed` | `solid`

### `border-width`

This shorthand property sets the width of an element's border.

Values:
- `<length>`: The width of the border (initial value: `0`) 

### `border-color`	

This shorthand CSS property sets the color of an element's border.

Values:
- `<color>` (`black` by default).

### `border-[left|top|right|bottom]-width`	

This property sets the width of one side of the element's border.

Values:
- `<length>`:  (initial value: `0`) 

### `border-[left|top|right|bottom]-color`	

This property sets the color of one side of the element's border.

Values:
- `<color>` (`black` by default).

### `border-radius`	

This shortcut property rounds the corners of an element's border.

Values:
- `<length>` | `<percentage>`

This property is a shorthand for the following properties:

- [`border-bottom-left-radius`](#border-top-bottom-left-right-radius)
- [`border-top-left-radius`](#border-top-bottom-left-right-radius)
- [`border-bottom-right-radius`](#border-top-bottom-left-right-radius)
- [`border-top-right-radius`](#border-top-bottom-left-right-radius)

::: warning
For rounded corners only `border-width` and `border-[left|top|right|bottom]-width` may be used.
For other types of corners, you only can use `border-color`. `border-[left|top|right|bottom]-color` cannot be applied.
:::

### `border-[top|bottom]-[left|right]-radius`	

This property rounds a specific corner of an element's border.

Values:
- `<length>` | `<percentage>`

### `background`	

This property allows to apply [gradient styles](#gradient-styles).

Values:
- `linear-gradient`

### `background-color`	

This property sets the background color of an element.

Values:
- `<color>`.

### `background-image`	

This property sets the background image of an element.

Values:
- `<uri>`. You can use both external and local resources. You can precompile PNG images using the Android Asset Packaging Tool (AAPT).

::: warning
This style cannot be used together with background-color or border-color. 
:::

### `background-size`	

This property sets the size of the background image in an element.

Values:
- `contain` | `cover` | `auto` | `<length>` | `<percentage>` (`100%` by default)

### `background-repeat`	

This property defines the repetition pattern of a background image in an element.

Values:
- `repeat` | `repeat-x` | `repeat-y` | `no-repeat` (`repeat` by default)

By default, a background image is repeated horizontally and vertically.

### `background-position` 

This property sets the initial position for each background image.

Values:
- `left top` | `center top` | `right top` | `left center` | `center center` | `right center` | `left bottom` | `center bottom` | `right bottom`. If only one keyword is specified, the second value is `center` by default.
- `<length> <length>` (`0px 0px` by default). (only `px` units) The first value indicates the horizontal position, and the second value indicates the vertical position. For the upper left corner, the value is `0 0`. If only one value is specified, the other value is `50%`.
- `<percentage> <percentage>`. The first value indicates the horizontal position, and the second value indicates the vertical position. For the upper left corner, the value is `0% 0%`. For the lower right corner, the value is `100% 100%`. If only one value is specified, the other value is `50%`.

### `opacity`

The `opacity` property sets the opacity of an element. 

Values:
- `<decimal>` (`1` by default) Range `0` to `1`.

### `display`	

This property sets the display type. 

Values:
- `flex` | `none` (`flex` by default)

::: warning
Quick apps only support `flex`. If the value is `none`, the element will not be rendered.
:::

### `visibility`

This property shows or hides an element without changing the layout of a document.

Values:
- `visible` | `hidden`	(`visible` by default)

### `flex`	

This property sets how an item will grow or shrink to fit the space available in its container.

Values:
- `<number>`

::: warning
This style is only valid when the parent container is `div`, `list-item`, or `tabs`.
:::

### `flex-grow`

This property sets the flex grow factor of the element size.

Values:
- `<number>` (`0` by default)	

::: warning
This style is only valid when the parent container is `div` or `list-item`.
:::

### `flex-shrink`

This property sets the flex shrink factor of an element.

Values:
- `<number>` (`1` by default)	

::: warning
This style is only valid when the parent container is `div` or `list-item`.
:::

### `flex-basis`

This property sets the initial main size of an element.

Values:
- `<length>` (`-1` by default)	

::: warning	
This style is valid when the parent container is `div` or `list-item`.
:::

### `position`	

This property sets how an element is positioned on a page. 

Values:
- `relative` | `absolute` | `fixed`	(`relative` by default)

::: warning
`position` cannot be applied when the parent container is `list` or `swiper`.
:::

### `[left|top|right|bottom]`

These properties specify the horizontal and vertical position of an element in the page.

Values:
- `<number>` (`1` by default)	


::: tip
Read more details about CSS properties in the [MDN CSS reference guide](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference).
:::


## Styles for Animations

This section includes the CSS properties you can use to create animations on your [components](./).

### `transform-origin`	

This property sets the origin for an element's transformations.

Values:
- `<position>` (`50% 50%` by default)

This style allows you to change the origin of a transformation. The two values correspond to the horizontal (X) and vertical axis (Y), respectively. The units can be `px` or `%` (e.g., `50px 100px`, `50% 50%`).

Read more about the [transform-origin property](https://developer.mozilla.org/en-US/docs/Web/CSS/transform-origin).

### `transform`	

This property lets you rotate, scale, skew, or translate an element. 

Values:
- `string` with the function to apply:
  - `translate(<length-percentage>, <length-percentage>?)`
  - `translateX(<length-percentage>)`
  - `translateY(<length-percentage>)`
  - `scale(<number>, <number>?)`
  - `scaleX(<number>)`
  - `scaleY(<number>)`  
  - `rotate((<deg|rad>))`
  - `rotateX(<deg|rad>)`
  - `rotateY(<deg|rad>)`

Read more about the [transform property](https://developer.mozilla.org/en-US/docs/Web/CSS/transform) and the [transform functions](https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function).

### `animation-name`

This property specifies the names of one or more `@keyframes` at-rules describing the animation or animations to apply to the element.

Values:
- `string` with the type of animation:

### `animation-delay`	

This property specifies the amount of time to wait from applying the animation to an element before beginning to perform the animation.

Values:
- `<time>` Value in seconds (`s`) or millisecond (`ms`) (`0` by default)

Read more about the [animation-delay property](https://developer.mozilla.org/en-US/docs/Web/CSS/animation-delay).

### `animation-duration`	

This property sets the length of time that an animation takes to complete one cycle. 

Values:
- `<time>` Value in seconds (`s`) or millisecond (`ms`) (`0` by default)

Read more about the [animation-duration property](https://developer.mozilla.org/en-US/docs/Web/CSS/animation-duration).

### `animation-iteration-count`	

This property sets the number of times an animation sequence is played before stopping.

Values:
- `<integer>` | `infinite` (`1` by default) 

Read more about the [animation-iteration-count property](https://developer.mozilla.org/en-US/docs/Web/CSS/animation-iteration-count).

### `animation-timing-function`

This property sets how an animation progresses through the duration of each cycle.

Values:
- `linear` | `ease` | `ease-in` | `ease-out` | `ease-in-out` (`ease` by default)

Read more about the [animation-timing-function property](https://developer.mozilla.org/en-US/docs/Web/CSS/animation-timing-function).

### `animation-fill-mode`	

This function sets how a CSS animation applies styles to its target before and after its execution.

Values:
- `none` | `forwards` (`none` by default)

Read more about the [animation-fill-mode property](https://developer.mozilla.org/en-US/docs/Web/CSS/animation-fill-mode).

### `@keyframes`

The `@keyframes` is a CSS [at-rule](https://developer.mozilla.org/en-US/docs/Web/CSS/At-rule) controls the intermediate steps in a CSS animation sequence by defining styles for keyframes (or waypoints) along the animation sequence.

Keyframes will be identified by unique names and can specify the position of the elements of the animation sequence, using `from` for the starting offset of `0%` and `to` for the ending offset of `100%`.  

Example:

``` css 
@keyframes go { 
    from {
        background-color:#f76160;
    } 
    to {
        background-color: #09ba07;
    } 
}
```

You can also specify intermediate waypoints as percentages:

``` css
@keyframes gradient1 { 
    0% { 
        background-color: black; 
    } 
    33% { 
        background-color: red; 
    } 
    66% { 
        background-color: blue; 
    } 
    100% { 
        background-color: green; 
    } 
} 
@keyframes translation { 
    0% { 
        transform: translateX(0px); 
    } 
    25% { 
        transform: translateX(90px); 
    } 
    100% { 
        transform: translateX(180px); 
    } 
}

```

You can include the following properties in the steps of the sequence:

- [`background-color`](#background-color)
- [`opacity`](#opacity)
- [`width`](#width)
- [`height`](#height) (only in absolute units)
- [`transform`](#transform) (`translate` and `scale` do not support percentages).

Read more about the [@keyframes at-rule](https://developer.mozilla.org/en-US/docs/Web/CSS/@keyframes).


## Gradient Styles

Gradients are smooth transitions between two or more specified colors. For that, you can apply [CSS gradients](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Images/Using_CSS_gradients).

The quick app platform supports two gradient options:
- `linear-gradient()`
- `repeating-linear-gradient`

To create a linear gradient, you need to define both the transition direction and colors. 
1.	Transition direction, as a direction or an angle
2.	Transition colors, in any of the following formats: `<color-name>`, `<#RRGGBB>`, `rgb(R,G,B)`, and `rgba(R,G,B,A)`.

### `linear-gradient()`

``` css
    background: linear-gradient(<direction>, <color-stop>, <color-stop>, ...);
    background: linear-gradient(<angle>, <color-stop>, <color-stop>);
```

Values:
- `<direction>` = `to`	[`left` | `right`] [`top` | `bottom`] (`to bottom` by default)
- `<angle>` = (Optional) Angle in `deg` between the gradient line and the Y axis (clockwise direction), with the geometric center of the element as the origin, and considering X as the horizontal axis.
- `<color-stop>` = `<color>` [`<length-percentage>`] (Mandatory) value of the background color at the different steps.

``` css
to right: /* gradient from left to right */
to bottom right /* gradient from the upper left corner to the lower right corner */
```

### `repeating-linear-gradient()`

``` css
    background: repeating-linear-gradient(<direction>, <color-stop>, <color-stop>, ...);
    background: repeating-linear-gradient(<angle>, <color-stop>, <color-stop>);
```

Values:
- `<direction>` = `to`	[`left` | `right`] [`top` | `bottom`] (`to bottom` by default)
- `<angle>` = (Optional) Angle in `deg` between the gradient line and the Y axis (clockwise direction), with the geometric center of the element as the origin, and considering X as the the horizontal axis.
- `<color-stop>` = `<color>` [`<length-percentage>`] (Mandatory) value of the background color at the different steps.

## `@font-face`

This CSS [at-rule](https://developer.mozilla.org/en-US/docs/Web/CSS/At-rule) specifies a custom font with which to display text; the font can be loaded from either a remote server or a locally installed font on the user's own computer.

If you want to customize the style of the text in components, you can define the `@font-face` in the stylesheets and use the custom font in the `font-family` properties for element styles.

Examples:

``` css
@font-face { 
    font-family: 'myLocalFont';  /* using a local ttf file */ 
    src: url('/Common/fonts/font.ttf'); 
} 
@font-face { 
    font-family: 'myExternalFont';  /* using an external ttf file. */ 
    src: url('https://example.org/font.ttf'); 
} 
@font-face { 
    font-family: 'Arial';
    src: local('Arial'); 
}
```

This at-rule supports the following properties:
- `font-family`: name of the custom font.
- `src`: source of the font file. The source could be:
  − a __relative local path__ (absolute path of a project file).
  − a __public URL__ to the font file.

To apply the defined custom font family to specific elements, you must use the `font-family` property with the name of your customized style. For instance:

``` css
<text class="txt">                
    <span style="font-family:myLocalFont">Hi Quick Apps</span> 
</text> 
```
