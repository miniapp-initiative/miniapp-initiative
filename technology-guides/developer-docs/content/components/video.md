# `video`

Video player.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/video01.gif" alt="Video options on a Quick App" /> 

([Example code](#example))

::: tip Supported Formats
Quick apps supports the HTTP, HTTPS, and RTSP protocols, and audio and video encodings such as H.263, H.264, MPEG-4, AAC, FLAC, MP3. The platform supports the following file formats: MPEG-4 (.mp4), 3GPP (.3gp), MPEG-TS (.ts), Matroska (.mkv), and Ogg (.ogg).
:::

[[toc]]

## Children Elements

This element doesn't support children elements.

## Attributes

In addition to the [common attributes](./attributes), this element may contain the following attributes.

- [`muted`](#muted)
- [`src`](#src)
- [`autoplay`](#autoplay)
- [`poster`](#poster)
- [`controls`](#controls)
- [`orientation`](#orientation)
- [`titlebar`](#titlebar)
- [`title`](#title)

### `muted`

Attribute that indicates whether a video is played in mute mode (`true`) or not (`false`).

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no 

### `src`

Source of the video document. The value can be [an absolute or a relative URI](../guide/directory.html#references-and-uris). 

- Type: `uri` 
- Default value: -
- Mandatory: no 

### `autoplay`

Flag to indicate if a video runs automatically once rendered.

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no

### `poster`

Source of a preview image for the video.

- Type: `uri` 
- Default value: -
- Mandatory: no

### `controls`

Flag to indicate if the system shows the video control bar. If the value is`false`, the controls are hidden.

- Type: `boolean` 
- Default value: `true` 
- Mandatory: no 

### `orientation`

This attribute indicates the page orientation used when the video is displayed in full-screen mode.

- Type: `string` (`portrait` | `landscape`)
- Default value: `landscape` 
- Mandatory: no 

### `titlebar`

Flag that indicates if the system displays the title bar when a video is played in full-screen mode. The value `true` shows the title bar.

- Type: `boolean` 
- Default value: `true` 
- Mandatory: no 

::: warning
When the [`controls`](#controls) attribute is `false`, this parameter does not take effect.
:::


### `title`

The value of this attribute indicates the title to be displayed in the title bar during full-screen playback. 

- Type: `string` 
- Default value: - 
- Mandatory: no 

::: tip
Only one line of text is displayed. If the text length exceeds the limit, the text is automatically truncated (`...`).
:::


## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling properties:

- [`object-fit`](#object-fit)

### `object-fit` 

Indicates how a video and its [preview image](#poster) are resized and adjusted to the container.

- Type: `string` (`contain` | `cover` | `fill` | `none` | `scale-down`)  
- Default value: `contain` 
- Mandatory: no 

The values for this property are:
- `cover`: Resize an image while keeping its aspect ratio. As a result, the image's dimensions could exceed the view's dimensions, and the image will be centered.
- `contain`: Resize an image while keeping its aspect ratio within the view's dimensions and centering it.
- `fill`:	Resize an image without keeping its aspect ratio to fill the view.
- `none`:	Resize an image while keeping its original aspect ratio.
- `scale-down`: The effect of this style is similar to applying `none` and `contain` in sequence. The result will be a smaller image.

## Events

In addition to the [common events](./events) (with the exception of [swipe](events.html#swipe)), this element supports the following events:

- [`prepared`](#prepared)
- [`start`](#start)
- [`pause`](#pause)
- [`finish`](#finish)
- [`error`](#error)
- [`seeking`](#seeking)
- [`seeked`](#seeked)
- [`timeupdate`](#timeupdate)
- [`fullscreenchange`](#fullscreenchange)

### `prepared` 

This event is triggered when a video is successfully loaded. 

### `start` 

This event is triggered when a video starts o re-starts playing.

### `pause` 

This event is triggered when a video is paused.

### `finish` 

This event is triggered when a video ends.

### `error` 

This event is triggered when a video playback fails.

### `seeking` 

This event is triggered after a user interaction with the video progress bar.

__Additional parameters__: 
- `{ currenttime: number }`. The current position of the video (in seconds).

### `seeked` 

This event is triggered when the user ends the interaction with the video progress bar.

__Additional parameters__: 
- `{ currenttime: number }`. The current position of the video (in seconds).

### `timeupdate` 

This event is triggered when the playing progress changes. This event is triggered every 250 ms.

__Additional parameters__: 
- `{ currenttime: number }`. The current position of the video (in seconds).

### `fullscreenchange` 

This event is triggered when a video changes the full-screen mode.

__Additional parameters__: 
- `{ fullscreen: boolean }`


## Methods

This element has the following methods:

- [`start()`](#start)
- [`pause()`](#pause)
- [`setCurrentTime({currenttime})`](#setcurrenttime-currenttime)
- [`requestFullscreen({ screenOrientation })`](#requestfullscreen-screenorientation)
- [`exitFullscreen()`](#exitfullscreen)

### `start()`

Starts playing a video.

### `pause()`

Pauses a video.

### `setCurrentTime({currenttime})`

Method to set the video playing position.

__Parameters__:
- `object` with the following attributes:
  - `currenttime: number`. Position in seconds.

### `requestFullscreen({screenOrientation})`

Requests to enter the full-screen mode.

__Parameters__:
- `object` with the following attributes:
  - `screenOrientation:string`. Indicates the orientation of the screen (`landscape` or `portrait`) (`portrait` by default).

	-	
### `exitFullscreen()`

Method to exit the full-screen mode.

## Example

``` html
<template>
  <div class="container">
    <div class="mt-item mlr-container">
      <video title="Video component" titlebar="{{showTitleBar}}" controls="{{showControls}}" src="{{videoUrl}}" class="video" id="video"></video>
    </div>
    <list class="mlr-container btn-list">
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="startVideo">Play</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="stopVideo">Pause</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="fullscreen">Full Screen</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="switchControls">Toggle Controls</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="switchTitleBar">Toggle Title</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="setTime">Seek (5")</text>
      </list-item>
    </list>
  </div>
</template>

<style lang="sass">
  .btn-list{
    columns: 2;
    .btn-item{
      height: 140px;
      align-items: center;
      justify-content: center;
    }
  }
  .video {
    width: 60%;
  }
  .mt-item {
    flex-direction: row;
    width: 100%;
    justify-content: center;
  }
</style>

<script>
  module.exports = {
    public: {
      videoUrl: "https://espinr.github.io/sandbox/quickapps/videos/raining-leaf.mp4",
      showControls:true,
      showTitleBar:true,
    },
    onInit: function () {
      this.$page.setTitleBar({ text: 'video' });
    },
    startVideo(){
      this.$element('video').start();
    },
    stopVideo(){
      this.$element('video').pause();
    },
    fullscreen(){
      this.$element('video').requestFullscreen();
    },
    exitFullscreen(){
      this.$element('video').exitFullscreen();
    },
    switchControls(){
      this.showControls = !this.showControls;
    },
    switchTitleBar(){
      this.showTitleBar = !this.showTitleBar;
    },
    setTime(){
      this.$element('video').setCurrentTime({currenttime:5});
    }
  }
</script>
```