# Elements Summary

The essential elements to build [components](../guide/components-basics) and pages are the following. 

| Element | Description |
| ------- | ----------- |
| [`a`](./a) | Hyperlink |
| [`canvas`](./canvas) | Canvas and drawings |
| [`camera`](./camera) | Management of the camera |
| [`div`](./div) | Generic block container |
| [`image`](./image) | Image |
| [`input`](./input) | Inline user input (i.e., text, range, email,...) |
| [`label`](./label) | Label for form fields |
| [`list`](./list) | Generic list of items |
| [`list-item`](./list-item) | Item of a `list` |
| [`marquee`](./marquee) | Marquee effect for scrolling text |
| [`option`](./option) | Options in `select` elements |
| [`picker`](./picker) | Selection of items, date, time... |
| [`popup`](./popup) | Pop up messages |
| [`progress`](./progress) | Progress indicator |
| [`rating`](./rating) | User's rating mechanism |
| [`refresh`](./refresh) | Pull-to-refresh mechanism |
| [`richtext`](./richtext) | Styling text features |
| [`select`](./select) | Dropdown form select |
| [`slider`](./slider) | Defined range input |
| [`slot`](./slot) | Template's content slot |
| [`span`](./span) | Generic inline container |
| [`stack`](./stack) | Container for overlaid items |
| [`swiper`](./swiper) | Slideshow or carousel container |
| [`switch`](./switch) | Switch checkbox |
| [`tab-bar`](./tab-bar) | Tab bar within `tabs` |
| [`tab-content`](./tab-content) | Tab content within `tabs` |
| [`tabs`](./tabs) | Block container distributed in tabs |
| [`text`](./text) | Textual content |
| [`textarea`](./textarea) | Multi-line plain text input |
| [`video`](./video) | Video player |
| [`web`](./web) | Web view container |


All these elements have [common attributes](attributes.html#basic-attributes) and support [common events](events.html#base-events).
