# `span`

Inline auxiliary container.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/span01.jpg" alt="Span texts on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

This element only supports `span` as children elements.

## Attributes

This element may contain the following attributes.

- [`id`](#id)
- [`style`](#style)
- [`class`](#class)
- [`for`](#for)
- [`if`](#if)

### `id`	

Unique identifier of the element.

- Type: `string`
- Value by default: -
- Mandatory: no 

### `style`

Definition of the CSS styles inline.

- Type: `string`
- Value by default: -
- Mandatory: no 

### `class`	

Class to apply CSS styles on elements

- Type: `string`
- Value by default: -
- Mandatory: no 

### `for`

Loop on an array of elements.

- Type: `array`	
- Mandatory: no 

### `if`

Boolean conditions to display or skip elements in the rendering process.

- Type: `boolean`
- Mandatory: no 


## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling properties:

- [`color`](#color)
- [`font-size`](#font-size)
- [`font-style`](#font-style)
- [`font-weight`](#font-weight)
- [`text-decoration`](#text-decoration)
- [`font-family`](#font-family)

This element supports the [`:active` pseudo-class](../guide/styling.html#css-selectors).

### `color`	

Color of the font.

- Type: `<color>`  
- Default value: `rgba(0, 0, 0, 0.54)` 
- Mandatory: no 

### `font-size`	

Font size.

- Type: `<length>`  
- Default value: `30px` 
- Mandatory: no 

### `font-style`

Font style selected from the [font family](#font-family).

- Type: `string` (`italic` |	`normal`)  
- Default value: `normal` 
- Mandatory: no 

### `font-weight`	

Weight of the font.

- Type: `string` (`lighter` | `100` | `200` | `300` | `400` | `500` | `600` | `700` | `800` | `900` | `normal` | `bold` | `bolder`)  
- Default value: `normal` 
- Mandatory: no 

### `text-decoration`	

Decoration options to the text.

- Type: `string` (`underline` | `line-through` | `none`)  
- Default value: `none` 
- Mandatory: no 

### `font-family`

Font family of for the text.

- Type: `string`  
- Default value: -
- Mandatory: no 

To customize fonts, please refer [font-face style](styles.html#font-face).

## Events

No events supported on this element. 

## Methods

This element does not have additional methods.

## Example

``` html
<template>
  <div class="container">
    <div class="case-title mt-item">
      <text class="title">As a text's child component</text>
    </div>
    <div class="mlr-container">
      <div>
        <text>
          <span class="color-primary">I am span... I am span... I am span... I am span... I am span... I am span... </span>
        </text>
      </div>
    </div>
    <div class="case-title mt-item">
      <text class="title">{{$t('message.component.span.title3')}}</text>
    </div>
    <div class="mlr-container">
      <div>
        <a href="https://quick-app-initiative.ow2.io/">
          <span class="color-a">Text inside a span element</span>
          <span class="color-a">...more span text... </span>
          <span class="color-a">I am span... </span>
        </a>
      </div>
    </div>
  </div>
</template>
```