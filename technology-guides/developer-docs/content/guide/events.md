# Event Handling

We can listen to events on elements and run some scripts when they are triggered. 

There are some [common events](../components/events) that may be triggered on any essential element:

- `touchstart`, `touchmove`, `touchcancel`, `touchend`, `key`, `click`, `swipe`, and `longpress`, triggered after the user's interaction with the app;
- `focus`, `blur`, `appear`, `disappear`, and `resize` when the element changes its appearance or is selected/unselected;
- `animationstart`, `animationiteration`, and `animationend`, triggered during an animation process.

Check all the [essential elements](../components) and their specific events. 


## Event Listeners

We can use the name of the event with the prefix `on`, or using the `@` symbol to bind a listener method (or handler) with an event. 

For instance, the follow example binds the method `press()` with the `click` event of `<div>` elements.

``` html
<template> 
    <div> 
        <!-- Normal format --> 
        <div onclick="press"></div> 
        <!-- Shortcut --> 
        <div @click="press"></div> 
    </div> 
</template> 
<script> 
    module.exports= { 
        press: function(e) { 
            this.title = 'You clicked!!' 
        } 
    } 
</script>
```

The event handler includes an argument for the event that is triggered. In the case the method includes other parameters, the event is automatically added to the parameter list. 

## Event Handlers with Constant Parameters

Methods to handle events may receive static and dynamic parameters.

This example displays a list of 5 correlative integers and binds `click` event handler on each digit. 

``` html 
<template> 
    <div clas="page"> 
        <text for="{{list}}" onclick="handle($idx,$item)">{{$item}}</text> 
    </div> 
</template> 
<style> 
    .page{ 
        flex-direction: column; 
        align-items: center; 
    } 
</style> 
<script> 
export default { 
    private: { 
        list: [1,2,3,4,5] 
    }, 
    handle(index, item, event) { 
        console.log("Index: " + index); 
        console.log("Item: " + item); 
        console.log("Event: " + JSON.stringify(event)); 
    } 
} 
</script>
```

After clicking on the last digit, the console shows:

``` bash
Index:4
Item: 5
Event: {"offsetX":8,"offsetY":724,"pageY":724,"clientY":724,"pageX":75,"clientX":75,"type":"click","target":{"ref":"30","type":"text","attr":{"value":5},"style":{"animationName":null,"fontFamily":[]},"event":["click"]},"currentTarget":{"ref":"30","type":"text","attr":{"value":5},"style":{"animationName":null,"fontFamily":[]},"event":["click"]},"timeStamp":1632828311348}
``` 

### Event Handlers with Variable Parameters

Event handler also may receive dynamic parameters, stored in the current component instance.

This example displays a list of 5 correlative integers and binds `click` event handler on each digit, passing a variable and the item of the list. 

``` html 
<template> 
    <div clas="page"> 
        <text for="{{list}}" onclick="handle($idx,$item,result)">{{$item}}</text> 
    </div> 
</template> 
<style> 
    .page{ 
        flex-direction: column; 
        align-items: center; 
    } 
</style> 
<script> 
export default { 
    private: { 
        list: [1,2,3,4,5],
        result: 0 
    }, 
    handle(index, item, res, event) { 
        console.log("Index: " + index); 
        console.log("Item: " + item); 
        console.log("Result: " + res); 
        console.log("Event: " + JSON.stringify(event)); 
    } 
} 
</script>
```

After clicking on the last digit, the console shows:

``` bash
Index: 4
Item: 5
Result: 0
Event: {"offsetX":8,"offsetY":717,"pageY":717,"clientY":717,"pageX":75,"clientX":75,"type":"click","target":{"ref":"10","type":"text","attr":{"value":5},"style":{"animationName":null,"fontFamily":[]},"event":["click"]},"currentTarget":{"ref":"10","type":"text","attr":{"value":5},"style":{"animationName":null,"fontFamily":[]},"event":["click"]},"timeStamp":1632828900275}
```

Read more on events within each [essential element](../components). 