# Getting Started

## Understanding Quick Apps

[Quick app](https://quick-app-initiative.ow2.io/page/whitepaper/#what-is-a-quick-app) is an implementation of the emerging light apps paradigm (no-installation hybrid apps) in the process of standardization by the [W3C MiniApps Working Group](https://www.w3.org/2021/miniapps/).

Quick app provides a framework for mobile application development based on widely known front-end web technologies (JavaScript, CSS,…). It enables developers to create “light” applications more efficiently with significant access to a host devices’ native resources and services. Read more about this framework and uses cases on the [Quick App White Paper](https://quick-app-initiative.ow2.io/page/whitepaper/#what-is-a-quick-app).

Quick apps are compiled and packaged as `.rpk` files. `.rpk` packages can be executed by quick app engines deployed on the device's operating system. Currently, the Quick App platform is __only supported by Android devices__  (version 5+), but we expect to see cross-platform implementations in the near future (more in the open-source [Quick App Initiative](https://quick-app-initiative.ow2.io/page/about/)).

## Prerequisites

- [Node.js (10+)](https://nodejs.org/en/)

The development tools are based on [Node.js](https://nodejs.org), a JavaScript runtime built on Chrome's V8 JavaScript engine. Also, for installing the dependencies, we will need the npm package manager. You may download and run a [pre-built installer for your platform](https://nodejs.org/en/download/).

## Environment Installation

There are two ways to start creating a quick app :
1. [Using a __Quick App IDE__](#option-1-using-a-quick-app-ide) with all the tools integrated;
2. [Using separate tools](#option-2-separate-tools): your favorite editor, an _npm_-based toolkit, and the [quick app loader](#quick-app-loader) on your Android device.

Various quick app IDEs allow you to create apps based on intuitive templates and compile and debug them directly within the same environment. In the second case, you must have more advanced skills (i.e., [ADB](https://android-doc.github.io/tools/help/adb.html) and management of Android packages) and install the following tools separately.  

### (Option 1) Using a Quick App IDE

This guide uses the Huawei Quick App IDE, but the process is similar to other IDEs —there is a [generic Quick App IDE](https://www.quickapp.cn/docCenter/IDEPublicity), but some parts are in Chinese.

The Quick App IDE is based on the VS Code framework that enables the development of quick apps through easy-to-use services such as coding templates, real-time preview, debugging. It also allows cloud testing and releasing on concrete marketplaces in case you are interested.

Currently, the Quick App IDE runs on systems based on Windows and macOS. [Download and install the latest version](https://developer.huawei.com/consumer/en/doc/development/Tools-Library/quickapp-ide-download-0000001101172926).

### (Option 2) Separate Tools

#### Quick App Loader

This guide uses the Huawei Quick App Loader, but the process is similar to other loaders —there is a [generic Quick app loader](https://www.quickapp.cn/docCenter/post/69), but some parts are in Chinese.

The Quick App Loader enables you to launch and debug quick apps on your Android device, even though it does not include a built-in quick app engine. This APK contains the quick app platform's basic functions that will allow you to load `.rpk` files. 

- [Quick App Loader for Android](https://developer.huawei.com/consumer/en/doc/development/Tools-Library/quickapp-ide-download-0000001101172926#section9347192715112)

::: tip NOTE
The quick app platform is evolving, and the quick app services and capabilities are growing, so ensure the version of the quick app engine is always equals or superior to the version used by the quick app.
:::

::: tip
If you are interested in developing tools for other platforms, join the (open-source [Quick App Initiative](https://quick-app-initiative.ow2.io/page/about/) and let us know! 
:::

## Create Your First Quick App

You can use the IDE to understand the basic structure of a quick app and test it in a few minutes. 

Once you launch the IDE, you can create a _New Project_ based on a _Hello World_ template. 

<img style="width: 500px; border: 1px #333333 solid" src="./images/ide01.png" alt="Quick App IDE screenshot: new project from a template" />

The next screen allows you to set up your app, indicating the name, the identifier of the package, and the location in the local filesystem:

<img style="width: 500px; border: 1px #333333 solid" src="./images/ide02.png" alt="Quick App IDE screenshot: configuration of the new project (metadata)" />

If you chose the _Hello World_ template, the IDE would create a new project with the minimum configuration for your first quick app, with only one page and a text showing a message in the center of the screen.

The structure of directories and files of the source code (`src`) is something like: 

```
├── manifest.json
├── app.ux
├── Hello
|   └── hello.ux
└── Common
    └── logo.png
```

### `manifest.json`

In the `manifest.json` you can find the basic metadata and configuration of the app:

``` json
{
  "package": "org.example.myquickapp",
  "name": "My First Quick App",
  "versionName": "1.0.0",
  "versionCode": 1,
  "icon": "/Common/logo.png",
  "minPlatformVersion": 1060,
  "features": [],
  "permissions": [
    {
      "origin": "*"
    }
  ],
  "config": {},
  "router": {
    "entry": "Hello",
    "pages": {
      "Hello": {
        "component": "hello"
      }
    }
  },
  "display": {}
}
```

### `app.ux`

In the `app.ux` file you can find event handlers that will be triggered when the app is launched and closed:

``` html
<script>
  module.exports = {
    onCreate() {
      console.info('App just created!!');
    },
    onDestroy() {
      console.info('App destroyed!!');
    },
    dataApp: {
      localeData: {}
    }
  }
</script>
```

### The Page

As you can see in the `manifest.json` the app has only one page named `Hello`, whose main component is `hello`. This component can be found in the file `/Hello/hello.ux`: 

``` html
<template>
  <div class="container">
    <text class="title">Hi Quick Apps!!!</text>
  </div>
</template>

<style>
  .container {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
  .title {
    font-size: 100px;
  }
</style>

<script>
  module.exports = {
    data: {
      componentData: {},
    },
    onInit() {
      this.$page.setTitleBar({
        text: 'My First Quick App',
        textColor: '#000000',
        backgroundColor: 'orange',
        backgroundOpacity: 0.5,
        menu: true
      });
    }
  }
</script>
```

## Compile the Code

Using the menu of the IDE, you can compile the project (i.e., _Build_ > _Run Build_). 

<img style="width: 600px; border: 1px #333333 solid" src="./images/ide04.png" alt="Quick App IDE screenshot: view of the editor of the page" />

If you want to compile the app using __npm__ from the command line, you can install the dependencies using the  _Npm_ > _Start Npm Library_ option in the menu. This will place the quick app compiler (`fa-toolkit-version.tgz`) in the home project directory.

Now you can perform the compilation directly from the home directory of the project.

``` bash
npm run fa-build
```

This command compiles the app, creating a new directory `/dist` that includes the quick app package (`org.example.myquickapp.rpk`), named after the metadata specified in the manifest file.

## Run the App

The quickest way to test the app is using an Android device connected to a computer running the Quick App IDE. Just connect your Android device using the USB port (you can also use a wireless connection), and connect it using the _Connect_ option in the menu of the Quick App IDE.

You will be able to link and configure the device from the IDE:

<img style="width: 400px; border: 1px #333333 solid" src="./images/ide06.png" alt="Quick App IDE screenshot: configuration of the external running device" />  

::: tip
Ensure that the device has the developer mode enabled.
:::

Once the device is connected, you can run the quick app directly on the connected device using the _Run_ option. 

In case the Android device does not have support for running quick apps, the IDE may download and install the quick app engine on your device (the [Quick App Loader](#quick-app-loader)). 

After that, you can interact with the quick app on your device:

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/ide05.png" alt="Quick App screenshot running on a device" />  

<!--
Learn more about the [Quick App IDE](https://developer.huawei.com/consumer/en/doc/development/Tools-Guides/ide-overview-0000001147936547).
-->