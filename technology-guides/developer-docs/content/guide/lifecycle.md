# App/Page Lifecycle

Quick app engines separates the __view layer__, responsible of rendering the content, and the __logic layer__, that performs the business logic of the app. The __view layer__ renders the pages and its components using an hybrid approach (using both native and web elements). The __logic layer__ or scripting layer, deals with events, API interaction, and __lifecycle management__.

The quick app lifecycle enable developers to manage both, the view and logic layers through the global [app lifecycle events](#app-lifecycle-events) and the specific individual [page lifecycle events](#page-lifecycle-events). 

## App Lifecycle Events

A quick app implements the following event handlers that control the global behavior of the app.

- [`onCreate`](#oncreate)
- [`onRequest`](#onrequest) 
- [`onShow`](#onshow)
- [`onHide`](#onhide)
- [`onError`](#onerror)
- [`onPageNotFound`](#onpagenotfound)


### `onCreate`

Event triggered when the app is created.

- Return: -

### `onRequest`

Event triggered when the app is started.

- Return: -

### `onShow`

Event triggered when the app is app is displayed.

- Return: -

### `onHide`

Event triggered when the app is hidden.

- Return: -

### `onError` 

Event triggered when the an error occurs in the app.

- Return: `object` that contains the message and stack information. 
  - `message` member indicates the error description.
  - `stack` member indicates the system stack of the call about an error message.
  

### `onPageNotFound` 

Event triggered when the engine redirects to an error page

- Return: `object` that contains the attribute `uri`, with the source where the error was produced.

### `onDestroy`	

Event triggered when the app is destroyed.

- Return: -


## Page Lifecycle Events

A page implements the following event handlers that control the behavior of individual pages.

- [`onInit`](#oninit)
- [`onReady`](#onready)
- [`onShow`](#onshow-page)
- [`onHide`](#onhide-page)
- [`onDestroy`](#ondestroy-page)
- [`onBackPress`](#onbackpress)
- [`onMenuButtonPress`](#onmenubuttonpress)
- [`onRefresh`](#onrefresh)
- [`onConfigurationChanged`](#onconfigurationchanged)

### `onInit`

Event triggered when the page completes the initialization.

- Return: -

### `onReady`

Event triggered when the page is created and is ready to be displayed. This event is triggered only once for each page.

- Return: -


### `onShow` (page)

Event triggered when a user accesses the page.

- Return: -

::: warning
This event is not triggered when the root node of the page is a custom component. You are recommended to use `div` as the root node.
:::

### `onHide` (page)

Event triggered when a page is redirected to another.

- Return: -

### `onDestroy` (page)

Event triggered when a page is redirected to another without storing in the navigation stack.

- Return: -

### `onBackPress`

Event triggered when the user activates the back button.

- Return: `boolean`
- Behavior:
  - If the returned value is `true`, the page handles the return logic itself;
  - If the returned value is `false`, the system uses the return logic by default;
  - If no value is returned: same as `false`.

### `onMenuButtonPress`

Event triggered when a user presses a menu key.

- Return: `boolean`
- Behavior:
  - If the returned value is `true`, the page handles the return logic itself;
  - If the returned value is `false`, the system uses the return logic by default;
  - If no value is returned: same as `false`.

### `onRefresh`

Event triggered when a page is re-opened.

- Return: `object`
- Behavior:
  - If the page has [`launchMode`](./manifest.html#launchmode) set to `singleTask`, only one instance of this page can exist, so this event is triggered once the page is opened again.
  - This event is triggered when the parameter of the router `push` function contains the `clearTask` flag when a page is opened (and an instance already exists). The callback parameters are the same provided when the page is re-opened. See more details in the [page launch mode section](#page-launch-mode).


### `onConfigurationChanged` 

Event triggered when the app configuration changes (e.g., the language, country, region, and text output direction).

- Parameters: -
- Return: `object` (e.g., `{"type": "locale","types":["locale","layoutDirection"]}`)
- Behavior: The constants mark the changes.
  - `locale`: changes in language, country, or region;
  - `layoutDirection`: changes in the text direction of the system.


### Sequence of Page Lifecycle

The sequence of the page lifecycle (page _A_) is as follows:
- Open page _A_: `onInit()` -> `onReady()` -> `onShow()`
- Open page _B_ on page _A_: `onHide()`
- Return to page _A_ from page _B_: `onShow()`
- Back from page _A_: `onBackPress()` -> `onHide()` -> `onDestroy()`


## Page Launch Mode

You can define how the router manages the page lifecycle. 

You can do it either statically, [in the manifest](./manifest.html#launchmode), or dynamically with a flag in the router.


### Definition of Page Launch Mode in the Manifest

You can add the [`launchMode`](./manifest.html#launchmode) attribute to `router.page` in the manifest document.

The `launchMode` member of router's page object is optional and have the following configuration:

- Type: `string`
- Value by default: `standard`
- Required: no

The launchMode attribute supports two different values: `standard` and `singleTask`.

- `standard`. Using this value, the system creates a new page instance every time a user accesses the page.
- `singleTask`. Using this value, the system opens the existing instance of the page every time a user opens the page. It triggers the `refresh` event (i.e., [`onRefresh`](#onRefresh) handler) to remove other pages launched through this page. If it is the first time the page is launched, the system creates one page instance.

Example:

``` json
"router": { 
    "entry": "PageA", 
    "pages": { 
        "PageA": { 
            "launchMode": "singleTask", 
            "component": "index" 
        }, 
        "PageB": { 
            "launchMode": "standard", 
            "component": "index" 
        }, 
        "PageC": { 
            "launchMode": "singleTask", 
            "component": "index" 
        } 
    } 
}
```

To open the sequence of pages _PageA_, _PageB_, _PageC_, _PageB_, _PageC_, and _PageA_, the behavior of the system will be as follows: 

[Page Stack is empty]

1.	Open _PageA_ (first time) 
  - Create instance of _PageA_;
  - Page Stack: [_PageA_].
2.	Open _PageB_
  - Create instance of _PageB_;
  - Display _PageB_ on _PageA_; 
  - Page Stack: [_PageA_, _PageB_].
3.	Open _PageC_ (fist time) 
  - Create instance of _PageC_;
  - Display _PageC_ on _PageB_;
  - Page Stack: [_PageA_, _PageB_, _PageC_].
4.	Open _PageB_
  - Create instance of _PageB_; 
  - Display _PageB_ on _PageC_; 
  - Page Stack: [_PageA_, _PageB_, _PageC_, _PageB_].
5.	Open _PageC_ 
  - Close _PageB_ on top of the stack;
  - Show the existing instance of _PageC_ and trigger _PageC_'s [`onRefresh` lifecycle event](#onrefresh);
  - Page Stack: [_PageA_, _PageB_, _PageC_].
6.	Open _PageA_
  - Close _PageC_, _PageB_ instances (on top of _PageA_)
  - Show the existing instance of _PageA_ and trigger _PageA_'s [`onRefresh` lifecycle event](#onrefresh);
  - Page Stack: [_PageA_].

### Dynamic Page Launch Mode 

You can use two different ways to indicate the launch mode of a page, when working with the router in runtime. 

You can either specify the __launch flag attribute__ in the `router.push` method, or indicate the __launch flag attribute__ as a parameter in the link of the page to opened.

The page launch flag is defined as follows:

- Attribute: `___PARAM_LAUNCH_FLAG___`
- Type: `string`
- Required: no

If this attribute is `clearTask`, all the pages on the page stack are closed when the target page is opened. If the stack contains multiple instances of target, the system will select the earliest instance and it will trigger the [`onRefresh` lifecycle event](#onrefresh) on this instance. If there is no instances of the target page, the system will close all pages and creates a new instance for the target page.

Example:

``` js
router.push({ 
    uri: '/PageB', 
    params: { 
        ___PARAM_LAUNCH_FLAG___: 'clearTask' 
    } 
})
```

If the page stack consists of [_PageA_, _PageB_, _PageC_] the logic of the system for opening the _PageB_ is as follows:

1.	Destroy the _PageC_ instance;
2.	Destroy the _PageA_ instance;
3.	Show the existing _PageB_ instance and trigger [`onRefresh` lifecycle event](#onrefresh) on this instance.

If the page stack consists of [_PageA_, _PageC_], the logic of the system for opening the _PageB_ is as follows:
1.	Destroy the _PageC_ instance.
2.	Destroy the _PageA_ instance.
3.	Create a _PageB_ instance and show it.

