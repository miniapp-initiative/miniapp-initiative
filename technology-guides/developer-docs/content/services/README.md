# Summary of APIs

Quick apps support services and APIs to enable complex business logic and rich user experience. Read more details about the [basics of APIs and services](../guide/api-basics.html#types-of-api-functions), or access all the details for the concrete services listed below. 

[[toc]]

### Basic Services

| Service | Module | Description |
| ------- | ------ | ----------- |
| [App Context](./app-context) | `system.app` | Information about the quick app |   
| [Background Running](./background-running) | `system.resident` | Running apps in the background |
| [Logging](./logging) | - | Methods for recording messages (e.g., console)  |
| [Package Management](./package-management) | `system.package` | App package management |
| [Page Routing](./page-routing) | `system.router` | Routing of pages and components |


### UI Interaction

| Service | Module | Description |
| ------- | ------ | ----------- |
| [Notifications](./notification) | `system.notification` | Notification display |
| [Pop-up](./pop-up) | `system.prompt` | User dialogs and prompts |
| [QR Code](./qr-code) | `system.barcode`  | QR code scan |
| [Sharing](./sharing) | `system.share` | Data sharing with other external apps |
| [Text to Speech (TTS)](./text-to-speech) | `system.tts` | Text-to-speech functions |
| [Vibration](./vibration) | `system.vibrator` | Vibration management |
| [WebView](./webview) | `system.webview` | Use of WebView to display websites within quick apps |

### Network

| Service | Module | Description |
| ------- | ------ | ----------- |
| [Fetch](./fetch) | `system.fetch` | Getting content from the network |
| [Download](./download) | `system.downloadtask` | File download management |
| [Network Status](./network-status) | `system.network` | Status of the network (WiFi, 4G, 5G, ethernet,...)  |
| [Request](./request) | `system.requesttask` | Network requests |
| [Upload](./upload) | `system.uploadtask`| File upload management |
| [Upload/Download Files](./upload-download) | `system.request` | Upload and download files through the network |
| [WebSockets](./websocket) | `system.websocketfactory` | WebSocket management |

### Files and Data Management

| Service | Module | Description |
| ------- | ------ | ----------- |
| [Cryptographic Operations](./crypto) | `system.cipher` | Cryptographic algorithm operations |
| [Data Exchange](./data-exchange) | `service.exchange` | Data exchange between quick apps |
| [Data Storage](./data-storage) | `system.storage` | Local app database management |
| [File Management](./file-storage) | `system.file` | Local file system management  |
| [ZIP Decompression](./zip) | `system.zip` | Decompress ZIP files |

### System Services

| Service | Module | Description |
| ------- | ------ | ----------- |
| [Alarms](./alarms) | `system.alarm` | Management of device's alarms. |
| [App Configuration](./app-configuration) | `system.configuration` | Management of app configuration (locale, date, screen orientation) |
| [Audio Volume](./audio-volume) | `system.volume` | System's multimedia volume management |
| [Battery Level](./battery-level) | `system.battery` | Device's battery level |
| [Bluetooth](./bluetooth) | `system.bluetooth` | Bluetooth communication management |
| [Calendar Events](./calendar) | `system.calendar` | Insert events in the calendar |
| [Clipboard](./clipboard) | `system.clipboard` | Clipboard management |
| [Contacts](./contacts) | `system.contact` | Contact selection |
| [Device Information](./device) | `system.device` | Information about the device |
| [Home Screen Icon](./home-screen-icon) | `system.shortcut` | App shortcut installation |
| [Location](./location) | `system.geolocation` | Geolocation |
| [MediaQuery](./mediaquery) | `system.mediaquery` | Media Query matching and listeners |
| [Screen Brightness](./screen-brightness) | `system.brightness` | Control of screen brightness |
| [Sensors](./sensor) | `system.sensor` | Sensors (accelerometer, compass, proximity, light, step counter...) |
| [SMS Messages](./sms) | `system.sms` | SMS distribution |
| [Wi-Fi](./wifi) | `system.wifi` | WiFi connection management |

### Multimedia

| Service | Module | Description |
| ------- | ------ | ----------- |
| [Animations](./animations) | - | Visual animations and transformations |
| [Audio Player](./audio) | `system.audio` | Audio player |
| [Audio Recording](./recording) | `system.record` | Audio recording |
| [Canvas](./canvas) | - | Canvas manipulation |
| [Image Processing](./image-processing) | `system.image` | Advanced image processing |
| [Multimedia](./multimedia) | `system.media` | Video/audio management (recording, preview,...) |
| [Video Processing](./video) | `hap.io.Video` | Video manipulation |

