# Data Storage

__Local app database management.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.storage"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import storage from '@system.storage' 
```

Or

``` js
let storage = require("@system.storage")
```


## Methods

This service has the following methods:

- [`get({key,default,success,fail,complete})`](#get-key-default-success-fail-complete)
- [`set({key,value,success,fail,complete})`](#set-key-value-success-fail-complete)
- [`clear({success,fail,complete})`](#clear-success-fail-complete)
- [`delete({key,success,fail,complete})`](#delete-key-success-fail-complete)
- [`key({index,success,fail,complete})`](#key-index-success-fail-complete)

### `get({key,default,success,fail,complete})`

__Method to read the value of a key-value pair stored in the app database__.

#### Arguments

This method requires an `object` with the following attributes:
- `key` (`string`). Mandatory attribute with the index key.
- `default` (`string`). Optional attribute with the value that will be returned by default if the key does not exist in the database. 
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The function has an argument that is a `string` with the value corresponding to the `key` in the database. 
- `fail` (`function`). Optional callback function corresponding to a failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

::: tip 
In case `default` is not specified and `key` doesn't exist, the system will return the empty string.
:::

Example:

``` js 
storage.get({ 
  key: 'A1', 
  success:function(data){console.log("handling success");}, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```

### `set({key,value,success,fail,complete})`

__Method to write and update the value of a key-value pair stored in the app database__.

#### Arguments

This method requires an `object` with the following attributes:
- `key` (`string`). Mandatory attribute with the index key.
- `value` (`string`). Optional attribute with the value that will be stored in the database. It cannot exceed 2 MB. 
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
- `fail` (`function`). Optional callback function corresponding to a failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

::: tip Usage
If the new `value` is an empty string, the key-value pair will be removed from the database.
:::

Example:

``` js 
storage.set({ 
  key: 'A1', 
  value: 'V1', 
  success:function(data){console.log("handling success");}, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```

### `clear({success,fail,complete})`

__Method to remove all the key-value pairs stored in the app database__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
- `fail` (`function`). Optional callback function corresponding to a failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
storage.clear({ 
  success:function(data){console.log("handling success");}, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```

### `delete({key,success,fail,complete})`

__Method to delete a key-value pair stored in the app database__.

#### Arguments

This method requires an `object` with the following attributes:
- `key` (`string`). Mandatory attribute with the index key.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
- `fail` (`function`). Optional callback function corresponding to a failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
storage.delete({ 
  key: 'A1', 
  success:function(data){console.log("handling success");}, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```

### `key({index,success,fail,complete})`

__Method to get the key of a key-value pair stored in the app database by its numeric index__.

#### Arguments

This method requires an `object` with the following attributes:
- `index` (`number`). Mandatory attribute with the index of the key.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The callback function has an argument that will contain the key identifier corresponding to the index.  
- `fail` (`function`). Optional callback function corresponding to a failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

::: tip Usage
If the new `value` is an empty string, the key-value pair will be removed from the database.
:::

Example:

``` js 
storage.key({ 
    index: parseIndex, 
    success: function (ret) { 
        console.info('storage.key(): ', JSON.stringify(ret)) 
        prompt.showToast({message: 'key: ' + ret,}) 
    }, 
    fail: function (erromsg, errocode) { 
        prompt.showToast({ message: 'key fail --- ' + errocode + ':' + erromsg }); 
        console.info('key fail --- ' + errocode + ':' + erromsg) 
    }
})
```
