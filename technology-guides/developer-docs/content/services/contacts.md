# Contacts

__Access to the contacts list.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.contact"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import contact from '@system.contact' 
```

Or

``` js
let contact = require("@system.contact")
```


## Methods

This service has the following method:

- [`pick({success,fail,complete})`](#pick-success-fail-complete)

### `pick({success,fail,complete})`

__Selects a contact__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` as main argument with the following members:
  - `res.displayName` (`string`) with the name of the contact selected.
  - `res.number` (`string`) with the phone number of the contact.
- `fail` (`function(code)`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
contact.pick({ 
    success: function(data) { 
        console.log('data : '+data.displayName+', number = '+data.number); 
    }, 
    fail: function(errmsg, errcode) { 
        console.log('pick failed ' + errmsg+' errcode = '+errcode); 
    } 
})
```
