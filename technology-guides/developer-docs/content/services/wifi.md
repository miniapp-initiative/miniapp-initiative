# Wi-Fi

__WiFi connection management.__

Methods for searching nearby Wi-Fi signals and attach to hotspots.

::: warning
If the device's location capabilities are disabled, these methods don't work.
:::

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.wifi"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import wifi from '@system.wifi' 
```

Or

``` js
let wifi = require("@system.wifi")
```


## Methods

This service has the following methods:

- [`connect({SSID,BSSID,password,success,fail,complete})`](#connect-ssid-bssid-password-success-fail-complete)
- [`scan({success,fail,complete})`](#scan-success-fail-complete)
- [`getConnectedWifi({success,fail,complete})`](#getconnectedwifi-success-fail-complete)

### `connect({SSID,BSSID,password,success,fail,complete})`

__Connects to a Wi-Fi hotspot__.

If you already know information about a specific Wi-Fi hotspot, you can call this method directly.

::: warning
The connection will only be established after the user is notified by the operating system and agrees to connect.
:::


#### Arguments

This method requires an `object` with the following attributes:
- `SSID` (`string`). Mandatory attribute that indicates the SSID of the network.
- `BSSID` (`string`). Mandatory attribute that indicates the BSSID of the network.
- `password` (`string`). Mandatory attribute that indicates the password.
- `success`	(`function`). Optional callback function corresponding to the successful execution. 
- `fail` (`function`). Optional callback function corresponding to a failed execution.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
wifi.connect({ 
    SSID:'myssid', 
    BSSID:'mybssid', 
    password:'', 
    success: function() { 
        console.log('connect wifi success'); 
    }, 
    fail: function(errmsg, errorCode) { 
        console.log('connect failed ' + errmsg+', error = '+errorCode); 
    } 
})
```

### `scan({success,fail,complete})`

__Method to get the list of Wi-Fi networks around.__ 

Use the [`onscanned`](#onscanned-wifilist) listener to get the list of the networks once they are discovered.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
- `fail` (`function`). Optional callback function corresponding to a failed execution.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
wifi.scan({ 
    success:function() { 
        console.log('scan wifi success');     
    }, 
    fail: function(errmsg, errorCode) { 
        console.log('scan failed ' + errmsg+', error = '+errorCode); 
    } 
})
```

### `getConnectedWifi({success,fail,complete})`

__Method to get information about the Wi-Fi network currently connected.__ 

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The function has an `object` argument with the following attributes:
  - `SSID` (`string`). The SSID of the network.
  - `BSSID` (`string`). The BSSID of the network.
  - `secure` (`boolean`). Flag that indicates if the network hotspot is secured (`true`) or not.
  - `signalStrength` (`number`). Quality of the Wi-Fi signal. The value is an `integer` between `–100` and `0`, indicating the dBm of the signal. 
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `1000` (`password error`): Incorrect Wi-Fi password.
  - `1001` (`connection timeout`): Connection timed out.
  - `1002` (`duplicate request`): The device has already connected to the Wi-Fi hotspot.
  - `1003` (`wifi not turned on`): Wi-Fi connection is disabled.
  - `1004` (`gps not turned on`): GPS positioning is disabled.
  - `1005` (`invalid SSID`): Invalid Wi-Fi device SSID.
  - `500` (`quick app in background`): The app cannot be operated in the background.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

::: tip Values of reference (dBm)
- `–49` to `0`: Signal is strong.
- `–70` to `–50`: Signal is weak.
- <`–70`: It is difficult to establish a connection or keep connected.
:::

Example:

``` js 
wifi.getConnectedWifi({ 
    success:function(ret) { 
        console.log('ssid = '+ret.SSID+', BSSID = '+ ret.BSSID+', secure = '+ret.secure+', signalStrength = '+ret.signalStrength); 
    }, 
    fail: function(errmsg, errorCode) { 
        console.log('getConnectedWifi failed ') 
    } 
})
```

## Events

This service implements the following event listeners:

- [`onscanned({wifiList})`](#onscanned-wifilist)
- [`onstatechanged({SSID,BSSID,secure,signalStrength,state})`](#onstatechanged-ssid-bssid-secure-signalstrength-state)


### `onscanned({wifiList})`

__Method to be triggered with the scanning process finds a network__.

::: tip
To cancel the listener, use `wifi.onscanned = null`.
:::

#### Arguments

This method requires a callback `function` with an `object` as argument that has the following member:
- `wifiList` (`array`). List of `objects` with information about the networks found. Every entry has the following members:
  - `SSID` (`string`). The SSID of the network.
  - `BSSID` (`string`). The BSSID of the network.
  - `secure` (`boolean`). Flag that indicates if the network hotspot is secured (`true`) or not.
  - `signalStrength` (`number`). Quality of the Wi-Fi signal. The value is an `integer` between `–100` and `0`, indicating the dBm of the signal. 

Example:

``` js 
wifi.onscanned = function(data) { 
    console.log('scanned result data = '+ JSON.stringify(data));     
}
```

### `onstatechanged({SSID,BSSID,secure,signalStrength,state})`

__Method to be triggered with the state of the network connection changes__.

::: tip
To cancel the listener, use `wifi.onstatechanged = null`.
:::

#### Arguments

This method requires a callback `function` with an `object` as argument that has the following member:
- `wifiList` (`array`). List of `objects` with information about the networks found. Every entry has the following members:
  - `SSID` (`string`). The SSID of the network.
  - `BSSID` (`string`). The BSSID of the network.
  - `secure` (`boolean`). Flag that indicates if the network hotspot is secured (`true`) or not.
  - `state` (`number`). Indicator of the status of the connection (`1`: connected, `0`: disconnected). 

Example:

``` js 
wifi.onstatechanged = function(data) { 
    console.log(data.state+' ssid = '+data.SSID+', bssid = '+data.BSSID+', secure = '+data.secure+' signalStrength = '+data.signalStrength); 
}
```
