# File Management

__Local file system management.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.file"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import file from '@system.file' 
```

Or

``` js
let file = require("@system.file")
```


## Methods

This service has the following methods:

- [`move({srcUri,dstUri,success,fail,complete})`](#move-srcuri-dsturi-success-fail-complete)
- [`copy({srcUri,dstUri,success,fail,complete})`](#copy-srcuri-dsturi-success-fail-complete)
- [`list({uri,success,fail,complete})`](#list-uri-success-fail-complete)
- [`get({uri,recursive,success,fail,complete})`](#get-uri-recursive-success-fail-complete)
- [`delete({uri,success,fail,complete})`](#delete-uri-success-fail-complete)
- [`writeText({uri,text,encoding,append,success,fail,complete})`](#writetext-uri-text-encoding-append-success-fail-complete)
- [`writeArrayBuffer({uri,buffer,position,append,success,fail,complete})`](#writearraybuffer-uri-buffer-position-append-success-fail-complete)
- [`readArrayBuffer({uri,position,length,success,fail,complete})`](#readarraybuffer-uri-position-length-success-fail-complete)
- [`readText({uri,encoding,success,fail,complete})`](#readtext-uri-encoding-success-fail-complete)
- [`access({uri,success,fail,complete})`](#access-uri-success-fail-complete)
- [`mkdir({uri,recursive,success,fail,complete})`](#mkdir-uri-recursive-success-fail-complete)
- [`rmdir({uri,recursive,success,fail,complete})`](#rmdir-uri-recursive-success-fail-complete)
- [`copyFileToPublicDir({srcPath,destPath,success,fail,complete})`](#copyfiletopublicdir-srcpath-destpath-success-fail-complete)

### `move({srcUri,dstUri,success,fail,complete})`

__This method moves a source file to a specified location__.

#### Arguments

This method requires an `object` with the following attributes:
- `srcUri` (`string`). Mandatory attribute with the URI of the source file. It cannot be a path of an app resource or a temporary resource.
Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `dstUri` (`string`). Mandatory attribute with the URI of the target file. It cannot be a path of an app resource or a temporary resource.
Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `success`	(`function(uri)`). Optional callback function corresponding to the successful execution. The function has `uri` (`string`) as an agument with the destination URI.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
file.move({ 
  srcUri: "internal://cache/path/to/file", 
  dstUri: "internal://files/path/to/file", 
  success: function(uri) { 
    console.log("move success: " + uri); 
  }, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```

### `copy({srcUri,dstUri,success,fail,complete})`

__This method copies a source file into a specified location__.

#### Arguments

This method requires an `object` with the following attributes:
- `srcUri` (`string`). Mandatory attribute with the URI of the source file. It cannot be a path of an app resource or a temporary resource.
Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `dstUri` (`string`). Mandatory attribute with the URI of the target file. It cannot be a path of an app resource or a temporary resource.
Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `success`	(`function(uri)`). Optional callback function corresponding to the successful execution. The function has `uri` (`string`) as an argument with the destination URI.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
file.copy({ 
  srcUri: "internal://cache/path/to/file", 
  dstUri: "internal://files/path/to/file", 
  success: function(uri) { 
    console.log("copy success: " + uri); 
  }, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```

### `list({uri,success,fail,complete})`

__This method gets a list of files in a specific directory__.

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the URI of the directory. Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The function has an `object` as an argument with a unique member `fileList`, an `array` of `objects` with the following members:
  - `uri` (`string`). File URI, which can be accessed by other components or services. Read more about [references and URIs](../guide/directory.html#references-and-uris).
  - `length` (`number`). File size, in bytes.
  - `lastModifiedTime` (`number`). Timestamp for file storage, which is the number of milliseconds elapsed since `1970-01-01 00:00:00 GMT`.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
file.list({ 
  uri: "internal://files/movies/" 
  success: function(data) { 
    console.log(data.fileList) 
  }, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```

### `get({uri,recursive,success,fail,complete})`

__This method gets information about a specific local file__.

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the URI of the file. Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `recursive` (`boolean`). Optional attribute that indicates if the method must get information about the list in a subdirectory (`false` by default).
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The function has an `object` as an argument with a unique member `fileList`, an `array` of `objects` with the following members:
  - `uri` (`string`). File URI, which can be accessed by other components or services. Read more about [references and URIs](../guide/directory.html#references-and-uris).
  - `length` (`number`). File size, in bytes.
  - `lastModifiedTime` (`number`). Timestamp for file storage, which is the number of milliseconds elapsed since `1970-01-01 00:00:00 GMT`.
  - `type` (`string`). File type (`dir` for directory and `file` for file).
  - `subFiles` (`array`). File list. When recursive is set to true and type is set to `dir`, the file information in the subdirectory will be recursively returned. Otherwise, no value will be returned.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.


Example:

``` js 
// get a file
file.get({ 
  uri: "internal://cache/test/hey1.txt" 
  success: function(data) { 
    console.log(data.fileList) 
    // {"lastModifiedTime":1637239299000,"length":103,"type":"file","uri":"internal://cache/test/hey1.txt"}
  }, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})

// get a directory
file.get({ 
  uri: "internal://cache/test",
  recursive: true,
  success: function(data) { 
    console.log(data.fileList) 
    // {"subFiles":[{"lastModifiedTime":1637237898000,"length":10,"type":"file","uri":"internal://cache/test/h1.txt"},{"lastModifiedTime":1637238055000,"length":3,"type":"file","uri":"internal://cache/test/h2.txt"},{"lastModifiedTime":1637239430000,"length":2,"type":"file","uri":"internal://cache/test/hey1.txt"}],"lastModifiedTime":1637239430000,"length":15,"type":"dir","uri":"internal://cache/test"}
  }, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```

### `delete({uri,success,fail,complete})`

__This method deletes a specific local file__.

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the URI of the file. Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `success`	(`function`). Optional callback function corresponding to the successful execution. 
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.


Example:

``` js 
file.delete({ 
  uri: 'internal://files/path/to/file', 
  success: function(data) { 
    console.log("handling success"); 
  }, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```

### `writeText({uri,text,encoding,append,success,fail,complete})`

__This method writes text into a specific local file__.

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the URI of the local file. Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `text` (`string`). Mandatory attribute with the text to be written into the local file.
- `encoding` (`string`). Mandatory attribute with the encoding format (`UTF-8` by default).
- `append`	(`boolean`)	(`false` by default) Optional attribute that specifies if the text will be appended to the file if it exists.
- `success`	(`function`). Optional callback function corresponding to the successful execution. 
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
file.writeText({ 
    uri: 'internal://cache/test/test.txt', 
    text: 'This is the text to test the method', 
    encoding: 'UTF-8', 
    success: function() { 
        console.log('success');                
    }, 
    fail: function(errmsg, errcode) { 
        console.log('failed ' + errmsg+' errcode = '+errcode); 
   } 
})
```

### `writeArrayBuffer({uri,buffer,position,append,success,fail,complete})`

__This method writes content into a buffer__.

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the URI of the local file. Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `buffer` (`Uint8Array`). Mandatory attribute with the content to be written into the local file.
- `position` (`number`). Optional attribute with the position of the buffer where the writing starts. If the value of `position` exceeds the length of the file, the file will be extended to the length required by `position`. The value by default is `0`.
- `append`	(`boolean`)	(`false` by default) Optional attribute that specifies if the text will be appended to the file if it exists.
- `success`	(`function`). Optional callback function corresponding to the successful execution. 
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
file.writeArrayBuffer({ 
    uri: 'internal://cache/test/test1.txt', 
    buffer: uint8array, 
    position: 0, 
    success: function() { 
        console.log('success '); 
    }, 
    fail: function(errmsg, errcode) { 
        console.log('failed ' + errmsg+' errcode = '+errcode); 
    } 
})
```

### `readArrayBuffer({uri,position,length,success,fail,complete})`

__This method reads a file content into a buffer__.

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the URI of the local file. Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `position` (`number`). Optional attribute with the position to start reading. The value by default is `0`.
- `length`	(`number`). Optional attribute that specifies if the length of the content to be read. All content of the file will be read by default.
- `success`	(`function`). Optional callback function corresponding to the successful execution. This function has an argument, an `object` that have the following member:
  - `buffer` (`Uint8Array`). Buffer that contains the content of the file. 
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
  - `301`: The file does not exist.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
file.readArrayBuffer({ 
    uri: 'internal://cache/test/test1.txt', 
    position:0, 
    length: 3, 
    success: function(data) { 
        console.log("data buffer  = "+data.buffer) 
    }, 
    fail: function(errmsg, errcode) { 
        console.log('failed ' + errmsg+' errcode = '+errcode); 
    } 
})
```

### `readText({uri,encoding,success,fail,complete})`

__This method reads the text from a local file__.

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the URI of the local file. Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `encoding` (`string`). Mandatory attribute with the encoding format (`UTF-8` by default).
- `success`	(`function`). Optional callback function corresponding to the successful execution. This function has an argument, an `object` that have the following member:
  - `text` (`string`). Mandatory attribute with the text of the local file.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
  - `301`: The file does not exist.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
file.readText({ 
    uri: 'internal://cache/test/test.txt', 
    encoding: 'UTF-8', 
    success: function(data) { 
        console.log('success data.text = '+data.text); 
    }, 
    fail: function(errmsg, errcode) { 
        console.log('failed ' + errmsg+' errcode = '+errcode); 
    } 
})
```

### `access({uri,success,fail,complete})`

__This method checks if a local file or directory exists__.

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the URI of the local file (not a temporary file or an app resource). Read more about [references and URIs](../guide/directory.html#references-and-uris).
- `success`	(`function`). Optional callback function corresponding to the successful execution.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
file.access({ 
  uri: 'internal://files/test', 
  success: function () { 
    console.log('handling success') 
  }, 
  fail: function (data, code) { 
    console.log(`handling fail, code `) 
  } 
})
```

### `mkdir({uri,recursive,success,fail,complete})`

__This method creates local directories__.

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the URI of the local directory.
- `recursive` (`boolean`). Optional attribute that indicates if the system must create the parent directories if they don't exist. The default value is `false`.
- `success`	(`function`). Optional callback function corresponding to the successful execution.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
file.mkdir({ 
  uri: 'internal://files/dir/', 
  success: function () { 
    console.log('handling success') 
  }, 
  fail: function (data, code) { 
    console.log(`handling fail`) 
  } 
})
```

### `rmdir({uri,recursive,success,fail,complete})`

__This method removes a local directory__.

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the URI of the local directory.
- `recursive` (`boolean`). Optional attribute that indicates if the system must delete also the sub-directories recursively. The default value is `false`.
- `success`	(`function`). Optional callback function corresponding to the successful execution.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
file.rmdir({ 
  uri: 'internal://files/dir/', 
  success: function () { 
    console.log('handling success') 
  }, 
  fail: function (data, code) { 
    console.log(`handling fail`) 
  } 
})
```

### `copyFileToPublicDir({srcPath,destPath,success,fail,complete})`

__This method copies a in-app file to a public system directory in the device__.

::: warning Permissions required
This method requires users to grant the access to the local storage.
:::

#### Arguments

This method requires an `object` with the following attributes:
- `srcPath` (`string`). Mandatory attribute with the local path of the source file.
- `destPath` (`string`). Mandatory attribute with the the path of the target file, in the format `external://DIRECTORY_TYPE/xxx/yyy.zzz` (e.g., `external://DIRECTORY_DOCUMENTS/test/test.pdf`). The options for `DIRECTORY_TYPE` are:	`DIRECTORY_MUSIC`, `DIRECTORY_PICTURES`, `DIRECTORY_MOVIES`, `DIRECTORY_DCIM`, `DIRECTORY_DOCUMENTS`.
- `success`	(`function`). Optional callback function corresponding to the successful execution.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `200`: General error.
  - `202`: Invalid parameter.
  - `300`: I/O error.
  - `302`: A file already exists.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
file.copyFileToPublicDir({ 
    srcPath: '/Common/test.mp3', 
    destPath: 'external://DIRECTORY_DOCUMENTS/test.mp3', 
    success: function (ret) { 
      console.log("copy success: " + ret); 
    }, 
    fail: function (data, code) {           
      console.log("handling fail, code=" + code)        
    } 
})
```
