# Text to Speech (TTS)

__Text to speech (TTS) functions.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.tts"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import tts from '@system.tts' 
```

Or

``` js
let tts = require("@system.tts")
```


## Methods

This service has the following method:

- [`speak({text,queueMode,volume,success,fail,complete})`](#speak-text-queuemode-volume-success-fail-complete)

### `speak({text,queueMode,volume,success,fail,complete})`

__This method speaks the specified text__.

#### Arguments

This method requires an `object` with the following attributes:
- `text` (`string`). Mandatory attribute with the content to be spoken.
- `queueMode` (`number`). Optional attribute with the mode of the queue (`1` for add to the queue and `0` to flush the queue). The default value is `1` (add). 
- `volume` (`number`). Optional attribute with the audio volume from `0.0` (silence) to `1.0` (highest). By default the system volume will be used.
- `success`	(`function(res)`). Optional callback function for success.
- `fail` (`function(code)`). Optional callback function for failure, with the potential codes:
  - `200` General error.
  - `202`	Invalid parameter.
  - `1001`	Network connection error.
  - `1002`	Network connection timed out.
  - `1003`	Voice data download is not completed.
  - `1004`	Failed to play the audio on an audio device.
  - `1005`	The TTS service failed.
  - `1006`	Text to speech failed.
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
tts.speak ({ 
    text: 'Testing, testing', 
    volume: 1, 
    queueMode: 0, 
        success: function(data) { 
            console.log("handling success"); 
        }, 
        fail: function(data, code) { 
            console.log("handling fail, code=" + code); 
        } 
})
```
