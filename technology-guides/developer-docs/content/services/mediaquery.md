# Media Queries

__Media Query matching and listeners.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.mediaquery"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import mediaquery from '@system.mediaquery' 
```

Or

``` js
let mediaquery = require("@system.mediaquery")
```


## Methods

This service has the following method:

- [`matchMedia(string)`](#matchmedia-string)

### `matchMedia(string)`

__Method to create a media query__.

#### Arguments

This method have the following arguments:
- `condition` (`string`). Mandatory attribute with the condition to match.


#### Return 

This method returns an `object` with the following members:
- `matches` (`boolean`). Read-only attribute that indicates if the system found data based on the current query condition (`true`). 
- `media` (`string`). Read-only attribute with the serialized media query condition.
- `onchange` (`function`). Listener for events that will be triggered once the matching data change.

Example:

``` js 
const mql = mediaquery.matchMedia('(max-width: 600)');
```

You can also bind event listeners to this event with `addListener(function)`.

For instance:

``` js 
const mql = mediaquery.matchMedia('(max-width: 600)');

function screenTest(e) { 
  if (e.matches) { 
    // change element style 
  } else { 
    // change element style 
  } 
} 
mql.addListener(screenTest);
```

You can also remove the event listener with `removeListener(function)`.

For instance:

``` js 
mql.removeListener(screenTest);
```
