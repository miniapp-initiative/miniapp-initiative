# Fetch

__Get content from the network.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.fetch"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import fetch from '@system.fetch' 
```

Or

``` js
let fetch = require("@system.fetch")
```


## Methods

This service has the following method:

- [`fetch({url,data,header,method,responseType,success,fail,complete})`](#fetch-url-data-header-method-responsetype-success-fail-complete)

### `fetch({url,data,header,method,responseType,success,fail,complete})`

__This method allows you to fetch data from the network__.

::: tip Timeout configuration 
You can configure the network timeout in the [`config.network` member](../guide/manifest.html#network) of the manifest.
:::

#### Arguments

This method requires an `object` with the following attributes:
- `url` (`string`). Mandatory attribute with the URL of the resource to fetch.
- `data` (`string`|`object`|`ArrayBuffer`). Optional parameter with the data sent in the call.
- `header` (`object`). Optional parameter with the HTTP header of the request, which includes all the information of the request. Example: `{"Accept-Encoding": "gzip, deflate","Accept-Language": "en-US,fr-FR;q=0.8,en;q=0.6","Referer":"https://quick-app-initiative.ow2.io/index.html"}`
- `method` (`string`). Optional attribute with the HTTP method: `OPTIONS`, `GET` (default value), `HEAD`, `POST`, `PUT`, `DELETE`, `TRACE`, and `CONNECT`. 
- `responseType` (`string`). Optional attribute with the type of the response: `text`, `json`, `file`, and `ArrayBuffer`. By default, the response type is determined based on the `Content-Type` in the header returned by the server.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `code` (`number`). Server status code.
  - `data` (`string`|`object`|`ArrayBuffer`|`json`). Response data.
  - `headers` (`object`). HTTP headers in the response from the server. 
- `fail` (`function`). Optional callback function corresponding to the failed execution.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.


Relationship between `data` and the `Content-Type`:

| `data` | `Content-Type` | Description |
| ------ | -------------- | ----------- |
| `string` | (not set) | The default value of `Content-Type` is `text/plain`, and the value of `data` is used as the body of the request. |
| `string` | (Any type) |  The value of `data` is used as the request body. |
| `object` | (not set) | The default value of `Content-Type` is `application/x-www-form-urlencoded`. |
| `object` | `application/x-www-form-urlencoded` | The value of `data` is encoded based on the URL rule and is used as the request body. |
| `object` | (Any type other than `application/x-www-form-urlencoded`) | `data` is converted into a character string as the request body. |
| `ArrayBuffer` | (not set) | The default value of `Content-Type` is `application/octet-stream`, and the value of `data` is used as the request body. |
| `ArrayBuffer` | (any type) | The value of `data` is used as the request body. |


Relationship between `responseType` and `data` in `success`:

| `responseType` | `data` | Description |
| -------------- | ------ | ----------- |
| N/A | `string` | If the value of type in the header returned by the server is `text/*`, `application/json`, `application/javascript`, or `application/xml`, the value is `text`. Otherwise, the value is the URI of a temporary file. |
| `text` | `string` | A text is returned. |
| `json` | `object` | A JavaScript object is returned. |
| `file` | `string` | The URI of a stored temporary file is returned.  | 
| `ArrayBuffer` | `ArrayBuffer` | An ArrayBuffer object is returned. |


Example:

``` js 
fetch.fetch({ 
  url:"https://www.example.org", 
  success:function(res){ 
    console.log("title: " + JSON.parse(res.data).title); 
  }, 
  fail: function(res, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```

::: tip Security requirements

For HTTPS requests, the HTTPS certificate must be valid. The certificate must be trusted by the system and does not support self-signed certificates. The domain name of the website where the SSL certificate is deployed must be the same as the domain name for which the certificate is issued. The certificate must be within the validity period.

:::

## Example

``` html
<template> 
    <div> 
        <div> 
            <text>Fetch Example</text> 
        </div> 
        <div> 
            <div> 
                <text>{{fetchData}}</text> 
                <image show="{{showVar}}" src="{{photoUri}}"></image> 
            </div> 
            <input type="button" onclick="refresh" value="Update the GET instance." /> 
        </div> 
        <div> 
            <div> 
                <text>{{fetchData1}}</text> 
            </div> 
            <input type="button" onclick="refresh1" value="Update the arraybuffer POST instance." /> 
            <input type="button" onclick="refresh2" value="Update the json POST instance." /> 
             <input type="button" onclick="refresh3" value="Update the text POST instance." /> 
        </div> 
    </div> 
</template> 
<style> 
    @import '../Common/css/common.css'; 
    .image { 
        margin: 50px 0px; 
        width: 600px; 
        height: 140px; 
    } 
    .item-container { 
        margin-bottom: 50px; 
        margin-right: 60px; 
        margin-left: 60px; 
        flex-direction: column; 
    } 
    .item-content { 
        flex-direction: column; 
        background-color: #ffffff; 
        padding: 30px; 
        margin-bottom: 50px; 
        align-items: center; 
        justify-content: center; 
    } 
    .txt { 
        line-height: 45px; 
        padding-top: 15px; 
        padding-bottom: 15px; 
    } 
</style> 
<script> 
    import fetch from '@system.fetch' 
    import prompt from '@system.prompt' 
    export default { 
        data: { 
            fetchData: '', 
            fetchData1: '', 
            photoUri: '', 
            showVar: true, 
        }, 
        postarraybuffer:function(){ 
            var that = this; 
            var buffer = new ArrayBuffer(4); 
            var a = new Uint16Array(buffer); 
            a[0] = 1; 
            a[1] = 2; 
            fetch.fetch({ 
                url: "https://httpbin.org/post", 
                data:buffer, 
                responseType:'arraybuffer', 
                method:"POST", 
                success: function (ret) { 
                    var buf1 = ret.data; 
                    that.fetchData1 = "Obtain data: " + JSON.stringify(String.fromCharCode.apply(null, new Uint8Array(buf1))); 
                    that.showVar = true; 
                    prompt.showToast({ 
                        message: "success" 
                    }) 
                }, 
                fail: function (msg, code) { 
                    console.log(msg, code); 
                }, 
                complete: function () { 
                    console.log("complete"); 
                } 
            }) 
        }, 
        postjson:function(){ 
            var that = this; 
            var str = '{"username":"123","password":"123"}'; 
            fetch.fetch({ 
                url: "https://httpbin.org/post", 
                data:JSON.parse(str), 
                header:{"User-Agent": "Mozilla/5.0 (Linux; U; Android 7.0; fr-FR; STF-AL00 Build/HUAWEISTF-AL00) AppleWebKit/537.36 (KHTML, like Gecko)Version/4.0 Chrome/37.0.0.0 MQQBrowser/7.9 Mobile Safari/537.36","Accept-Encoding": "gzip, deflate","Accept-Language": "fr-FR,en-US;q=0.8,en;q=0.6"}, 
                responseType:'json', 
                method:"POST", 
                success: function (ret) { 
                    that.fetchData1 = "Obtain data: " + JSON.stringify(ret.data); 
                    that.showVar = true; 
                    prompt.showToast({ 
                        message: "success" 
                    }) 
                }, 
                fail: function (msg, code) { 
                    console.log(msg, code); 
                }, 
                complete: function () { 
                    console.log("complete"); 
                } 
            }) 
        }, 
         poststring:function(){ 
            var that = this; 
            var str = '"name"="user"&"password":"123"'; 
            fetch.fetch({ 
                url: "https://httpbin.org/post", 
                responseType:'text', 
                data:str, 
                method:"POST", 
                success: function (ret) { 
                    that.fetchData1 = "Obtain data: " + JSON.stringify(ret.data); 
                    that.showVar = true; 
                    prompt.showToast({ 
                        message: "success" 
                    }) 
                }, 
                fail: function (msg, code) { 
                    console.log(msg, code); 
                }, 
                complete: function () { 
                    console.log("complete"); 
                } 
            }) 
        }, 
        getphoto: function () { 
            var that = this; 
            fetch.fetch({ 
                url: "https://quick-app-initiative.ow2.io/img/avatar-icon.png", 
                success: function (res) { 
                    that.fetchData = "Getting data: " + JSON.stringify(res.data);
                    that.photoUri = res.data; 
                    that.showVar = true; 
                    prompt.showToast({ 
                        message: "success" 
                    }) 
                }, 
                fail: function (msg, code) { 
                    console.log(msg, code); 
                }, 
                complete: function () { 
                    console.log("complete"); 
                } 
            }) 
        }, 
        refresh: function () { 
            this.getphoto(); 
        }, 
        refresh1: function () { 
            this.postarraybuffer(); 
        }, 
        refresh2: function () { 
            this.postjson(); 
        }, 
        refresh3: function () { 
            this.poststring(); 
        } 
    } 
</script>

```