# Sensors

__Management of device's sensors__ (accelerometer, compass, proximity, light, step counter...).

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.sensor"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import sensor from '@system.sensor' 
```

Or

``` js
let sensor = require("@system.sensor")
```


## Methods

This service has the following methods:

- [`subscribeAccelerometer({interval,callback})`](#subscribeaccelerometer-interval-callback)
- [`unsubscribeAccelerometer()`](#unsubscribeaccelerometer)
- [`subscribeCompass({callback})`](#subscribecompass-callback)
- [`unsubscribeCompass()`](#unsubscribecompass)
- [`subscribeProximity({callback})`](#subscribeproximity-callback)
- [`unsubscribeProximity()`](#unsubscribeproximity)
- [`subscribeLight({callback})`](#subscribelight-callback)
- [`unsubscribeLight()`](#unsubscribelight)
- [`subscribeStepCounter({callback})`](#subscribestepcounter-callback)
- [`unsubscribeStepCounter()`](#unsubscribestepcounter)

### `subscribeAccelerometer({interval,callback})`

__Listens to gravity sensor data.__

If you call this method multiple times, only the last one will take effect.

#### Arguments

This method requires an `object` with the following attributes:
- `interval` (`string`). Optional attribute with the frequency to run the listener function. The possible values are:
  - `game`: every 20 ms.
  - `ui`: every 60 ms.
  - `normal`: every 200 ms (by default).
- `callback` (`function({x,y,z})`). Mandatory attribute with the callback function after the sensor values changes. This function is executed every 200 ms. The function has an `object` argument with the following attributes:
  - `x` (`number`): X coordinate.
  - `y` (`number`): Y coordinate.
  - `z` (`number`): Z coordinate.

Example:

``` js 
sensor.subscribeAccelerometer({ 
    interval: "normal", 
    callback:function(ret){console.log(`[${ret.x}, ${ret.y}, ${ret.z}]`);} 
})
```


### `unsubscribeAccelerometer()`

__Cancels listening to gravity sensor data.__

Example:

``` js 
sensor.unsubscribeAccelerometer()
```

### `subscribeCompass({callback})`

__Listens to compass data changes.__

If you call this method multiple times, only the last one will take effect.

#### Arguments

This method requires an `object` with the following attribute:
- `callback` (`function({direction,accuracy})`). Mandatory attribute with the callback function after the sensor values changes. The function has an `object` argument with the following attributes:
  - `direction` (`number`): Decimal with the latest direction in degrees.
  - `accuracy` (`number`): Integer with the current precision of the sensor. Values are: 
    - `-1`: The value is unreliable and the sensor is disconnected.
    - `0`: The value is unreliable and the cause is unknown.
    - `1`: low precision.
    - `2`: medium precision.
    - `3`: high precision.

Example:

``` js 
sensor.subscribeCompass({ 
    callback:function(ret){
        console.log(`[${ret.direction}, ${ret.accuracy}]`);
    } 
})
```

### `unsubscribeCompass()`

__Cancels listening to compass data changes.__

Example:

``` js 
sensor.unsubscribeCompass()
```

### `subscribeProximity({callback})`

__Listens to proximity sensor data changes.__

If you call this method multiple times, only the last one will take effect.

#### Arguments

This method requires an `object` with the following attribute:
- `callback` (`function({distance})`). Mandatory attribute with the callback function after the sensor values changes. The function has an `object` argument with the following attribute:
  - `distance` (`number`): Distance to the device in centimeters.

Example:

``` js 
sensor.subscribeProximity({ 
    callback:function(ret){
        console.log(`[${ret.distance} cm]`);
    } 
})
```

### `unsubscribeProximity()`

__Cancels listening to proximity sensor data changes.__

Example:

``` js 
sensor.unsubscribeProximity()
```

### `subscribeLight({callback})`

__Listens to light sensor data changes.__

If you call this method multiple times, only the last one will take effect.

#### Arguments

This method requires an `object` with the following attribute:
- `callback` (`function({intensity})`). Mandatory attribute with the callback function after the sensor values changes. The function has an `object` argument with the following attribute:
  - `intensity` (`number`): Light intensity, in lux.

Example:

``` js 
sensor.subscribeLight({ 
    callback:function(ret){
        console.log(`[${ret.intensity} lux]`);
    } 
})
```

### `unsubscribeLight()`

__Cancels listening to light sensor data changes.__

Example:

``` js 
sensor.unsubscribeLight()
```

### `subscribeStepCounter({callback})`

__Listens to pedometer sensor data changes.__

If you call this method multiple times, only the last one will take effect.

#### Arguments

This method requires an `object` with the following attribute:
- `callback` (`function({steps})`). Mandatory attribute with the callback function after the sensor values changes. The function has an `object` argument with the following attribute:
  - `steps` (`number`): Number of steps counted by the sensor.

Example:

``` js 
sensor.subscribeStepCounter({ 
    callback:function(ret){
        console.log(`Steps: ${ret.steps}`);
    } 
})
```

### `unsubscribeStepCounter()`

__Cancels listening to pedometer sensor data changes.__

Example:

``` js 
sensor.unsubscribeStepCounter()
```

::: tip Note:
If you don't call this method, listening will automatically stop when the user closes the quick app.
:::