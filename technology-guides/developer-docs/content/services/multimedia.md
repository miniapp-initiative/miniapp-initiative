# Multimedia

__Service to manipulate the camera and media resources.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.media"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import media from '@system.media' 
```

Or

``` js
let media = require("@system.media")
```

## Methods

This service has the following method:

- [`takePhoto({cancel,success,fail,complete})`](#takephoto-cancel-success-fail-complete)
- [`takeVideo({maxDuration,cancel,success,fail,complete})`](#takevideo-maxduration-cancel-success-fail-complete)
- [`saveToPhotosAlbum({uri,success,fail,complete})`](#savetophotosalbum-success-fail-complete)
- [`pickFile({cancel,success,fail,complete})`](#pickfile-cancel-success-fail-complete)
- [`pickFiles({cancel,success,fail,complete})`](#pickfiles-cancel-success-fail-complete)
- [`pickImage({cancel,success,fail,complete})`](#pickimage-cancel-success-fail-complete)
- [`pickImages({cancel,success,fail,complete})`](#pickimages-cancel-success-fail-complete)
- [`setRingtone({uri,type,title,success,fail,complete})`](#setringtone-uri-type-title-success-fail-complete)
- [`getRingtone({type,success,fail,complete})`](#getringtone-type-success-fail-complete)
- [`pickVideo({cancel,success,fail,complete})`](#pickvideo-cancel-success-fail-complete)
- [`pickVideos({cancel,success,fail,complete})`](#pickvideos-cancel-success-fail-complete)
- [`previewImage({uris,current,success,fail,complete})`](#previewimage-uris-current-success-fail-complete)

### `takePhoto({cancel,success,fail,complete})`

__This method allows you to take a photo using the device's camera__.

#### Arguments

This method requires an `object` with the following attributes:
- `cancel` (`function`). Optional callback function to be executed when the process is cancelled.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `uri` (`string`). URI of the file generated.
  - `name` (`string`). Name of the file generated.
  - `size` (`string`). Size of the file in bytes. 
- `fail` (`function(mes, code)`). Optional callback function corresponding to the failed execution. The possible code is:
  - `201`. The user has rejected the request for the camera permission.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.takePhoto({   
    success: function(data) { 
        console.log("handling success: " + data.uri)
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code)
    } 
})
```

### `takeVideo({maxDuration,cancel,success,fail,complete})`

__This method allows you to record a video using the device's camera__.

#### Arguments

This method requires an `object` with the following attributes:
- `maxDuration` (`number`). Optional argument with the maximum duration of the video, in seconds. The value by default is `60`.
- `cancel` (`function`). Optional callback function to be executed when the process is cancelled.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `uri` (`string`). URI of the file generated.
  - `name` (`string`). Name of the file generated.
  - `size` (`string`). Size of the file in bytes. 
- `fail` (`function(mes, code)`). Optional callback function corresponding to the failed execution. The possible code is:
  - `201`. The user has rejected the request for the camera permission.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.takeVideo({ 
    maxDuration: 10, 
    success:function(data){
        console.log("handling success: " + data.uri)
    },
    fail: function(data, code) { 
        console.log("handling fail, code=" + code)
    } 
})
```

### `saveToPhotosAlbum({uri,success,fail,complete})`

__This method allows you to image or video files to the device's album__.

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the URI of the source file.
- `success`	(`function`). Optional callback function corresponding to the successful execution. 
- `fail` (`function(mes, code)`). Optional callback function corresponding to the failed execution. The possible codes are:
  - `201`. The user has rejected the request for the access to the filesystem.
  - `202`. Invalid parameter.
  - `300`. I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.saveToPhotosAlbum({ 
    uri: 'internal://cache/123.png', 
    success:function(data){
        console.log("save success")
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code)
    } 
})
```

### `pickFile({cancel,success,fail,complete})`

__This method allows you to select a file from the device's filesystem__.

#### Arguments

This method requires an `object` with the following attributes:
- `cancel` (`function`). Optional callback function to be executed when the process is cancelled.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `uri` (`string`). URI of the file selected.
  - `name` (`string`). Name of the file selected.
  - `size` (`string`). Size of the file in bytes. 
- `fail` (`function`). Optional callback function corresponding to the failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.pickFile({ 
    success: function (data) { 
        console.log("handling success: " + data.uri) 
    }, 
    fail: function (data, code) { 
        console.log("handling fail, code=" + code) 
    } 
})
```

### `pickFiles({cancel,success,fail,complete})`

__This method allows you to select multiple files from the device's filesystem__.

#### Arguments

This method requires an `object` with the following attributes:
- `cancel` (`function`). Optional callback function to be executed when the process is cancelled.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `uri` (`array`). URIs (`string`) of the files selected.
- `fail` (`function`). Optional callback function corresponding to the failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.pickFiles({ 
    success: function (data) { 
        console.log("handling success: " + data.uris) 
    }, 
    fail: function (data, code) { 
        console.log("handling fail, code=" + code) 
    } 
})
```

### `pickImage({cancel,success,fail,complete})`

__This method allows you to select an image from the device's filesystem__.

#### Arguments

This method requires an `object` with the following attributes:
- `cancel` (`function`). Optional callback function to be executed when the process is cancelled.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `uri` (`string`). URI of the image selected.
  - `name` (`string`). Name of the image selected.
  - `size` (`string`). Size of the image in bytes. 
- `fail` (`function`). Optional callback function corresponding to the failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.pickImage({ 
    success:function(data) {
        console.log("handling success: " + data.uri)
    } 
})
```

### `pickImages({cancel,success,fail,complete})`

__This method allows you to select multiple images from the device's filesystem__.

#### Arguments

This method requires an `object` with the following attributes:
- `cancel` (`function`). Optional callback function to be executed when the process is cancelled.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `uris` (`array`). List of URIs of the images selected.
  - `files` (`array`). List of the selected images (file objects).
- `fail` (`function`). Optional callback function corresponding to the failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.pickImages({ 
    success:function(data) {
        console.log("handling success: " + data.uris)
    } 
})
```

### `setRingtone({uri,type,title,success,fail,complete})`

__This method allows you to set a ringtone on the device__.

::: tip Note
Every time you call this method, the user will be prompted with an authorization request pop-up.
:::

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the name of the ringtone file path (only local paths supported).
- `type` (`string`). Mandatory attribute with the type of ringtone. Types are: `ringtone`, `notification`, and `alarm`.
- `title` (`string`). Optional attribute with the name of the ringtone. The system uses the file name by default.
- `success`	(`function`). Optional callback function corresponding to the successful execution. 
- `fail` (`function(mes, code)`). Optional callback function corresponding to the failed execution. The possible codes are:
  - `202`. Invalid parameter.
  - `1001`. The file doesn't exist.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.setRingtone({ 
    uri:"/Common/test.mp3", 
    type:"ringtone", 
    success: function () { 
        console.log('media.setRingtone----------success') 
    }, 
    fail: function (erromsg, errocode) { 
        console.log('media.setRingtone----------' + errocode + ': ' + erromsg) 
    }, 
    complete: function () { 
        console.log('media.setRingtone----------complete') 
    } 
})
```

### `getRingtone({type,success,fail,complete})`

__This method allows you to get the ringtone of the device__.

#### Arguments

This method requires an `object` with the following attributes:
- `type` (`string`). Mandatory attribute with the type of ringtone. Types are: `ringtone`, `notification`, and `alarm`.
- `success`	(`function`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following member:
  - `title` (`string`). The name of the ringtone. If the device is muted, `none` is returned.
- `fail` (`function(mes, code)`). Optional callback function corresponding to the failed execution. The possible code is:
  - `202`. Invalid parameter.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.getRingtone({ 
    type:"alarm", 
    success: function (ret) { 
        console.log('media.getRingtone----------success'+ret.title) 
    }, 
    fail: function (erromsg, errocode) { 
        console.log('media.getRingtone----------' + errocode + ': ' + erromsg) 
    }, 
    complete: function () { 
        console.log('media.getRingtone----------complete') 
    } 
})
```

### `pickVideo({cancel,success,fail,complete})`

__This method allows you to select a video from the device's filesystem__.

#### Arguments

This method requires an `object` with the following attributes:
- `cancel` (`function`). Optional callback function to be executed when the process is cancelled.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `uri` (`string`). URI of the video selected.
  - `name` (`string`). Name of the video selected.
  - `size` (`string`). Size of the file in bytes. 
- `fail` (`function`). Optional callback function corresponding to the failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.pickVideo({ 
    success:function(data) {
        console.log("handling success: " + data.uri)
    } 
})
```

### `pickVideos({cancel,success,fail,complete})`

__This method allows you to select multiple videos from the device's filesystem__.

#### Arguments

This method requires an `object` with the following attributes:
- `cancel` (`function`). Optional callback function to be executed when the process is cancelled.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `uris` (`array`). List with the URIs of the selected videos.
  - `files` (`array`). List of file objects with the selected videos.
- `fail` (`function`). Optional callback function corresponding to the failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.pickVideos({ 
    success:function(data) {
        console.log("handling success: " + data.uris)
    } 
})
```

### `previewImage({uris,current,success,fail,complete})`

__This method allows you to preview an image__.

::: tip
Once this method is invoked, the images are previewed in full-screen mode on a new page. 
Users can swipe left and right to preview these images, zoom in and out on the images, and save them into albums.
:::

#### Arguments

This method requires an `object` with the following attributes:
- `uris` (`array`). Mandatory attribute with the list of image paths (`string`s) to be previewed. Both, external and local URIs are supported.
- `current` (`string`). Optional attribute with the path of the image that is currently displayed.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
- `fail` (`function`). Optional callback function corresponding to the failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
media.previewImage({ 
    current: "https://example.org/pic01.jpg", 
    uris: [
        "https://example.org/pic01.jpg", 
        "https://example.org/pic02.jpg", 
        "https://example.org/pic03.jpg", 
        "https://example.org/pic04.jpg", 
        "https://example.org/pic05.jpg", 
        "/common/logo.png", 
    ], 
    success: function () { 
        console.log('previewImage success')
    }, 
    fail: function (erromsg, errocode) { 
        console.log('previewImage errocode:' + errocode)
    }, 
    complete: function () { 
        console.log('previewImage complete')
    } 
})
```

