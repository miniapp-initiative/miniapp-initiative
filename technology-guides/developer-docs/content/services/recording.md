# Audio Recording

__Audio recording functions.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.record"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import record from '@system.record' 
```

Or

``` js
let record = require("@system.record")
```


## Methods

This service has the following methods:

- [`start({duration,sampleRate,numberOfChannels,encodeBitRate,format,success,fail,complete})`](#start-duration-samplerate-numberofchannels-encodebitrate-format-success-fail-complete)
- [`stop()`](#stop)

### `start({duration,sampleRate,numberOfChannels,encodeBitRate,format,success,fail,complete})`

__Start a recording__.

#### Arguments

This method requires an `object` with the following attributes:
- `duration` (`number`). Optional attribute indicating the duration of the recording, in milliseconds. The value by default is `-1`.
- `sampleRate` (`number`). Optional attribute with the sampling rate. The options are: `8000`, `16000`, and `44100`.
- `numberOfChannels` (`number`). Optional attribute with the number of recording channels. The options are `1` (mono) and `2` (stereo).
- `encodeBitRate` (`number`). Optional attribute with the encoding bit rate. 
- `format` (`string`). Optional attribute specifying the audio format. The options are `3gpp`, `amr`, and `aac`. The default value is `3gpp`.
- `success`	(`function(object)`). Optional callback function for success. The function has an `object` argument with the following attributes:
  - `uri` (`string`). Path for storing the recordings, which is in the cache of the app. Recording files are in 3GPP format.
- `fail` (`function(data, code)`). Optional callback function for failure. The method can produce the following code:
  - `201` when the user rejected the request for the recording permission.
- `complete` (`function()`). Optional callback function for completion.

::: tip AAC Sampling Rates
If the format is set to `aac`, the mapping between sampling rates and bit rates is described as follows:

| Sampling Rate | Bit Rate |
| ------------- | -------- |
| 8000 | 16000-48000 |
| 11025 | 16000-48000 |
| 12000 | 24000-64000 |
| 16000 | 24000-64000 |
| 22050 | 32000-128000 |
| 24000 | 32000-128000 |
| 32000 | 48000-192000 |
| 44100 | 64000-320000 |
| 48000 | 64000-320000 |

:::

Example:

``` js 
record.start({ 
    duration: 20000, 
    sampleRate:8000, 
    numberOfChannels:1, 
    encodeBitRate:16000, 
    format:'aac', 
    success: function (ret) { 
        console.log('Storing at ' + ret.uri); 
    }, 
    fail: function (erromsg, errocode) { 
        console.log('record.startRecord: ' + errocode + ': ' + erromsg) 
    }, 
    complete: function () { 
        console.log('record.startRecord complete') 
    } 
})
```

### `stop()`

__Stop a recording__.

Example:

``` js 
record.stop()
```
