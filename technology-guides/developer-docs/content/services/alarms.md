# Alarms

__Device's alarms management.__


## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.alarm"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import alarm from '@system.alarm' 
```

Or

``` js
let alarm = require("@system.alarm")
```


## Methods

This service has the following methods:

- [`getProvider()`](#getprovider)
- [`setAlarm({hour,minute,message,vibrate,days,ringtone,success,fail,complete})`](#setalarm-hour-minute-message-vibrate-days-ringtone-success-fail-complete)

### `getProvider()`

__Get information about the service provider__, usually, the device vendor.

#### Arguments

This method does not have arguments.

#### Return 

This method returns a `string` with the identifier of the service provide. Empty if there is no provider available.  

Example:

``` js 
let provider = alarm.getProvider()
// provider = huawei
```

### `setAlarm({hour,minute,message,vibrate,days,ringtone,success,fail,complete})`

__Set an alarm.__

This method sets an alarm in the device. A warning dialog will be displayed, asking the user for confirmation. 

#### Arguments

This method requires an `object` with the following attributes:
- `hour` (`number`). Mandatory attribute with the alarm time (in hours). The range of the value is `0`-`23`.
- `minute` (`number`). Mandatory attribute with the alarm time (in minutes). The range of the value is `0`-`59`.
- `message`	(`string`). Optional attribute with the name of the alarm (up to 200 characters).
- `vibrate`	(`boolean`). Optional flag that indicates whether to enable vibration (`true`, by default) or not (`false`).
- `days` (`array<number>`). Optional attribute with the recurring pattern of the alarm. The alarm does not repeat by default. For example, `[0,1,2,3,4,5,6]` repeats the alarm every day, `[0,1,2,3,4]` only weekdays, `[0,6]` repeats the alarm on Monday and Sunday.
- `ringtone` (`string`). Optional attribute with the ringtone. The file path may be a data file path or an in-app resource.
- `success`	(`function()`). Optional callback function for success.
- `fail` (`function()`). Optional callback function for failure.
- `complete` (`function()`). Optional callback function on completion, regardless success or failure of the call.


Example:

``` js 
<script>
  import alarm from '@system.alarm' 
  export default {
    onReady () {
      alarm.setAlarm({ 
        hour: 6, 
        minute: 0, 
        message: 'Wake me up!!', 
        vibrate: true, 
        days: [0,1,2,3,4], 
        ringtone: '/Common/audio/ding.m4a', 
        success() { 
          console.log('Success!') 
        }, 
        fail(erromsg, errocode) { 
          console.error(`Set alarm failed, msg: ${erromsg} code: ${errocode}`) 
        }, 
        complete() { 
          console.log('Done!' ) 
        } 
      })
    }, 
  }
</script>
```

The system will show a confirmation dialog like the following.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/alarm01.jpg" alt="Confirmation dialog on a Quick App" /> 




