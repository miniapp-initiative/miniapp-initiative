# ZIP Decompression

__Decompression of zip files.__


## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.zip"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import zip from '@system.zip' 
```

Or

``` js
let zip = require("@system.zip")
```

## Methods

This service has the following method:

- [`decompress(srcUri,dstUri,topSize,topFileNumbers,success,fail,complete)`](#decompress-srcuri-dsturi-topsize-topfilenumbers-success-fail-complete)

### `decompress(srcUri,dstUri,topSize,topFileNumbers,success,fail,complete)`

__Method to decompress a file__.

#### Arguments

This method requires an `object` with the following attributes:

- `srcUri` (`string`). Mandatory attribute with the URI of the file to be decompressed (it cannot be a temporary file).
- `dstUri` (`string`). Mandatory attribute with the URI of the target directory (it cannot be a temporary file or an app resource path).
- `topSize` (`number`). Optional attribute indicating the maximum size after the decompression, in MB.
- `topFileNumbers` (`number`). Optional attribute indicating the maximum number of files after decompression.
- `success`	(`function`). Optional callback function corresponding to the successful execution. 
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
  - `300`: I/O error.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
zip.decompress({ 
    srcUri: 'internal://cache/test.zip', 
    dstUri: 'internal://files/unzip/', 
    success: function () { 
        console.log(`handling success`) 
    }, 
    fail: function (data, code) { 
        console.log(`handling fail`) 
    } 
})
```
