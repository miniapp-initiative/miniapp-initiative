# Page Routing

__Routing of pages and components.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.router"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import router from '@system.router' 
```

Or

``` js
let router = require("@system.router")
```


## Methods

This service has the following methods:

- [`push({uri,params})`](#push-uri-params)
- [`replace({uri,params})`](#replace-uri-params)
- [`back({path,delta})`](#back-path-delta)
- [`clear()`](#clear)
- [`getLength()`](#getlength)
- [`getPages()`](#getpages)
- [`getState()`](#getstate)

### `push({uri,params})`

__This method redirects the user to an in-app page.__

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the target URI, which supports the following formats:
  - A full URI with one of the following schemas: `tel`, `sms`, `mailto`, and `hap` (e.g., `tel:10086`, `mailto:me@example.org`, and `hap://app/org.example.app/index?param1=value1`).
  - An absolute in-app page path (e.g., `/about`).
  - a relative in-app page path (e.g., `about`).
  - `/`. If there is no any page with the path `/`, the home page is used by default.
  - `hap://settings/permission_manager` to access the permission management page of a quick app.
  - `hap://settings/location_source_manager`: The system loads the location management page.

- `params` (`object`). Optional attribute with the data to be passed during the redirection. The target page will be able to access the parameters directly as a member of its instance (e.g., `this.param1`, where `param1` is the name of the parameter name defined in the JSON object. The value of `param1` will be converted into a `string`. The `object` may contain the following reserved members:
  - `___PARAM_LAUNCH_FLAG___` (`string`). Optional member to indicate the launch mode of the page (only `clearTask` value is supported). Read more about [Page Launch Mode](../guide/lifecycle.html#page-launch-mode).
  - `___PARAM_LAUNCH_NATIVE_FLAG___` (`string`). Optional flag to indicate that the URI corresponds to a native app. The default value is `newTask`. You can set multiple flags as required, separating them through the pipeline character (`|`) (e.g., `newTask|clearTask` and `newTask|clearTop`).
    - `newTask`: default behavior of quick app engine, which is the same as the behavior triggered by `Intent.FLAG_ACTIVITY_NEW_TASK` for Android.
    - `standard`: default behavior by using activity. A newly opened page is added to the quick app stack. After a user taps the back button, the user does not exit the quick app. This behavior is the same as that triggered when no flag is carried during activity-based launch on Android.
    - `broughtToFront`: behavior like with `Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT` on Android.
    - `clearTask`: behavior like with `Intent.FLAG_ACTIVITY_CLEAR_TASK` on Android.
    - `singleTop`: behavior like with `Intent.FLAG_ACTIVITY_SINGLE_TOP` on Android.
    - `clearTop`: behavior like with `Intent.FLAG_ACTIVITY_CLEAR_TOP` on Android.
  `___PARAM_PAGE_ANIMATION___` (`object`). Optional attribute to indicate the type of animation for the page redirection.

When the value of the `uri` parameter is a schema-based URI, the system will process the request as follows:
1. Match the proper filter, as specified in the [filter settings](../guide/manifest.html#filter).
2. If the page doesn't exist, the system will use the default policy to process the request (supporting HTTP, HTTPS, and [internal schemas](../guide/directory.html#references-and-uris)).
3. If the default policy cannot process the request, the system will use a system app to process the request.
4. If there is no system app that can process the request, the system will discard the request.

The processing logic of the default policy is as follows:
1. If the schema is HTTP or HTTPS, open the web page in stand alone mode, instead of widget mode.
2. If the schema is an [internal schema](../guide/directory.html#references-and-uris), determine the file type based on the file name extension of the URI, and then launch a system app to open the file.

Example:

``` js 
// launch phone app 
router.push({ 
  uri: 'tel:10086' 
}); 
// open page by path 
router.push({ 
  uri: '/about', 
  params: { testId:'1' } 
}); 
// open page by name 
router.push({ 
  uri: 'About', 
  params: { testId:'1' } 
}); 
// send SMS 
router.push({ 
  uri: 'sms', 
  params: { body:'test send message' } 
});
// open web page 
router.push({ 
  uri: 'https://www.example.org' 
}); 
// install apk 
router.push({ 
  uri: 'internal://cache/example.apk' 
});
// Open page with 'clearTask' flag 
router.push({ 
  uri: '/about', 
  params: {___PARAM_LAUNCH_FLAG___: 'clearTask'} 
}); 
// Animations
router.push({ 
  uri: '/', 
  params: { 
    ___PARAM_PAGE_ANIMATION___: { 
      openEnter: 'slide', 
      closeEnter: 'none', 
      openExit: 'slide', 
      closeExit: 'none' 
    } 
  } 
});
```
Example code for opening two native apps:

``` js
import pkg from '@system.package'; 
// Map navigation 
navMap(address){ 
    if (address == undefined) { 
        return; 
    } 
    // Address conversion 
    var special = "Address 1"; 
    if (address.indexOf(special)>=0) { 
        address = address.substr(0, address.indexOf(special)+ special.length); 
    } 
    special = "Address 2"; 
    if (address.indexOf(special)>=0) { 
        address = address.substr(0, address.indexOf(special)+ special.length); 
    } 
    special = "Address 3"; 
    if (address.indexOf(special)>=0) { 
        address = address.substr(0, address.indexOf(special)+ special.length); 
    } 
    special = "Address 1"; 
    if (address.indexOf(special)>=0) { 
        address = address.substr(0, address.indexOf(special)+ special.length); 
    } 
    special = "Address 2"; 
    if (address.indexOf(special)>=0) { 
        address = address.substr(0, address.indexOf(special)+ special.length); 
    } 
    special = "Address 3"; 
    if (address.indexOf(special)>=0) { 
        address = address.substr(0, address.indexOf(special)+ special.length); 
    } 
    special = "Address 1"; 
    if (address.indexOf(special)>=0) { 
        address = address.substr(0, address.indexOf(special)+ special.length); 
    } 
    special = "Address 2"; 
    if (address.indexOf(special)>=0) { 
        address = address.substr(0, address.indexOf(special)+ special.length); 
    } 
    var listNavApp = {baidu:false,gaode:false}; 
    // Asynchronously detect Baidu Map. 
    this.checkBaidu(listNavApp, address); 
}, 
// Asynchronously detect Baidu Map. 
checkBaidu(navAppObj, address){ 
    var that = this; 
    pkg.hasInstalled({ 
        package: 'com.baidu.BaiduMap', 
        success: function (data) { 
            if (data.result) { 
                navAppObj.baidu = true; 
            } 
            that.checkGaode(navAppObj, address); 
        }, 
        fail: function (data, code) { 
            that.checkGaode(navAppObj, address); 
        } 
    }); 
}, 
// Asynchronously detect AutoNavi Map. 
checkGaode(navAppObj, address){ 
    var that = this; 
    pkg.hasInstalled({ 
        package: 'com.autonavi.minimap', 
        success: function (data) { 
            if (data.result) { 
                navAppObj.gaode = true; 
            } 
            that.onCheckEnd(navAppObj, address); 
        }, 
        fail: function (data, code) { 
            that.onCheckEnd(navAppObj, address); 
        } 
    }); 
}, 
// Display the navigation selection box. 
onCheckEnd(navAppObj, address){ 
    var listNavApp=[]; 
    if(navAppObj.baidu) { 
        listNavApp = listNavApp.concat("Baidu Map"); 
    } 
    if(navAppObj.gaode) { 
        listNavApp = listNavApp.concat("AutoNavi Map"); 
    } 
    if (listNavApp.length == 0) { 
        return; 
    } 
    // show nav list 
    prompt.showContextMenu({ 
        itemList: listNavApp, 
        success: function (data) { 
            // find map url 
            var mapUrl; 
            if (listNavApp[data.index] == 'Baidu Map') { 
                mapUrl = 'baidumap://map/direction?destination='+address+'&coord_type=bd09ll&mode=driving&src=com.huawei.fastapp'; 
            } else if (listNavApp[data.index] == 'AutoNavi Map') { 
                mapUrl = 'amapuri://route/plan?sourceApplication=amap&dname='+address+'&dev=0&style=0'; 
            } else { 
                return; 
            } 
            // start nav app 
            router.push({ 
                uri: mapUrl 
            }) 
        }, 
        cancel: function () { 
            console.log('user cancel'); 
        } 
    }) 
}
```

::: warning
The total size of the `object` attributes cannot exceed 128 KBytes.
:::

::: tip
Data passed during the page redirection can be accessed by other apps. Be careful with when you share sensitive data in the process.
:::


### `replace({uri,params})`

__This method redirects the user to an in-app page, removing the current page from the page stack.__ 

#### Arguments

This method requires an `object` with the following attributes:
- `uri` (`string`). Mandatory attribute with the target URI, which supports the following formats:
  - An absolute in-app page path (e.g., `/about`).
  - a relative in-app page path (e.g., `about`).
  - `/`. If there is no any page with the path `/`, the home page is used by default.
  - `hap://settings/permission_manager` to access the permission management page of a quick app.
  - `hap://settings/location_source_manager`: The system loads the location management page.
-`params` (`object`). Optional attribute with the data to be passed during the redirection. The target page will be able to access the parameters directly as a member of its instance (e.g., `this.param1`, where `param1` is the name of the parameter name defined in the JSON object. The value of `param1` will be converted into a `string`. 


Example:

``` js 
router.replace({ 
  uri: '/test' 
  params: { testId:'1' } 
})
```

::: tip
Data passed during the page redirection can be accessed by other apps. Be careful with when you share sensitive data in the process.
:::

### `back({path,delta})`

__This method redirects the user back to a previous page in the history.__

#### Arguments

If no parameter is specified, the system returns to the previous page (the effect is similar to tapping the back button on a smartphone). 

This method requires an `object` with the following attributes:
- `path` (`string`). Optional attribute with the path of a page. The options are as follows:
  - If this parameter is not set and `delta` is specified, the system will load the page corresponding to `delta`. If `delta` is not set either, the system will load the previous page with the same result as `router.back()`.
  - When this parameter is set to an absolute in-app page (e.g., `/about`), the router loads that page. 
  - If the page specified by path doesn't exist in the app, go back to the previous page.
- `delta` (`number`). Optional attribute with location of the page in the page stack to be loaded. The value by default is `1` (the previous page in the history). If the value of `delta` is greater than the number of existing pages, the system will load the home page. If both `path` and `delta` are specified, `path` will be used. 

::: warning
The `path` parameter cannot specify a page in other apps.
::: 

Example:

``` js 
// A user on page A will be routed to the specified page. 
router.push({ 
    uri: 'B' 
}) 
// A user on page B will be routed to the specified page. 
router.push({ 
    uri: 'C' 
}) 
// A user on page C will be routed to the specified page. 
router.push({ 
    uri: 'D' 
}) 
// A user on page D will be routed to the specified page. 
router.push({ 
    uri: 'E' 
}) 
// No page is passed so a user on page E will go back to page D. 
router.back() 
// No page is passed so a user on page D will go back to page C. 
router.back() 
// /A is passed so a user on page C will go back to page A. 
router.back({ 
    path: '/A' 
})
```

### `clear()`

__This method clears all the pages in the page stack, keeping in the history only the current page.__

Example:

``` js 
router.clear()
```


### `getLength()`

__This method gets the number of pages in the current page stack (length of the history).__

#### Return 

This method returns a `string` with the number of the pages. 

Example:

``` js 
let length= router.getLength()
```

### `getPages()`

__This method returns the list with the current page stack (the history).__

#### Return 

This method returns an `array` of `object` with the stack of pages. Each `object` will contain the following members:
- `name`: a `string` with the name of the page.
- `path`: a `string` with the path of the page.

Example:

``` js 
// With two pages in the stack
console.log(JSON.stringify(router.getPages()))
```

Showing the following message:
``` bash
[
    {"path":"/Index","name":"Index"},
    {"path":"/Component/basic","name":"Component/basic/"}
]
```

### `getState()`

__This method gets the status of the current page.__

#### Return 

This method returns an `object` with information about the state of the page, including:
- `index` (`string`) with the position of the current page in the stack.
- `name` (`string`) with the name of the current page.
- `path` (`string`) with the path of the current page.


Example:

``` js 
console.log(JSON.stringify(router.getState()))
```

Showing the following message on the console:

``` bash
{"path":"/Component/basic","name":"Component/basic","index":1}
```

