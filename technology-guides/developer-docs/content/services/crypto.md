# Cryptographic Algorithms

__Cryptographic operations.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.cipher"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import cipher from '@system.cipher' 
```

Or

``` js
let cipher = require("@system.cipher")
```


## Methods

This service has the following methods:

- [`rsa({action,text,key,transformation,success,fail,complete})`](#rsa-action-text-key-transformation-success-fail-complete)
- [`aes({action,text,key,transformation,iv,ivOffset,ivLen,input,secretKey,initialVector,success,fail,complete})`](#aes-action-text-key-transformation-iv-ivoffset-ivlen-input-secretkey-initialvector-success-fail-complete)
- [`getRandomValues(int)`](#getrandomvalues-int)

### `rsa({action,text,key,transformation,success,fail,complete})`

__Encrypts or decrypts data through RSA__.

#### Arguments

This method requires an `object` with the following attributes:
- `action` (`string`). Mandatory attribute with the operation to perform: `encrypt` for encryption and `decrypt` for decryption.
- `text` (`string`). Mandatory attribute with the text to be encrypted or decrypted. Plain text for encryption, and Base64 representation for decryption. By default, the system uses Base64 encoding.
- `key` (`string`). Mandatory attribute with the RSA key used for encryption or decryption (a string encrypted using Base64). During encryption, this parameter corresponds to the public key. During decryption, it is the private key.
- `transformation` (`string`). Optional attribute with the padding of the RSA algorithm. The default value is `RSA/None/OAEPWithSHA256AndMGF1Padding`.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The function has an `object` as argument with the following member:
  - `text` (`string`) with the text generated after the encryption or decryption. 
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example for encryption:

``` js 
cipher.rsa({ 
     action: 'encrypt', 
    // Text to be encrypted. 
     text: '', 
       transformation: 'RSA/None/OAEPWithSHA256AndMGF1Padding', 
     // Encryption public key after Base64 encoding. 
     key: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDKmi0dUSVQ04hL6GZGPMFK8+d6\n' + 
     'GzulagP27qSUBYxIJfE04KT+OHVeFFb6K+8nWDea5mkmZrIgp022zZVDgdWPNM62\n' + 
     '3ouBwHlsfm2ekey8PpQxfXaj8lhM9t8rJlC4FEc0s8Qp7Q5/uYrowQbT9m6t7BFK\n' + 
     '3egOO2xOKzLpYSqfbQIDAQAB', 
     success: function(data){ 
       console.log("handling success: " + data.text); 
     }, 
     fail: function (erromsg, errocode) { 
       console. log ('cipher.rsa fail--------' + errocode + ': ' + erromsg); 
     } 
   });
```

Example for decryption:

``` js 
var self=this; 
cipher.rsa({ 
     action: 'decrypt', 
    // Content to be decrypted, which is a binary value after Base64 encoding and a text after decryption. 
     text: self.encryptedValue, 
       transformation: 'RSA/None/OAEPWithSHA256AndMGF1Padding', 
     // Private key for decryption after Base64 encoding. 
     key: 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMqaLR1RJVDTiEvo\n' + 
     'ZkY8wUrz53obO6VqA/bupJQFjEgl8TTgpP44dV4UVvor7ydYN5rmaSZmsiCnTbbN\n' + 
     'lUOB1Y80zrbei4HAeWx+bZ6R7Lw+lDF9dqPyWEz23ysmULgURzSzxCntDn+5iujB\n' + 
     'BtP2bq3sEUrd6A47bE4rMulhKp9tAgMBAAECgYBjsfRLPdfn6v9hou1Y2KKg+F5K\n' + 
     'ZsY2AnIK+6l+sTAzfIAx7e0ir7OJZObb2eyn5rAOCB1r6RL0IH+MWaN+gZANNG9g\n' + 
     'pXvRgcZzFY0oqdMZDuSJjpMTj7OEUlPyoGncBfvjAg0zdt9QGAG1at9Jr3i0Xr4X\n' + 
     '6WrFhtfVlmQUY1VsoQJBAPK2Qj/ClkZNtrSDfoD0j083LcNICqFIIGkNQ+XeuTwl\n' + 
     '+Gq4USTyaTOEe68MHluiciQ+QKvRAUd4E1zeZRZ02ikCQQDVscINBPTtTJt1JfAo\n' + 
     'wRfTzA0Lvgig136xLLeQXREcgq1lzgkf+tGyUGYoy9BXsV0mOuYAT9ldja4jhJeq\n' + 
     'cEulAkEAuSJ5KjV9dyb0RIFAz5C8d8o5KAodwaRIxJkPv5nCZbT45j6t9qbJxDg8\n' + 
     'N+vghDlHI4owvl5wwVlAO8iQBy8e8QJBAJe9CVXFV0XJR/n/XnER66FxGzJjVi0f\n' + 
     '185nOlFARI5CHG5VxxT2PUCo5mHBl8ctIj+rQvalvGs515VQ6YEVDCECQE3S0AU2\n' + 
'BKyFVNtTpPiTyRUWqig4EbSXwjXdr8iBBJDLsMpdWsq7DCwv/ToBoLg+cQ4Crc5/\n' + 
'5DChU8P30EjOiEo=', 
     success: function(data){ 
       console.log("handling success: " + data.text); 
     }, 
     fail: function (erromsg, errocode) { 
       console. log ('cipher.rsa fail--------' + errocode + ': ' + erromsg); 
     } 
});
```

### `aes({action,text,key,transformation,iv,ivOffset,ivLen,input,secretKey,initialVector,success,fail,complete})`

__Encrypts or decrypts data through AES__.

#### Arguments

This method requires an `object` with the following attributes:
- `action` (`string`). Mandatory attribute with the operation to perform: `encrypt` for encryption and `decrypt` for decryption.
- `text` (`string`). Optional attribute with the text to be encrypted or decrypted. Plain text for encryption, and Base64 representation for decryption. By default, the system uses Base64 encoding.
- `key` (`string`). Optional attribute with the key used for encryption or decryption (a string encrypted using Base64).
- `transformation` (`string`). Optional attribute with the padding of the RSA algorithm. The default value is `AES/CBC/PKCS5Padding`.
- `iv` (`string`). Optional attribute with the initial vector for AES-based encryption or decryption. The value is a character string encoded using Base64. The value by default is the `key` value.
- `ivOffset` (`number`). Optional attribute with an integer that represents the offset of the initial vector for AES-based encryption or decryption. The value by default is `0`.
- `ivLen` (`number`). Optional attribute an integer that represents the length of the initial vector for AES-based encryption or decryption. The value by default is `16`.
- `input` (`ArrayBuffer`). Optional attribute with the content to be encrypted or decrypted, as an alternative to `text` input.   
- `secretKey` (`ArrayBuffer`). Optional attribute with the key used for encryption or decryption, which is required for processing the content specified by the `input` parameter.
- `initialVector` (`ArrayBuffer`). Optional attribute with the initial vector for AES encryption and decryption, which is required for processing the content specified by the input parameter. The value of `secretKey` will be used by default in case this parameter is not specified.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The function has an `object` as argument with the following members:
  - `text` (`string`) with the text generated after the encryption or decryption. The encrypted content is a Base64 binary code, and the decrypted content is in plain text. If the decrypted content cannot be converted into a `UTF-8` string, the system throws an error.
  - `output` (`ArrayBuffer`) with the encryption or decryption result based on the `input` parameter.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The possible codes are:
  - `202`: Invalid parameter.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

::: tip Usage
If both `text` and `input` are specified, the system will consider the value of `text`.
:::

Example for encryption:

``` js 
cipher.aes({ 
  action: 'encrypt', 
  // Text to be encrypted. 
  text: 'hello', 
  // Key after Base64 encoding. 
  key: 'NDM5Qjk2UjAzMEE0NzVCRjlFMkQwQkVGOFc1NkM1QkQ=', 
  transformation: 'AES/CBC/PKCS5Padding', 
  ivOffset: 0, 
  ivLen: 16, 
  success: (data) => { 
    console.log(`handling success: ${data.text}`) 
  }, 
  fail: (data, code) => { 
    console.log(`### cipher.aes fail ### ${code}: ${data}`) 
  } 
}) 
```

Example for decryption:

``` js 
cipher.aes({ 
  action: 'decrypt', 
  // Content to be decrypted, which is a binary value after Base64 encoding. 
  text: '1o0kf2HXwLxHkSh5W5NhzA==\n', 
  // Key after Base64 encoding. 
  key: 'NDM5Qjk2UjAzMEE0NzVCRjlFMkQwQkVGOFc1NkM1QkQ=', 
  transformation: 'AES/CBC/PKCS5Padding', 
  ivOffset: 0, 
  ivLen: 16, 
  success: (data) => { 
    this.dealTxt = data.text 
  }, 
  fail: (data, code) => { 
    prompt.showToast({ 
      message: ("Decryption failed, code=" + code + ":" + data) 
    }) 
  } 
})
```


### `getRandomValues(int)`

__Method that generates and returns random values__.

#### Arguments

This method requires a `number` with an integer that indicates the number of values to be generated.

#### Return 

This method returns a `string` with the hexadecimal representation of the bytes generated.  

Example:

``` js 
let r16 = cipher.getRandomValues(16); 
console.log(`random16: ${r16}`);
// prints random16: a4db4dde71381174037064c6e1d901e0
```


