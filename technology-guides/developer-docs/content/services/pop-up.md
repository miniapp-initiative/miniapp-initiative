# Dialogs

__User dialogs__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.prompt"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import prompt from '@system.prompt' 
```

Or

``` js
let prompt = require("@system.prompt")
```

## Methods

This service has the following methods:

- [`showToast({message,duration,image,gravity})`](#showtoast-message-duration-image-gravity)
- [`showDialog({title,message,buttons,success,cancel,complete})`](#showdialog-title-message-buttons-success-cancel-complete)
- [`showContextMenu({itemList,itemColor,success,cancel,complete})`](#showcontextmenu-itemlist-itemcolor-success-cancel-complete)
- [`showLoading({message,loadingColor,mask})`](#showloading-message-loadingcolor-mask)
- [`hideLoading()`](#hideloading)

### `showToast({message,duration,image,gravity})`

__This method displays a toast message.__

#### Arguments

This method requires an `object` with the following attributes:
- `message`	(`string`). Mandatory text to display in the toast.
- `duration` (`number`). Optional attribute with the duration of the toast display in milliseconds.
- `image` (`uri`). Optional URI with the path of the local image that will be displayed.
- `gravity` (`string`). Optional attribute with the position of the toast on the screen. The value can be: `bottom`, `right`, `left`, `center`, or `top`.

Example:

``` js
prompt.showToast({ 
    message:'message', 
    duration:"100000", 
    image:"/Common/logo.png", 
    gravity: 'center' 
})
```

### `showDialog({title,message,buttons,success,cancel,complete})`

__This method displays a pop-up dialog.__

#### Arguments

This method requires an `object` with the following attributes:
- `title` (`string`). Optional text with the title of the dialog.  
- `message`	(`string`). Optional text with the content of the dialog.
- `buttons`	(`array`). Optional list containing the buttons (from one to three buttons). If several items are defined, the first one is is a positive button; the second would be a negative button; and the third item would be a neutral button. Each button is an `object` with the following members:
  - `text` (`string`). Mandatory text with the label of the button.  
  - `color` (`string`). Optional background color of the button.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
  - `res.index` is a `number` that indicates the index number in the button sequence of the selected button after interaction.
- `cancel` (`function()`). Optional callback function corresponding to the canceled execution.
- `complete` (`function()`). Optional callback function corresponding to the end of the execution.


Example:

``` js
prompt.showDialog({ 
    title: 'title', 
    message: 'message', 
    buttons: [ 
    { 
        text: 'OK', 
        color: '#33dd44' 
    }, 
    { 
        text: 'Cancel', 
        color: '#33dd44' 
    } 
    ], 
    success: function (data) { 
        console.log("handling callback",data); 
    }, 
    cancel: function(){ 
        console.log("cancel"); 
    } 
})
```

### `showContextMenu({itemList,itemColor,success,cancel,complete})`

__This method displays a context menu.__

#### Arguments

This method requires an `object` with the following attributes:
- `itemList` (`array`). Mandatory `array` of `strings` with the items in the context menu.  
- `itemColor` (`string`). Optional color of the menu items.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
  - `res.index` is a `number` that indicates the index number in the item sequence of the selected context menu item after user's interaction.
- `cancel` (`function()`). Optional callback function corresponding to the canceled execution.
- `complete` (`function()`). Optional callback function corresponding to the end of the execution.


Example:

``` js
prompt.showContextMenu({ 
  itemList: ['item1', 'item2'], 
  itemColor: '#ff33ff', 
  success: function(data) { 
    console.log("Handling callback"); 
  }, 
   cancel: function() { 
    console.log("Cancellation"); 
  } 
})
```

### `showLoading({message,loadingColor,mask})`

__This method displays a loading pop-up dialog.__


#### Arguments

This method requires an `object` with the following attributes:
- `message` (`string`). Optional text with the message to display (`Loading...` by default).
- `loadingColor` (`string`). Optional string with the color of the loading animation (`#CACACA` by default)
- `mask` (`boolean`). Optional flag that indicates whether to use a mask to prevent a touch event on the elements under the loading dialog (`true` by default).

::: tip
To close a loading pop-up, you need to use the [`prompt.hideLoading()` method](#hideloading).
:::

Example:

``` js
prompt.showLoading({ 
    message: 'message' 
    loadingColor: 'blue', 
    mask: false 
})
```

### `hideLoading()`

__This method closes a loading pop-up dialog if it is already being shown.__

Example:

``` js
prompt.hideLoading()
```