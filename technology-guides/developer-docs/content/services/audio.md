# Audio Player

__This service enables the management of an audio player from the app.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.audio"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import audio from '@system.audio' 
```

Or

``` js
let audio = require("@system.audio")
```

## Attributes

This service implements the following attributes:

- `src` (`string`). Mandatory attribute with the URI of the audio media to be played. You can set this parameter with a local path or a network URI of the resource. Read more about [accessing local resources](../guide/directory.html#access-to-resources-paths). 
- `currentTime` (`number`). Optional attribute with the progress of the audio playback (in seconds). This progress can be adjusted setting the value.
- `duration` (`number`). Optional read-oly attribute with the audio duration, in seconds. If the value is `unknown`, the system returns `NaN`. 
- `autoplay` (`boolean`). Optional attribute that indicates the audio will start playing automatically (`false` by default).
- `loop` (`boolean`). Optional attribute that indicates if the audio is played in a loop and repeated once it finishes (`false` by default).
- `volume` (`number`). Optional attribute with the volume of the audio (from `0.0` -muted- to `1.0` -maximum-).
- `muted ` (`boolean`). Optional attribute that indicates whether to mute the audio (`true`) or not (`false`). The value by default is `false`.
- `notificationVisible ` (`boolean`). Optional attribute that indicates if the system must display the status of playback in the notification panel of the device. The default value is `true` (shown).
- `title` (`string`). Optional attribute with the title of the audio title. If this parameter is not set, the system will use the status of the audio playback (playing or paused). The value of this parameter is displayed only when both title and artist are specified.
- `artist` (`string`). Optional attribute with the name of the artist. If this parameter is not set, the system will use the status of the audio playback (playing or paused). The value of this parameter is displayed only when both title and artist are specified.
- `cover` (`string`). Optional attribute with the URI of the cover image that represents the audio. The cover image must have a 1:1 appearance ratio and its dimension must be between `60x60 px` and `120x120 px`.
- `streamType` (`string`). Optional attribute with the type of audio playback. The options are `music` (audio played using the speakers), and `voicecall` (audio played using the earpiece). The default value is `music`.


## Methods

This service has the following methods:

- [`play()`](#play)
- [`pause()`](#pause)
- [`stop()`](#stop)
- [`getPlayState({success,fail,complete})`](#getplaystate-success-fail-complete)

### `play()`

__Starts the audio playback__.

::: tip Formats supported
Currently, quick apps support the following formats: MP4, M4A, MKV, MP3, Ogg, WAV, MPEG-TS and ADTS.
:::

### `pause()`

__Pauses the audio playback__.

### `stop()`

__Stops the audio playback__.


### `getPlayState({success,fail,complete})`

__Gets the current status of the playback.__ 

#### Arguments

This method requires an `object` with the following attributes:
- `attr1` (`string`). Mandatory attribute ....

- `success`	(`function(res)`). Optional callback function for success, with an `object` as argument, that contains the following attributes:
- `state` (`string`). Status of the playback status. The options are `play`, `pause`, and `stop`.
- `src` (`string`). URI of the audio file that is being played. When the playback stops, an empty string is returned.
- `currentTime` (`number`). Audio playback progress, in seconds (`-1` when the playback stops).
- `autoplay` (`boolean`). Flag that indicates if the current audio is automatically played.
- `loop` (`boolean`). Flag that indicates if the current audio is played in loop.
- `volume` (`number`). Audio volume. The default value is the system media volume (from `0.0` to `1.0`).
- `muted` (`boolean`). Flag that indicates whether the audio is muted.
- `notificationVisible` (`boolean`). Flag that indicates whether to display the audio playback status in the notification panel.
- `fail` (`function()`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.


Example:

``` js 
audio.getPlayState({ 
    success: function(data) { 
        console.log(`handling success: state:{data.state},src:${data.src},currentTime:${data.currentTime},autoplay:${data.autoplay},loop:${data.loop}, 
            volume: ${data.volume},muted:${data.muted},notificationVisible:${data.notificationVisible}`) 
    }, 
    fail: function(data, code) { 
        console.log('handling fail, code=' + code) 
    } 
})
```


## Events

This service provides the following event listeners:

- [`onplay(function)`](#onplay-function)
- [`onpause(function)`](#onpause-function)
- [`onloadeddata(function)`](#onloadeddata-function)
- [`onended(function)`](#onended-function)
- [`ondurationchange(function)`](#ondurationchange-function)
- [`onerror(function)`](#onerror-function)
- [`ontimeupdate(function)`](#ontimeupdate-function)
- [`onprevious(function)`](#onprevious-function)
- [`onnext(function)`](#onnext-function)

### `onplay(function)`

__Listener triggered when the audio playback starts but no audio data is loaded yet__.

Example:

``` js 
audio.onplay = function() { 
    console.log("audio onplay"); 
};
```

### `onpause(function)`

__Listener triggered when the audio playback is paused__.


### `onloadeddata(function)`

__Listener triggered once the audio data is loaded playback starts__.

### `onended(function)`

__Listener triggered when the audio playback stops__.

### `ondurationchange(function)`

__Listener triggered when the audio duration value is modified__.

### `onerror(function)`

__Listener triggered when an error happens__.

### `ontimeupdate(function)`

__Listener triggered when the playback progress changes.__ 

The Update is performed every 250 ms.

Example:

``` js 
audio.ontimeupdate = function() { 
    console.log("audio current time:" + audio.currentTime); 
};
```

### `onprevious(function)`

__Listener triggered when the user clicks on the 'previous' button in the notification panel.__ 

### `onnext(function)`

__Listener triggered when the user clicks on the 'next' button in the notification panel.__ 


## Example

``` html
<template> 
    <div class="container"> 
        <div class="nav_title"> 
            <text class="nav_title_detail">Audio Player</text> 
        </div> 
        <div class="item_container"> 
            <image src="/Common/img/play.png" style="width:50px;height:50px;margin-right:50px;" onclick="play"></image> 
            <text>{{time}}</text> 
            <text style="margin-left:6px;margin-right:6px;">/</text> 
            <text style="margin-right:50px;">{{total}}</text> 
            <image src="/Common/img/pause.png" style="width:50px;height:50px;" onclick="pause"></image> 
        </div> 
        <input type="button" style="background-color:#09ba07;margin-bottom:20px;" onclick="autoPlay" value="autoPlay" /> 
        <input type="button" style="background-color:#09ba07;margin-bottom:20px;" onclick="manualPlay" value="manualPlay" /> 
        <input type="button" style="background-color:#09ba07;margin-bottom:20px;" onclick="setMute" value="set muted?" /> 
        <input type="button" style="background-color:#09ba07;margin-bottom:20px;" onclick="seek" value="seek" /> 
    </div> 
</template> 
<style> 
    .item_container { 
        margin-bottom: 50px; 
        margin-right: 60px; 
        margin-left: 60px; 
        flex-direction: row; 
        height: 300px; 
    } 
    .item { 
        height: 250px; 
        text-align: center; 
        color: #ffffff; 
    } 
</style> 
<script> 
    import prompt from '@system.prompt' 
    import audio from '@system.audio' 
    export default { 
        data: { 
            component_name: "audio ksong", 
            time: 0, 
            total: 0, 
        }, 
        autoPlay: function () { 
            var self = this; 
            var srcUri = 'https://example.org/audio.mp3'; 
            audio.src = srcUri; 
            audio.volume = 0.2; 
            var isshow = self.notify; 
            audio.notificationVisible = isshow; 
            audio.autoplay = true; 
            audio.loop = true; 
            audio.onplay = function () { 
                console.log("audio onplay"); 
            }; 
            audio.onpause = function () { 
                console.log("audio onpause"); 
            }; 
            audio.onended = function () { 
                console.log("audio onended"); 
            }; 
            audio.ondurationchange = function () { 
                console.log("audio ondurationchange"); 
                self.total = audio.duration; 
                console.log("audio ondurationchange total=" + self.total); 
            }; 
            audio.ontimeupdate = function () { 
                self.time = audio.currentTime; 
                console.log("audio ontimeupdate time=" + self.time); 
            }; 
        }, 
        manualPlay: function () { 
            var self = this; 
            var s = 'https://od.qingting.fm/m4a/59db80de7cb8914779254b47_8041831_64.m4a'; 
            audio.src = s; 
            audio.play(); 
            audio.onplay = function () { 
                console.log("audio onplay"); 
            }; 
            audio.onpause = function () { 
                console.log("audio onpause"); 
            }; 
            audio.onended = function () { 
                console.log("audio onended"); 
            }; 
            audio.ondurationchange = function () { 
                console.log("audio ondurationchange"); 
                self.total = audio.duration; 
            }; 
            audio.ontimeupdate = function () { 
                console.log("audio ontimeupdate"); 
                self.time = audio.currentTime; 
            }; 
        }, 
        play: function () { 
            audio.play(); 
        }, 
        pause: function () { 
            audio.pause(); 
        }, 
        setMute: function () { 
            var self = this; 
            var isMuted = !self.muted; 
            self.muted = isMuted; 
            audio.muted = isMuted; 
        }, 
        seek: function () { 
            audio.currentTime = '288'; 
        } 
    } 
</script>
```

