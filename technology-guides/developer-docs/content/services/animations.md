# Animations

__Perform animations on content of the app.__

## Declaration

You don't need to declare the use of this service in the [manifest`](../guide/manifest.html) or import it from your scripts, since the animations are part of the CSS modules implemented by the quick app platform.

Before activating an animation on an element, you need to declare the animation options using the method `animate`.

Example:

``` js
const animation = this.$element('my-block').animate(keyframes, options);
```

## Methods

Any element support the following method:

- [`animate(keyframes,options)`](#animate-keyframes-options)
- [`play()`](#play)
- [`finish()`](#finish)
- [`pause()`](#pause)
- [`cancel()`](#cancel)
- [`reverse()`](#reverse)

### `animate(keyframes,options)`

__Define the animation configuration__.

#### Arguments

This method requires two arguments:
- `keyframes` (`array`): List of style `objects` that define the animation steps. These frame objects contains the following members:
  - `width` (`<length-percentage>`): Width of the element after performing the animation.
  - `height` (`<length-percentage>`): Height value applied to the element after after performing the animation.
  - `backgroundColor` (`<color>`): Background color applied to the element after after performing the animation.
  - `opacity` (`number`): Opacity level applied to the element after performing the animation (`0` to `1`). The value by default is `1`.
  - `transformOrigin` (`string`): Position of origin of the element, composed of two strings separated by a comma (`,`). The first value indicates the x-axis position (`left`,`center`, `right`, `<length>`, or `<percentage>`). The second value indicates the y-axis position (`top`, `center`, `bottom`, `<length>`, or `<percentage>`). The value by default is `center, center`.
  - `transform` (`object`): Type of transformation applied to the element in the animation. This `object` may contain the following attributes:
    - `translate/translateX/translateY` (`<length>`): Indicating where the element is placed.
    - `scale/scaleX/scaleY` (`number`): Indicating the scale factor of the element in the transformation.
    - `rotate/rotateX/rotateY` (`<deg-rad>`). Angle of rotation of the element after the transformation.
- `options` (`object`): Options to configure the animation, including the following attributes:
  - `duration` (`number`). Duration of the animation, in milliseconds. The value by default is `0`, indicating that there is no animation effect.
  - `easing` (`string`): Function that indicates the speed of the animation. Possible values are:
    - `linear`: The animation speed keeps unchanged.
    - `ease-in`: The animation starts at a lower speed.
    - `ease-out`: The animation ends at a lower speed.
    - `ease-in-out`: The animation starts and ends at a lower speed, and faster in the middle stages.
    - `cubic-bezier(x1, y1, x2, y2)`: Customize function using the `cubic-bezier()` where `x` and `y` parameters must take values between `0` and `1`.
  - `delay` (`number`). Period of time (in milliseconds) between the animation is requested and it starts. The value by default is `0`, indicating no delay.
  - `iterations` (`number`). Number of times that the animation will be performed. Values are any non-negative integer or `Infinity`.
  - `fill` (`string`): Attribute that indicates if the element needs to be restored to its initial state after the animation is executed. Values are: `none` (default), indicating restoring to the initial state; and `forwards`.

Example:

``` js 
let animation = this.$element('my-block').animate(
    [ 
        { transform: { scaleX: "0.1", scaleY: "0.1" } }, 
        { transform: { scaleX: "1", scaleY: "1" } }
    ],    
    { 
        duration: 15000, 
        easing: 'linear', 
        delay: 0, 
        fill: 'forwards', 
        iterations: 1 
    }
)
```

### `play()`

__Method to start an animation__.

Example:

``` js 
animation.play()
```

### `finish()`

__Method to end an animation__.

Example:

``` js 
animation.finish()
```

### `pause()`

__Method to pause an animation__.

Example:

``` js 
animation.pause()
```

### `cancel()`

__Method to cancel an animation__.

Example:

``` js 
animation.cancel()
```

### `reverse()`

__Method to play an animation in reverse direction__.

Example:

``` js 
animation.reverse()
```

## Events

Animations can trigger events on target elements depending on the performance status, including:

- `cancel`
- `finish`

::: tip
Read more about [animation events](../components/events.html#animation-events).
:::

Example:

``` js 
animation.onfinish = function() { 
    console.log("animation onfinish"); 
} 
animation.oncancel = function() { 
    console.log("animation oncancel"); 
}
```




