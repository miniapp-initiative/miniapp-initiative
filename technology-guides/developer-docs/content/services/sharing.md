# Sharing

__Data sharing with other apps__.

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.share"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import share from '@system.share' 
```

Or

``` js
let share = require("@system.share")
```


## Methods

This service has the following method:

- [`share({type,data,success,fail,cancel,complete})`](#share-type-data-success-fail-cancel-complete)

### `share({type,data,success,fail,cancel,complete})`

__This method enables the app to share data with other apps.__.

#### Arguments

This method requires an `object` with the following attributes:
- `type` (`string`). Mandatory `string` with the MIME type of the data to share (lowercase).
- `data` (`string`). Mandatory `string` with the data to share. The value can be of different types:
  - If the value of `type` is a MIME type starting by `text/` (e.g.,, `text/plain`), `data` contains the textual content to share.
  - Otherwise, `data` must contain the path of the file to be shared. The following file paths are supported:
    - Path of the file downloaded through [`fetch.fetch()`](./fetch.html#fetch);
    - File path obtained through [`file.save()`](./file-storage.html#save) or [`file.list()`](./file-storage.html#list)
    - In-app resource file path starting with a slash character (`/`).
- `success`	(`function`). Optional callback function corresponding to the successful execution.
- `fail` (`function`). Optional callback function corresponding to the failed execution.
- `cancel` (`function`). Optional callback function corresponding to the cancellation of the execution.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.


Example:

``` js 
share.share({ 
  type: "text/html", 
  data: "<b>bold</b>", 
  success: function(data) { console.log("handling success"); }, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
});
```
