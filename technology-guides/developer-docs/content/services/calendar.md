# Calendar Events

__Insert events in the calendar.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.calendar"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import calendar from '@system.calendar' 
```

Or

``` js
let calendar = require("@system.calendar")
```


## Methods

This service has the following method:

- [`insert({title,description,startDate,endDate,timezone,allDay,rrule,remindMinutes,organizer,success,fail})`](#insert-title-description-startdate-enddate-timezone-allday-rrule-remindminutes-organizer-success-fail)



### `insert({title,description,startDate,endDate,timezone,allDay,rrule,remindMinutes,organizer,success,fail})`

__This method inserts a calendar event.__

#### Arguments

This method requires an `object` with the following attributes:
- `title` (`string`). Mandatory attribute with the title of an event.
- `description` (`string`). Optional attribute with the description of the event.
- `startDate` (`number`). Mandatory attribute indicating the start time of an event,represented by the Coordinated Universal Time (in milliseconds) calculated from the beginning of the calendar year.
- `endDate` (`number`). Mandatory attribute indicating the end time of an event, which is represented by the Coordinated Universal Time (in milliseconds), calculated from the beginning of the calendar year.
- `timezone` (`string`). Optional attribute with time zone of the event.
- `allDay` (`boolean`). Optional flag indicating if it is full-day event (defined by the local time zone) (`true`) or if it is an  event with fixed start and end times.
- `rrule` (`string`). Mandatory attribute for recurring events that indicates the [rule format](https://datatracker.ietf.org/doc/html/rfc5545#section-3.8.5.3) of a recurring event (e.g., `FREQ=WEEKLY;COUNT=10;WKST=SU`).
- `remindMinutes` (`array`). Optional attribute to specify when the reminders will be sent, expressed as the number of minutes ahead the event (e.g., `[5,15,30]`).
- `organizer` (`string`). Optional attribute with the email address of an event organizer (owner).
- `success`	(`function(res)`). Optional callback function for success, with the following argument:
  - `res` (`integer`): the identifier of the event created.
- `fail` (`function(code)`). Optional callback function for failure, with the following codes:
  - `201` when the user rejected the request of setting a reminder.
  - `202` when the system received an invalid parameter (e.g., the time format is invalid).
 
Example:

``` js 
calendar.insert({ 
    title: "My Event", 
    startDate: "1490770543000", 
    endDate: "1490880543000", 
    remindMinutes: [5,15,30], 
    rrule: "FREQ=WEEKLY;COUNT=2", 
    success:function(data){console.log("handling success");} 
})
```
