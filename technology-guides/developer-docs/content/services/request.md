# Request 

__Network requests.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.requesttask"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import requesttask from '@system.requesttask' 
```

Or

``` js
let requesttask = require("@system.requesttask")
```


## Methods

This service has the following methods:

- [`request({url,data,header,method,responseType,timeout,success,fail,complete})`](#request-url-data-header-method-responsetype-timeout-success-fail-complete)
- [`abort()`](#abort)

### `request({url,data,header,method,responseType,timeout,success,fail,complete})`

__Method that creates a network request__.

Every time this method is called, a new `object` is returned.

#### Arguments

This method requires an `object` with the following attributes:
- `url` (`string`). Mandatory attribute with the URL of the requested resource.
- `data` (`string`|`object`|`ArrayBuffer`). Optional parameter with the data sent in the call.
- `header` (`object`). Optional parameter with the HTTP header of the request, which includes all the information of the request. Example: `{"Accept-Encoding": "gzip, deflate","Accept-Language": "en-US,fr-FR;q=0.8,en;q=0.6",
- `method` (`string`). Optional attribute with the HTTP method: `OPTIONS`, `GET` (default value), `HEAD`, `POST`, `PUT`, `DELETE`, `TRACE`, and `CONNECT`. 
- `responseType` (`string`). Optional attribute with the type of the response: `text`, `json`, `file`, and `ArrayBuffer`. By default, the response type is determined based on the `Content-Type` in the header returned by the server.
- `timeout` (`number`). Optional attribute that specifies the connection timeout (not the read/write timeout interval) in millisecond  (by default `10000`).
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `statusCode` (`number`). Server status code.
  - `statusText` (`string`). Server status information.
  - `data` (`string`|`object`|`ArrayBuffer`|`json`). Response data.
  - `header` (`object`). HTTP headers in the response from the server. 
- `fail` (`function`). Optional callback function corresponding to the failed execution.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.


Relationship between `data` and the `Content-Type`:

| `data` | `Content-Type` | Description |
| ------ | -------------- | ----------- |
| `string` | (not set) | The default value of `Content-Type` is `text/plain`, and the value of `data` is used as the body of the request. |
| `string` | (Any type) |  The value of `data` is used as the request body. |
| `object` | (not set) | The default value of `Content-Type` is `application/x-www-form-urlencoded`. |
| `object` | `application/x-www-form-urlencoded` | The value of `data` is encoded based on the URL rule and is used as the request body. |
| `object` | (Any type other than `application/x-www-form-urlencoded`) | `data` is converted into a character string as the request body. |
| `ArrayBuffer` | (not set) | The default value of `Content-Type` is `application/octet-stream`, and the value of `data` is used as the request body. |
| `ArrayBuffer` | (any type) | The value of `data` is used as the request body. |


Relationship between `responseType` and `data` in `success`:

| `responseType` | `data` | Description |
| -------------- | ------ | ----------- |
| N/A | `string` | If the value of type in the header returned by the server is `text/*`, `application/json`, `application/javascript`, or `application/xml`, the value is `text`. Otherwise, the value is the URI of a temporary file. |
| `text` | `string` | A text is returned. |
| `json` | `object` | A JavaScript object is returned. |
| `file` | `string` | The URI of a stored temporary file is returned.  | 
| `ArrayBuffer` | `ArrayBuffer` | An ArrayBuffer object is returned. |


Example:

``` js 
this.mytask = requesttask.request({ 
    url: 'https://httpbin.org/anything', 
    method:'POST', 
    data:"12313213213212", 
    success(res) { 
        console.log("Request success. resp = " + JSON.stringify(res)) 
    }, 
    fail(res) { 
        console.log("Request fail. resp = " + JSON.stringify(res)) 
    } 
})
```

### `abort()`

__Method to cancel a request task.__

Example:

``` js 
this.mytask.abort()
```


## Events

This service provides the following listeners:

- [`onProgressUpdate(function({progress,totalBytesSent,totalBytesExpectedToSend}))`](#onprogressupdate-function-progress-totalbytessent-totalbytesexpectedtosend)
- [`offProgressUpdate()`](#offprogressupdate)
- [`onHeadersReceived(function({header}))`](#onheadersreceived-function-header)
- [`offHeadersReceived()`](#offheadersreceived)


### `onProgressUpdate(function({progress,totalBytesSent,totalBytesExpectedToSend}))`

__Method to listen to request progress change events__.

This parameter is valid only for the `POST`, `PUT`, and `CONNECT` methods.

#### Arguments

This method requires a callback `function` with an `object` as argument that has the following members:
- `progress` (`number`). Request sending progress, in percentage.
- `totalBytesSent` (`number`). Size of the sent data, in bytes.
- `totalBytesExpectedToSend` (`number`). Expected size of data to be sent, in bytes.

Example:

``` js 
this.mytask.onProgressUpdate(res => { 
    console.log("Request progress update = " + JSON.stringify(res)) 
})
```

### `offProgressUpdate()`

__Method to cancel a listener of request progress change events__.

Example:

``` js 
this.mytask.offProgressUpdate()
```


### `onHeadersReceived(function({header}))`

__Method to listen to HTTP response header events__.

This method needs to be called before the file request is completed.

#### Arguments

This method requires a callback `function` with an `object` as argument that has the following member:
- `header` (`object`). HTTP response headers returned by the server.

Example:

``` js 
this.mytask.onHeadersReceived(res => { 
    console.log("Request  onHeaderReceived = " + JSON.stringify(res)) 
})
```

### `offHeadersReceived()`

__Method to cancel a listener of HTTP response headers events__.

Example:

``` js 
this.mytask.offHeadersReceived()
```

