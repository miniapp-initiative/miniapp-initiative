# App Context

__Information about the quick app.__


## Manifest Declaration

This service does not need to be declared in the [manifest's `features`](../guide/manifest.html#features) member.


## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import app from '@system.app' 
```

Or

``` js
let app = require("@system.app")
```


## Methods

This service has the following methods:

- [`getInfo()`](#getinfo)
- [`getPackageInfo({packageName,success,fail,complete})`](#getpackageinfo-packagename-success-fail-complete)

### `getInfo()`

__Get information about the current app.__

#### Arguments

This method does not have arguments.

#### Return 

This method returns a JSON object with the following members: 

- `name` (`string`): The name of the current app.
- `versionName`	(`string`):	Version of the app.
- `versionCode`	(`number`):	Version code of the app.
- `logLevel` (`string`): Logging level of the app.
- `source` (`object`): Origin of the app launch. This object includes the following attributes:
  - `packageName` (`string`): Package name of app (e.g., `org.example.myapp`).
  - `type` (`string`): How the app was launched. The options are:
    - `shortcut`: shortcut icon
    - `push`: push notification
    - `url`: web page
    - `barcode`: QR code scanning
    - `nfc`: access through NFC
    - `bluetooth`: access through Bluetooth
    - `other`: others
- `icon` (`string`): Path of the icon configured in the [manifest](../guide/manifest.html#icon) (e.g., `/common/logo.png`).
- `packageName`	(`string`).	Package name of the app.
- `extra` (`object`):  Additional information about the origin, which varies depending on the `type`:
  - if `type` is `shortcut`, values of `extra` could be:
    - `scene`: the shortcut creation scenario. The value can be: `dialog` (created by the internal policy), `api` (created by calling an API), `web` (created when the traffic is switched from an HTML5 page), or other.
    - `original`: original source for shortcut creation.

::: warning
The values of `source` may be `unknown` during the developing and testing phase.
:::

Example:

``` js 
console.log("App getInfo:"+ JSON.stringify(app.getInfo()));
```

With the following output:

``` bash
{ 
    "logLevel": "log",
    "name": "My First Quick App",
    "icon":"/Common/logo.png",
    "packageName":"org.example.myquickapp",
    "source":{"packageName":"unknown","type":"unknown","extra":"{}"},
    "versionName":"1.0.0",
    "versionCode":1
}
```


### `getPackageInfo({packageName,success,fail,complete})`

__Get information about a local app.__

In a quick app scenario, this API gets information about local native apps and quick apps installed in the device. In a widget scenario, this API can obtain only information about a native app. 

#### Arguments

This method requires an `object` with the following attributes:
- `packageName`	(`string`). Mandatory attribute that contains the package name of the quick app or native app.
- `success`	(`function(res)`). Optional callback function for success, with an `object` as argument, that contains the following attributes:
  - `versionName` (`string`): Version of the app.
  - `versionCode` (`number`): Code version of the app.
  - `signatures` (`array<string>`): List of the app signatures, related to the data publisher (encrypted using SHA-256).
- `fail` (`function(code)`). Optional callback function for failure, with the following potential values:
  - `200`: Common error.
  - `202`: Invalid parameter.
  - `1000`: App not installed. 
- `complete` (`function()`). Optional callback function for completion.


Example:

``` js 
<script>
  import app from '@system.app' 
  export default {
    onReady () {
        app.getPackageInfo({
          packageName:'org.example.nativeapp', 
          success(data) {
            console.log(`sucess: ${JSON.stringify(data)}`)
          }, 
          fail(errorMsg, errorCode) {
            console.log(`error: ${errorMsg}`)
          }, 
          complete: () {
            console.log('DONE!')
          },
        })
    }, 
    successHandler
    failHandler
    completeHandler
  }
</script>
```

With the following output:

``` bash
sucess: {"versionName":"1.0.7","versionCode":107,"signatures":["48d5d7956a0a26b4d32sASb55652ae50bbe20ad640df72440bc5ecd0e"]}
DONE!
```

