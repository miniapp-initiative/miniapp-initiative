# Quick App Developer Documents 

This project contains the technical documentation of Quick Apps. The documents are written in markdown and built using __VuePress__ (with Webpack). If you want to test it locally:

``` bash
npm install
npm run dev
``` 

To build the distribution:

``` bash
npm run build
```

The package compile all the documents in `./content` into a static HTML page distribution at `./dist/developers`.

You can configure the structure of the navigation, menu and output directory in the configuration file at `./content/.vuepress/config.js`.

## Get involved

The content is managed by the [OW2 Quick App Initiative](https://quick-app-initiative.ow2.io/) members with the support of its members. 

If you are interested in contributing, [join the Quick App Initiative](https://quick-app-initiative.ow2.io/page/about/#you-are-welcome-to-participate). 