# Technology Guides

Collection of technical documents relating to QAI. These documents should be primarily geared towards developers and people with a technical background.

- **white-paper** Technical whitepapers that have been written around the subject of Quick Apps
