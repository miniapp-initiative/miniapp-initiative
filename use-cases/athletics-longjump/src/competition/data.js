export const athletes = [
    {
      profile: {
        name: "DOE, Elizabeth", bib: "120", country: "za"
      },
      position: 1,
      attemptsArray: [
        {
          attempt: "1",
          measure: "6.34"
        },
        {
          attempt: "2",
          measure: "X"
        },
        {
          attempt: "3",
          measure: "X"
        },
        {
          attempt: "4",
          measure: "6.45"
        },
        {
          attempt: "5",
          measure: "6.22"
        },
        {
          attempt: "6",
          measure: ""
        }
      ],
      numberAttempts: "6",
    },
    {
      profile: {
        name: "JONES, Sarah", bib: "283", country: "at"
      },
      position: 2,
      attemptsArray: [
        {
          attempt: "1",
          measure: "X"
        },
        {
          attempt: "2",
          measure: "5.67"
        },
        {
          attempt: "3",
          measure: "X"
        },
        {
          attempt: "4",
          measure: "6.17"
        },
        {
          attempt: "5",
          measure: "5.98"
        },
        {
          attempt: "6",
          measure: ""
        },            
      ],
      numberAttempts: "6",
    },
    {
      profile: {
        name: "WILLIAMS, Mary", bib: "837", country: "au"
      },
      position: 2,
      attemptsArray: [
        {
          attempt: "1",
          measure: "5.98"
        },
        {
          attempt: "2",
          measure: "6.17"
        },
        {
          attempt: "3",
          measure: "X"
        },
        {
          attempt: "4",
          measure: "6.01"
        },
        {
          attempt: "5",
          measure: "X"
        },
        {
          attempt: "6",
          measure: ""
        },            
      ],
      numberAttempts: "6",
    },
    {
      profile: {
        name: "MILLER, Linda", bib: "553", country: "us"
      },
      position: 4,
      attemptsArray: [
        {
          attempt: "1",
          measure: "6.12"
        },
        {
          attempt: "2",
          measure: "5.97"
        },
        {
          attempt: "3",
          measure: "6.02"
        },
        {
          attempt: "4",
          measure: "6.10"
        },
        {
          attempt: "5",
          measure: ""
        },
        {
          attempt: "6",
          measure: ""
        },            
      ],
      numberAttempts: "6",
    },
    {
      profile: {
        name: "DAVIS, Karen", bib: "238", country: "bh"
      },
      position: 5,
      attemptsArray: [
        {
          attempt: "1",
          measure: "5.45"
        },
        {
          attempt: "2",
          measure: "5.54"
        },
        {
          attempt: "3",
          measure: "5.78"
        },
        {
          attempt: "4",
          measure: "X"
        },
        {
          attempt: "5",
          measure: ""
        },
        {
          attempt: "6",
          measure: ""
        },            
      ],
      numberAttempts: "6",
    },
    {
      profile: {
        name: "GARCIA, Maria", bib: "765", country: "es"
      },
      position: 6,
      attemptsArray: [
        {
          attempt: "1",
          measure: "6.10"
        },
        {
          attempt: "2",
          measure: "5.27"
        },
        {
          attempt: "3",
          measure: "6.05"
        },
        {
          attempt: "4",
          measure: "X"
        },
        {
          attempt: "5",
          measure: ""
        },
        {
          attempt: "6",
          measure: ""
        },            
      ],
      numberAttempts: "6",
    },
    {
      profile: {
        name: "BELOSO, Nancy", bib: "872", country: "br"
      },
      position: 7,
      attemptsArray: [
        {
          attempt: "1",
          measure: "5.34"
        },
        {
          attempt: "2",
          measure: "5.90"
        },
        {
          attempt: "3",
          measure: "6.02"
        },
        {
          attempt: "4",
          measure: "6.03"
        },
        {
          attempt: "5",
          measure: ""
        },
        {
          attempt: "6",
          measure: ""
        },            
      ],
      numberAttempts: "6",
    },
    {
      profile: {
        name: "GONZALES, Carol", bib: "453", country: "co"
      },
      position: 8,
      attemptsArray: [
        {
          attempt: "1",
          measure: "6.09"
        },
        {
          attempt: "2",
          measure: "6.10"
        },
        {
          attempt: "3",
          measure: "X"
        },
        {
          attempt: "4",
          measure: "X"
        },
        {
          attempt: "5",
          measure: ""
        },
        {
          attempt: "6",
          measure: ""
        },            
      ],
      numberAttempts: "6",
    }
];