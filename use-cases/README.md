# Use Cases

Example code projects highlighting different possible use cases for Quick Apps.

- **hello-world** Simple hello world program showing the structure of a Quick App in its most basic form.
- **sample-shop** An example e-commerce style shopping experience
