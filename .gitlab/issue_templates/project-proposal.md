## Introduction

The Quick App Initiative (QAI) aims to foster a healthy and vibrant Quick App ecosystem across different domains through open source collaboration.

QAI activities are organised through thematic _Task Forces_, which might include **deliverable generating actions** such as ecosystem development studies, code projects to enrich and refine Quick App tools and technologies, pilot applications and _proofs of concept_ applications (POCs) that help explore the use of Quick Apps in new domains and use cases.

If you would like to propose a collaborative study, POC or pilot project, please use this template as a guide.

## About you/your organisation (max. 250 words / 3 paragraphs)
- Who are you, what do you do, what are your driving goals?
- What technologies and standards do you use? Is open source on your radar?

## About your proposal

### Describe the project you propose, and why? (max. 125 words / 1-2 paragraphs)

### Describe why? (max. 150 words / 1-2 paragraphs)
- Opportunities for Quick App ecosystem growth
- Opportunities to explore Quick App technologies
- and/or the initiative and its participants?
etc.

### Timeline
  - Project structure (team, tools & meetings)
  - Timeline

### Deliverables
- Will these be open source, or proprietary, or a mix
  - If proprietary, why should the initiative help?
  - If open source/creative commons, do you know already which license(s) you aim to use?
  - **Attention**: As project “owner”, you are responsible for license compliance verifications and actions.

### QAI assistance request
  - Subject experts and/or peer guidance;
  - Developer time;
  - Testers, or designers;
  - Assistance with documentation drafting and translating;
  - Material resources (meeting rooms, cloud space, devices, ...);
  - Financial assistance (see the initiative charter about this);
  - Outreach assistance;
  - etc.

### Your participation
In counter party to QAI assistance, what can you provide to the project?


Attention: All proposals will be stored in GitLab and be public. 
Attention: Proposals will be evaluated by the Steering Committee; the finalised minutes of which are made public.
