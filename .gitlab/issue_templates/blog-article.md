<!-- Title -->
<H2>My blog article</H2>

<!-- Byline -->
By <use "@" to get a dropdown list of participant names, or just type name(s)>

<!-- optional keywords -->
_<div align="right">optional keywords</div>_

<!-- Short Description -->
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris lectus nibh, facilisis nec neque at, eleifend malesuada augue. Ut bibendum accumsan mi in pulvinar. Etiam eu elit dolor. Sed at egestas enim. Duis eleifend maximus lectus. Vestibulum fringilla enim sed eros accumsan consectetur.

---

<!-- Article text (400 words max, including title) -->
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in tincidunt magna, non iaculis neque. Vivamus sit amet elementum orci, et commodo mauris. Suspendisse malesuada enim sit amet semper feugiat. Praesent vitae lorem mattis, tempor mi eu, efficitur tellus. Nam feugiat sapien sit amet sem egestas porta non a nibh. Cras posuere lacus at lectus venenatis mattis. Aliquam varius at ex sed ornare. In hac habitasse platea dictumst. Morbi dapibus enim sit amet volutpat vestibulum. Nunc non nisl est. Fusce sed euismod lorem. Curabitur imperdiet rhoncus nisi, sed blandit justo accumsan ac. In bibendum nec turpis a mollis. Sed pellentesque orci nec vehicula ultrices. Pellentesque sed tellus in sapien lobortis fringilla.

Integer nibh augue, mattis eu bibendum id, commodo in velit. Sed accumsan est a enim consectetur posuere. Pellentesque condimentum lacus ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet venenatis risus, vel porta lorem. Nulla interdum tempor egestas. Nulla nec diam purus. Fusce a sodales dui, ac convallis nulla.

Aliquam ut gravida sem. Ut vel vestibulum ligula, in ullamcorper nisl. Integer at iaculis est, ut dapibus elit. Pellentesque quis elementum lorem, at iaculis lorem. Donec porttitor cursus nunc quis venenatis. Sed pulvinar id augue at volutpat. Vivamus varius neque eu eros laoreet egestas. Vestibulum euismod consequat dui sed venenatis. Maecenas mattis, massa et aliquam suscipit, lorem augue semper nunc, sed sodales ligula sapien eu dolor. Sed sed tristique erat, eget dapibus augue. Donec ultricies ullamcorper dui, ac hendrerit odio ultricies et. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam at leo laoreet ligula porttitor ultrices id eget dui. Curabitur tempor sit amet nunc quis volutpat. Duis mollis fringilla commodo. Vestibulum vehicula feugiat erat, placerat semper ipsum lacinia sit amet.

Vestibulum mauris lorem, bibendum ultricies auctor ullamcorper, maximus ut felis. Aenean velit nulla, rutrum ac cursus et, tristique non sapien. Quisque eget est imperdiet metus interdum venenatis non in tortor. Sed tempor metus at tellus porta, ullamcorper porta ligula faucibus. Integer nisi diam, pellentesque at finibus vel, auctor eget massa. Donec eu gravida lectus. Praesent ornare lorem nec feugiat rutrum. Donec tristique urna risus, eget sollicitudin libero vulputate et. Praesent vel mattis lacus. Mauris consectetur porta lorem id vulputate. Vivamus scelerisque leo in risus rutrum molestie. Vivamus tristique mi quis ipsum egestas, quis tincidunt nibh lobortis. Ut ut est et urna molestie euismod. Nam malesuada massa tortor, eu tempus purus fermentum sit.
