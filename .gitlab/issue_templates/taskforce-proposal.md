The Quick App Initiative structures its activities via thematic _Task Forces_. These are organizational structures that deliver _concrete results_ within a set scope and/or time-frame. Each Task Force may address horizontal topics such as "outreach and promotion", or vertical domainss such as "gaming", or project development such as an "open QA engine". A Task Force may encapsulate multiple actions and outputs.


## < XXX > Task Force ##

_Participants of this Task Force_

| Person | Company | Role (if any) | 
| ------ | ------ | ------ |
| aaa | bbb | Coordinating Representative |
| xxx | yyy | |

## Overview

- **Description:** _general context, broad objectives._
- **Motivation:** _why it is relevant to undertake this activity; what needs does it address?_


## Opportunity

- **Targeted outcomes:** _key activities and/or targeted outcomes/deliverables_
- **Anticipated resources:** _efforts, resources, budget (why & how much) ..._


## Progress Assessment

- **Roadmap:** _high level roadmap and milestones (even if just for next x months)_
- **Journey:** _how will progress be measured?_
- **Achievement:** _is there a clear "end point" and how will it's achievement be deteremined?_


## IPR ##

_Licenses for deliverables (especially software code) visible at time of writing._
- ...


## Recommendations

_Hints and best practices collected from participants._
- Make people happy.
- ...


## References

_References relevant to this Task Force._
* [W3C Web Content Accessibility Guidelines](https://www.w3.org/WAI/standards-guidelines/wcag/): a great resource to achieve this activity.
