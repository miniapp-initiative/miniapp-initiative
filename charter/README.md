# QAI Charter

Files relating to the Quick App Initiative's charter, highlighting the initiatives background, the people involved and the initiatives deliverables.

- **charter.md** The markdown version of the most current and up to date QAI charter

[PDF version of the Charter](https://quick-app-initiative.ow2.io/docs/charter.pdf)
