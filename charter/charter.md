# MiniApp Initiative – Charter


[PDF of the latest Charter version](https://miniapp-initiative.ow2.io/docs/charter.pdf)

##	General Information

### Version History

| Version	| Date | Change |
| --------------- | --------------- | --------------- |
| 1.0 | April 15 2021 | Initial version of the QAI charter added to OW2 code base |
| 1.1 | June 03 2021 | Charter revised to align with an offline version |
| 1.2 | June 04 2021 | Minor updates |
| 1.3 | June 22 2021 | Minor updates |
| 2.0 | November 17 2023 | Initiative name and scope enlarged from _Quick Apps_ to _MiniApps_ |


###	Initiative Name

| Initiative Name	| Initiative Name |
| --------------- | --------------- |
| OW2 MiniApp Initiative | MAI |

###	Initiative Point of Contact

| Data | Value |
| ---- | ----- |
| First Name, Last Name |	Christian, PATERSON |
| Organization | EU-SID CBG, Huawei Europe |
| Telephone	| 00 33 (0)6 70 03 03 47 |
| E-mail address | christian.paterson.ext@huawei.com |
| Remarks |	Christian Paterson is an independent open source consultant assisting Huawei. |

###	Quick Description

The OW2 MiniApp Initiative (also referred to simply as **"Initiative"** in this document) will focus on **MiniApps**, an emerging paradigm of light hybrid applications being defined within the W3C MiniApps Working Group.

## Initiative Purpose

###	Initiative Background

> This Initiative was previously focused on _Quick Apps_; one of the main implementations of MiniApp standards.
> In the spring of 2023 the OW2 Quick App Initiative (QAI) _steering committee_ voted to enlarge the scope of the Initiative to align with the full scope of the W3C MiniApps Working Group. This was done for 2 key reasons:
>   - work on exciting new technologies,
>   - attract more participants.
> 
> To reflect this enlarged scope, it was also agreed to change the name of the _OW2 Quick App Initiative_ (QAI) to _OW2 MiniApp Initiative_ (MAI).

There exist several technologies for developing mobile device application experiences for end-users, and these can be broadly classed under 3 categories:

- **Web Apps**. Applications that run on web browsers, including the classic web applications, _Progressive Web Applications_ (PWA) and other non-standard packaging formats such as kaiOS Apps and Fire OS Web Apps.
- **Native Apps**. Applications that run natively on operating systems. These can be developed using platform-specific SDKs, including the official SDKs, and cross-platform SDKs, enabling developers to release various versions for different operating systems using the same code.
- **Hybrid Applications**. Native applications that takes elements of the above types of apps, using specific system features and relying on an internal WebView engine to deliver content and interact with the user.

**This Initiative is focused on MiniApps**, a new paradigm of light hybrid applications described within Standards being fostered within the W3C. Typically, MiniApps do not require installation and enable quick and easy access.

#### From Quick Apps to MiniApps
In 2018, a group of ten Chinese device makers (Huawei, Gionee, Lenovo, Meizu, Nubia, OnePlus, Oppo, Vivo, Xiaomi, and ZTE) launched the [Quick App Alliance](https://www.quickapp.cn/), a forum to develop Quick Apps as a new platform of light applications for Android devices. The Quick App Alliance develops technology, documentation, IDEs and tools for developers, albeit targeted at the Chinese market. The Quick App Alliance also provides support to companies and individuals that want to adopt this technology.

In 2019, some of the Quick App Alliance members, along with other light app vendors such as Alibaba, Baidu, and Google, launched the World Wide Web Consortium (W3C) [MiniApps Ecosystem Community Group](https://www.w3.org/community/miniapps/), exploring a new homogeneous specification for this new paradigm of _light applications_. One year later, the [W3C MiniApps Ecosystem Community Group](https://www.w3.org/community/miniapps/) proposed creating a working group to define the MiniApp standard as a universal solution to develop light apps. In January 2021, the [W3C MiniApps Working Group](https://www.w3.org/2021/miniapps/) [was created](https://www.w3.org/blog/2021/01/w3c-launches-the-miniapps-working-group/) by consensus.

Quick Apps are a concrete implementation of the abstract [W3C MiniApp specifications](https://www.w3.org/TR/mini-app-white-paper/#what-is-miniapp), allowing light applications in native environments for smart devices.

###	Opportunity

[MiniApp technology](https://www.w3.org/TR/mini-app-white-paper/#what-is-miniapp) is based on the front-end Web technology stack (i.e., CSS, JavaScript and Web APIs), and can also offer advanced access to device features and supports native rendering.

Quick Apps are one way to develop, package and distribute MiniApp compliant applications across platforms, facilitating the process of development through advanced UI components and predefined native APIs (e.g., push notifications, network monitoring, Bluetooth, or camera) that enable developers to design and build usable, reliable and efficient applications rapidly.

#### Quick Apps

Quick Apps are coded using HTML-like markup language, CSS and JavaScript.

When compared to a native Android app, and looking at equivalent functions, Quick Apps require 20% less lines of code – easier for the developer, lighter on download infrastructures, less impact on device storage. The conversion of HTML5 apps into Quick Apps is straightforward and can be done in a short time.

Equally, the process of updating and maintaining Quick Apps is more straightforward than for native apps. Developers may update Quick Apps and deliver new versions to the end-users in a transparent way.

Aligned with emerging MiniApp standards, Quick Apps supports multi-channel distribution, using deep links, marketplaces, web-links, and specific device assistants, maximizing discoverability and app exposure opportunities. Thus, this technology enables app providers to perform innovative marketing activities to promote their services and products.

#### Other MiniApp examples

- __Alipay Mini Program__: Alipay Mini Programs are light applications that run on top of Alipay native app. There are [more than 1 million](https://w3c.github.io/miniapp-white-paper/#miniapp-market) Mini Programs that are running on Alipay native app, with 230 million DAU (Daily Active User). 

- __Baidu Smart Mini Program__: Baidu Smart Mini Program are components that connects people with information and services based on the Baidu app and other partners’ platforms. In July 2019, there were over 150,000 Smart Mini Programs, with 270 million MAU ([source](https://w3c.github.io/miniapp-white-paper/#miniapp-market)).

- __360 PC MiniApp__: 360 PC MiniApp is a light application platform to run MiniApps on PC browsers. 


#### Objectives

This Initiative will be driven by a multidisciplinary group of experts from different organizations and different countries. It will cover a wide range of industries and topics, fostering innovation and entrepreneurship, preserving core values such as sustainability, resilience, user privacy, and ethical use of the technology.

The main objectives of the Initiative are the following:

- **Create an open community**: Any organization or individual may participate in the activities, and become a member of the Initiative, according to participation rules (see [Participation section](#code-of-activity));
- **Multi-stakeholder community**: Public and private organizations, academia, and individuals are invited to enrich the community ecosystem;
- **Vendor-neutral oriented**: The Initiative will be focused on developing tools, documentation, training, use cases, code examples and awareness raising about MiniApps from a vendor-neutral perspective. This regardless of any individual or collective business activities that may be performed by the participants within or without the scope of the Initiative;
- **Not-for-profit association**: The Initiative is established as not-for-profit. However, members may be invited to voluntarily (co)fund and (co)sponsor activities like events, materials, tools development and test-beds where appropriate;
- **Transparent and driven by group’s consensus**: Resolutions of the group and the activities will be based on consensus under the principles of openness and transparency (see [Decision Policy section](#decision-policy));
- **Topic-oriented work**: Initiative participants may propose specific working groups or [Task Forces](#task-forces) to better enable focused collaboration around a specific need or objective. Any new action must be approved by the Initiative [Steering Committee](#steering-committee) and comply with OW2 policies;
- **Open Source advocate**: The Initiative is committed to the Open Source paradigm, fostering the production and release of Free Libre and Open Source code (OSI or FSF approved license), open documentation and open data (Creative Commons license).

The Initiative may explore vertical applications where MiniApps have the potential to be part of a solution, and transversal activities to further core technologies that apply across verticals, including (as appropriate) usability, accessibility, sustainability, privacy, security and open standards.

#### Participants

The Initiative is expected to have a wide variety of participants, opening the participation to any interested organization or individual, including, but not limited to:

- Any organization or individual regardless of geographic location;
- Operating system and device vendors that implement MiniApp technologies, marketplaces and other supporting tools;
- Content and service providers interested in end-user interactions using MiniApps;
- Marketing experts interested in the promotion of MiniApps as a paradigm;
- Developers, including professionals, hobbyists and students, interested in Web and native app technologies;
- Public institutions, including municipalities, with specific needs such as accessible services for citizens and visitors;
- Research centers and academic institutions interested in innovation through agile technologies;
- Innovative entrepreneurs and SMEs.

All the participants in the Initiative agree to be bound by this charter and are required to follow the OW2 governing laws and policies, not least the [OW2 Code of Conduct](https://www.ow2.org/view/Membership_Joining/Code_of_Conduct).

Participants may contribute to any [Task Force](#task-forces) or Project created under the Initiative, and they have no obligation to commit resources. Still, only the active participants will be able to have a representative in the [Steering Committee](#steering-committee) .

###	Alignment

The Initiative aims to be a neutral forum for advancing MiniApp related technologies in alignment with W3C [MiniApp](https://www.w3.org/TR/mini-app-white-paper/#what-is-miniapp) standards. The Initiative’s activities will be driven by real-world challenges, developer needs and business opportunities that maximize the technology's promotion and uptake.

The Initiative will be a place to foster awareness, develop documentation and use cases, collaborate on tooling and coordinate grassroots activities in support of MiniApp implementations. It will enable the active collaboration of different stakeholders, from device vendors to app/content providers, within a neutral, open source based structure.

The Initiative will embrace the ideals of open source transparency, meritocracy and respectful global collaboration.

To ensure the widest possible participation to the Initiative and avoid “pay to play” scenarios, there is no obligation on any individual or organization to participate financially to the Initiative, its [Task Forces](#task-forces), [Projects](#initiative-projects) or activities.

Financial sponsorship of the Initiative and/or significant provision of person time (developers, testers, UI experts, translators ...) and/or other resources will always be gladly received, and publicly recognized by the Initiative (unless instructed otherwise).

##	Initiative Organization

###	Initiative Leadership

| ORGANIZATION | JUSTIFICATION |
| ------------ | ------------- |
| Huawei | Member of the W3C MiniApp standard working group and active promoter of Quick Apps and other MiniApp technologies |

###	Initiative Participants

| ORGANIZATION | CONTRIBUTION AND BENEFITS |
| ------------ | ------------------------- |
| CTIC | Research and innovation center working on smart territories, active aging and well-being, and industrial digital transformation, interested in exploring new MiniApp use. |
| e Foundation | Hosts the core /e/OS project, including /e/OS builds for more than 200 smartphones, source code repository, community websites. |
| Famobi GmbH	| Pioneer in the H5 gaming industry, using the newest cross-platform technology to bring native app-quality to the web. The company has been working with Quick Apps since 2019. |
| FRVR | Successful games development company actively using Quick Apps as a means to distribute applications. |
| ICOL Group |  |
| PrestaShop | An open source e-commerce platform. With nearly 300,000 sites already using its software across the globe, PrestaShop is the leading open source ecommerce solution in Europe and Latin America. |
| Startin'blox | As a low-code solution for Data Spaces, Startin'blox develops interoperable Progressive Web App. We are interested in better mobile integration, and MiniApps are an interesting opportunity to do it. |
| Vonage | An American cloud communications provider (including VoIP services) operating as a subsidiary of Ericsson. |

###	Initiative Prospective Participants

| ORGANIZATION | CONTRIBUTION AND BENEFITS |
| ------------ | ------------------------- |
| none identified at time of writing ||

###	Initiative Management Lead

| Data | Value |
| ---- | ----- |
| First Name, Last Name |	Christian, PATERSON |
| Organization | EU-SID CBG, Huawei Europe |
| Telephone	| 00 33 (0)6 70 03 03 47 |
| E-mail address | christian.paterson.ext@huawei.com |
| Justification	| Respected Open Source actor |

###	Management Team members

| First, Last Name | e-mail	| Organization | Management team role |
| ---------------- | ------ | ------------ | -------------------- |
| Martin, Alvarez Espinar	| martin.alvarez.espinar@huawei.com	| Huawei Europe |	MiniApp W3C liaison + Quick App expert |

##	Initiative Scope

###	OW2 Projects

| OW2 Project |	Justification |
| ----------- | ------------- |
| | No existing OW2 project foreseen to be involved in the Initiative at this stage |
| |	A project submission to the OW2 code base is envisaged. |


###	Non-OW2 Projects
| Other Projects | Project Affiliation	| Organization | Justification | 
| -------------- | -------------------- | ------------ | ------------- |
| Heritage In | QAI / MAI | Martin Alvarez | demo of Quick App and PWA usage for open data|


###	Initiative Deliverable

The Initiative structures its activities in thematic [Task Forces](#task-forces). Each [Task Force](#task-forces) may address horizontal topics such as outreach and promotion, but also vertical joint [Projects](#initiative-projects). Project outputs depend on the specific requirements and needs of the [Task Force](#task-forces) and its participants.

[Task Forces](#task-forces) must be described in a specific space in the online workspace, which also can be used to store or link documents, software code, or other deliverables produced during the [Task Force](#task-forces) activities.

Examples of the expected outcomes are, but not limited to:

- New apps, including public and private services and products;
- Technical documentation and reference materials for developers, including code samples, articles, and technical documents;
- Methodologies and practical recommendations for marketing and development;
- Best practices on usability and accessibility for specific audiences (e.g., general consumers, elderly, rural citizens, vehicle drivers, etc.);
- Samples and source code to enable third-parties to develop, implement and support MiniApps;
- Networking opportunities for stakeholders;
- Technical documents with feedback to standardization bodies, including W3C and Quick App Alliance (i.e., new features, accessibility, multilingual, efficiency, etc.);
- Presentations for promotional and technical events;
- Press releases, position papers, and journal articles;

###	Standards

The [World Wide Web Consortium (W3C)](https://www.w3.org) hosts the [W3C MiniApps Working Group](https://www.w3.org/2021/miniapps/) aiming at harmonizing the heterogeneous MiniApp ecosystem, including platforms like Alipay Mini Apps, Baidu Smart Programs, and Quick Apps. This working group develops specifications and recommendations for enabling interoperability among the different MiniApp platforms, maximizing the convergence of MiniApps and the World Wide Web, reducing the development costs and facilitating the adoption of this technology.

The Initiative will align with the specifications/standards defined by the W3C MiniApps Working Group.

##	Initiative Outlook

###	Timeline

**Start**: 01 June 2021

**End**: no end date

###	Milestones

| Event | Description |
| ----- | ----------- |
| Formal acceptance of initiative creation and this charter by OW2 | During the month of June 2021 |
| Public launch | At OW2con’2021 |
| White paper	| Publication of a white paper explaining Quick Apps and the initiative. The aim is to time this with the initiative launch. |
| Start of 1st [Task Forces](#task-forces) | The initiative will organize deliverables around [Task Forces](#task-forces) (work groups). It is aimed that initial [Task Forces](#task-forces) and their respective rapporteurs/animators will be setup before the summer recess. |
| Scope & name change | Q2-Q3 2023: Enlargement of scope from Quick Apps to MiniApps, with concomitant name change. |

##	Constraints

###	Key Efforts

The main areas of perceived work are:

- Awareness raising within both developer and business communities.
- Exploration and enrichment of MiniApp use cases.
- Creation of technical documentation adapted to western developers.
- Creation of support tools adapted to western developers.
- Coordination with the W3C MiniApps Working Group.


###	Known Risks

There are several identified risks:

- MiniApp technologies (for example Quick Apps) and paradigm are not widely known outside of Asia.
- Little documentation exists in English.
- Tooling (include IDEs) is mainly in Chinese, or not vendor neutral.
- Currently only Xiaomi and Huawei have native MiniApp support via Quick App platforms. Gaining traction with other vendors will be important.
- There exist other technologies that are similar (but not the same) as MiniApps. Notably PWA.

##	IPR Policy

###	Business Rationale

The Initiative aims at facilitating the broadest distribution and use of its outcomes. Deliverables (documents, code) produced by the Initiative should be licensed under the following:

- Software source code: Any OSI approved license (Apache 2.0 is the preference).
- Documentation: A creative commons license (CC-BY 4.0 is the preference)

During the process to create a new [Task Force](#task-forces), the [Task Force](#task-forces) proposition should list the applicable licenses (there may be several) anticipated for its deliverables and describe briefly the rational of their selection (see [section 8.2](#task-forces)). If the target list of licenses associated to a [Task Force](#task-forces) needs to be later modified, a request for change should made to the [Steering Committee](#steering-committee) for validation.

Such communication can be made through the Initiative’s GitLab collaboration space hosted by OW2.

Public Initiative code and document repositories must contain an explicit LICENSE file with information about the license by default.

Neither the Initiative, nor OW2, seek ownership of project related trademarks. These will remain the property of the relevant parties. In the event that a trademark is donated to the Initiative, or the Initiative seeks to create a trademark, this will be organized through OW2 in discussion with the Initiative [Steering Committee](#steering-committee) and any other relevant party.

It is the full and sole responsibility of each  participant to respect Third party IPR and copyright to the full extent of their knowledge and capacity. The Initiative will not be held liable for any copyright or IP infringement made by participants in the course of their participation to the Initiative.

Each contributor will be requested to provide a signed [Developer Certificate of Origin](https://developercertificate.org/) (DCO) prior to contribution into the Initiative’s code base.

###	Licenses

| Licenses | Justification |
| -------- | ------------- |
|  |  |

###	Trademarks

The names “OW2 Quick App Initiative” and "OW2 MiniApp Initiative" remain a trademark of OW2. All other names will remain trademarks of their respective holders.

## Code of activity

###	Steering Committee

The Initiative Steering Committee is the governing body responsible for the strategic decisions of the Initiative. The role of the Steering Committee covers activities such as:

- Decisions about the creation or ending of [Task Forces](#task-forces).
- Decisions about the use of copyright licenses that deviate from the list described in [section 7](#ipr-policy).
- Decisions about the collection and use of funding (cf. [section 8.9](#collaborative-financing))
- Charter and policy enforcement, and dispute resolution.
- Update of this Charter and associated policies.
- Review of active/non-active participant status.
- Decisions to invite Distinguished Individuals to be part of the [Steering Committee](#steering-committee).
- Validation of press releases and white-papers.
- Coordination with the OW2 management office.

The [Steering Committee](#steering-committee)  will be composed of:

- Chairperson.
  - The [Steering Committee](#steering-committee)  will be chaired by one person from an “active” participating organization. This role is in addition to that organization’s Organization Representative.
  - The chairperson is the Initiative’s point of contact for OW2. That person is identified in [section 3.4](#initiative-management-lead) of this document.
  - The (re)selection of the Chairperson will be made by [Steering Committee](#steering-committee) vote (cf. [section 8.7](#decision-policy)).
- Organization Representatives.
  - Each “active” organization may appoint one person as a [Steering Committee](#steering-committee) representative. Subsidiaries of any organization are not counted separately.
- Distinguished Individuals.
  - The [Steering Committee](#steering-committee) may invite Distinguished Individuals to sit on the [Steering Committee](#steering-committee). Such people shall be recognized experts in their field. For example, on Web Technologies, industrial policy, marketing, digital accessibility, etc.
- [Task Force](#task-forces) Representative.
  - Each active [Task Force](#task-forces) will appoint a coordinating representative who will sit on the [Steering Committee](#steering-committee). (Sub) [Projects](#initiative-projects) of any [Task Force](#task-forces) are not counted separately.

The roles of ***Chairperson***, ***Task Force Representative*** and ***Organization Representative*** maybe filled by the same person. This does not change the total number of votes these roles are allocated.

Each [Steering Committee](#steering-committee) member regardless of role, has the same rights and obligations. In the case of formal votes, each member has a single vote. There are 2 exceptions:

- The Initiative Chairperson has a 2nd vote in the case of tie-breaks.
- In the case of [Task Force](#task-forces) decisions, the implicated [Task Force](#task-forces) Representative has 2 votes.

The decision making process is described in [section 8.7](#decision-policy).

No [Steering Committee](#steering-committee)  member will be compensated financially for [Steering Committee](#steering-committee) participation.

###	Task Forces

Activities of the Initiative (e.g., outreach and promotion, test-beds, standardization process, etc.) will be performed within specific Task Forces that have concrete objectives, scopes and timelines and deliverables.

All Task Forces must be submitted for prior validation and approval by the [Steering Committee](#steering-committee).

Participants of the Initiative may propose Task Forces to address specific needs. Task Forces are organizational structures that deliver concrete results within a set scope and/or time-frame. A Task Force often, but not always, encapsulates one or more [Projects](#initiative-projects).

Proposals for new Task Forces will be discussed during [Steering Committee](#steering-committee) meetings.

Task Force proposals must include a brief description of scope, deliverables, anticipated participants, licenses and expected timeline. Once a Task Force is approved by the [Steering Committee](#steering-committee), this information will be made public.

A Task Force will always have a coordinating representative who must be part of the Initiative having agreed to this charter. How this representative is selected is at the discretion of the Task Force and its participants, but the process must be fair, transparent and meritocratic.

The ***Task Force Representative*** will be part of the Initiative’s [Steering Committee](#steering-committee). He/She will have 1 vote for all [Steering Committee](#steering-committee) decisions, except for those relating to his/her Task Force, in which case the representative will have 2 votes (see [section 8.1](#steering-committee)).

###	Initiative Projects

Initiative Projects represent key outputs of the Initiative. An Initiative Project is not an OW2 project, however, an Initiative Project can be submitted, when ready, to the OW2 code base as an OW2 project.

An Initiative Project may deliver code and/or documentation. Project outputs are always delivered under an appropriate F/LOSS license (and/or Creative Commons).

A Project may have contributors and participants that are not part of the Initiative.

A Project will always have a *coordinating representative* who must be part of the Initiative having agreed to this charter. How this representative is selected is at the discretion of the Project and its participants, but the process must be fair, transparent and meritocratic. This coordinating representative shall ensure that the Initiative and OW2 policies are respected.

###	Active/No-Active Standing

Involvement and allocation of resources into the Initiative are optional, and the interests of the participant organizations (or individuals) will determine this.

Only those participants with the status of “Active Standing” may appoint a [Steering Committee](#steering-committee)  representative.

Companies may be assigned either with Active or Non-Active Standing. **All participants will have Active Standing by default**.

As appropriate, the [Steering Committee](#steering-committee)  will review and establish either Active or Non-Active Standing status to participants. A participant organization will be considered “Active Standing” if:

- Its representative takes part in the majority of [Steering Committee](#steering-committee) meetings per year, and/or
- It has at least one representative contributing actively to at least one [Task Force](#task-forces) and/or Project.

###	Coordination

The [Steering Committee](#steering-committee) will make decisions, such as starting/ending [Task Forces](#task-forces), by group consensus.

#### Steering Committee Meetings

The [Steering Committee](#steering-committee) holds periodic meetings that are scheduled four weeks in advance. The Chairperson is responsible for collecting the discussion topics, sharing the final agenda and a reminder of the meeting (at least one day before the appointment), drafting minutes, noting quorum votes.

Extraordinary coordination meetings may be made based on participants’ requests.

#### Task Force Meetings

[Task Forces](#task-forces) may propose their calendar of meetings and organize the work according to their needs and expectations. Most of meetings will focus on discussing particular needs and will be conducted on an as-needed basis.

Task Force Representatives will be responsible for scheduling the meetings, conducting them, delivering notes, and the main action points after the discussion.

###	Communication

Participation to the Initiative is not subject to non-disclosure agreement (NDA).

Minutes will be produced after any official meeting (Steering Committee, [Task Forces](#task-forces), and Projects), reflecting key points, issues and actions raised during the session.
To increase openness of discussion, meetings will generally be conducted using the Chatham House Rule. The following provisions are made:

- Meeting participants will be listed in the minutes.
- Where appropriate, attribution of a statement or action can be made when agreed by meeting participants.
- Where appropriate, different rules may be proposed and adopted before any meeting starts.

([Chatham House Rule](https://www.chathamhouse.org/about-us/chatham-house-rule) means that any meeting participant is free to use information from the discussion but nothing should be done to identify, either explicitly or implicitly, who said what.)

In accordance with this principle, draft minutes will be shared only with meeting participants. Once draft minutes are agreed by consensus (no formal objections are shown) they will be made available publicly.

Information about the different [Task Forces](#task-forces) (including details about deliverables, status, participants and outcomes) will be made available on the public website.

As mentioned in [section 8.9](#collaborative-financing) of this charter, matters relating to finance spending will be made public.

The group will use a participant-only mailing list.

###	Decision Policy

The Initiative will seek to make decisions through consensus. Typically, the chairperson or any participant makes an initial proposal, which is then reviewed and refined. Consensus emerges when there are no strong rejections against the proposal.

####	Consensus

Any resolution taken in a face-to-face meeting or teleconference will be considered provisional to afford asynchronous decisions and organizational deliberation.

A call for consensus will be issued for all resolutions (via email, GitHub issue, or web-based survey), with a response period of at least one week. If no objections are raised by the end of the response period, the resolution will be considered to have reached a consensus as to the resolution of the concrete issue.

####	Formal Votes

In the case the group does not achieve consensus, or in certain cases listed below, the chairperson may call for a formal vote and record the decision along with any objections.

Formal votes may only take place when Quorum is achieved as defined by [OW2 Bylaws](https://www.ow2.org/download/Membership_Joining/Legal_Resources/OW2-BylawsVERSION2017.pdf?rev=1.3) (section III.16). That is to say, 30% of voting members are present.

The following decision mechanism will be applied:

- Unanimity:
  - Charter related.
- Qualified majority (72% of votes):
  - Finance related.
  - Warning or eviction of a participant in relation to repeated or serious violations of code of conduct or other.
- Simple majority:
  - Any other action not listed above, for example:	
    - Public promotion (content, website, flyers, social media).
    - [Task Force](#task-forces) creation/change/closure, including license validation.
    - New chairperson selection.
    - Changes to an organization’s membership standing (cf. [section 8.4](#activeno-active-standing))


###	Public Visibility

The Initiative aims to foster and grow public visibility of the MiniApp technologies through a variety of channels such as events, sponsorship, press-releases, white-papers, articles and social media.

The Initiative is open source and as such, participants are always public knowledge.

Organizations that become part of the Initiative agree that their name and logo may be used in public materials when cited with others as a participant. That is to say, not singled-out. Typically this would be within general promotional materials such as: a web site associated to the initiative, flyers for events, conference displays, etc.

Press-releases must always be validated by any named parties prior to release. If a mutually agreed text is not achieved, the concerned party must be removed from the text.

Presentations slides for events (conferences, webinars, hackathons, seminars, etc.) may mention organizations or individuals if they play a lead role in a Project or [Task Force](#task-forces) that is pertinent to the presentation. Where naming is in “print” the upmost effort will be made to synchronize with the named parties in order to ensure alignment on messaging in respect to the named party and the Project or [Task Force](#task-forces) being mentioned.

###	Collaborative Financing

At the discretion of the Initiative [Steering Committee](#steering-committee) (and under OW2 oversight), the Initiative may coordinate fund raising in order to:

- Provide resources to [Initiative Projects](#initiative-projects) (co-funding).
- Organize or sponsor events, competitions and awards.
- Develop Initiative marketing materials and carry out Initiative communication actions.
- Provide bursaries or grants through independent organizations such as Start-Up Hubs, Competitively Poles and education establishments.
- Co-finance expert assistance and necessary resourcing to the Initiative itself (PMO, out-reach “ambassador(s)” ...)
- Organize and support (within reason) local non-profit MiniApp developer “clubs”.

Equally, Initiative participants and/or interested other parties may at any time voluntarily provide funding into the Initiative’s fund pool.

The use of funds, the decision to co-fund actions, and the choice of projects or actions that will receive funding, will be made only by the [Steering Committee](#steering-committee) under OW2 oversight.

By default all donations will be publicly acknowledged unless a donating organization expressly decides otherwise for a given donation.

All funding actions (spend) will always be made public.

Any and all funds will be managed uniquely through the OW2 management office. This includes reception and payment of funds.

In the event of closure/dissolution of the Initiative, any and all remaining funds will revert to OW2 for use at its sole discretion.

**No participant (individual or organization) will be held responsible in any way for fund management. Legal and fiduciary oversight of finances and funding will be the sole responsibility of OW2.**

