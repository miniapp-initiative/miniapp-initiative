# QAI Launch Participant: **Huawei Europe**

<!-- LOGO HERE -->
[![Huawei EU logo](https://huawei.eu/themes/custom/huawei_digitall/logo.svg)](https://huawei.eu/)

Huawei Europe is a member of the [W3C MiniApp Working Group](https://www.w3.org/2021/miniapps/) and actively promotes Quick Apps.
Huawei is proud to lead the launch of the [Quick App initiative](https://quick-app-initiative.ow2.io/) since June 2021.

<!-- additional text here -->
# Active support for open source globally

Globally, Huawei actively sponsors and participates in various leading open source organizations (Linux Foundation, Eclipse Foundation, Apache Software Foundation, OpenStack Foundation, OW2 ...). We share our technological experience with the members of these organizations to promote open innovation, and are a top contributor in technologies such as OpenStack, the Linux Kernel, and CNCF (where Huawei Cloud is a founding and platinum member). Huawei has made the most contributions to the Kubernetes community, and is one of the first Kubernetes Certified Service Providers (KSCPs). Huawei has also initiated more than 10 key Open Source projects and been a founding member of over 8 major projects (example: OpenEuler, LF AI ...).

# Active support for research in Europe
Since 2014, Huawei has directly financed around €14m of EU research through participation in 25 EU-led projects in FP7 and Horizon 2020. We have established 230 technical cooperation contracts with our entities in Finland, Germany, Ireland, and Belgium and partnered with over 150 European universities and research institutes. Huawei is committed to supporting the roll-out of Horizon Europe objectives, via engagement in various ICT Horizon Europe calls for proposals.

Since 2020, Huawei has committed to investing over €90bn in research worldwide, almost three times the current annual budget of the European Space Agency. Our efforts will focus on advanced technologies through Huawei's 23 research facilities with over 2200 research engineers in 14 European countries creating jobs and helping local economies. These technologies are for European industry and will help Europe strengthen its advantage in these areas. For example, in France our research focuses on parallel algorithms, network AI, image processing chipset, and sensor applications.

# Active support for open source in Europe
Huawei advocates for open governance in hardware and software technologies, including mobile operating systems with transparency and community collaboration that builds greater trust for today's digital society. Today we are spearheading the new OW2 initiative to promote and grow the Quick App ecosystem in Europe; this will be a benefit to European business, developers and users.

<!-- why be part of QAI, or do you lead a Task Force or Project, etc. -->
Did you know that Quick Apps have been developed since 2018; before the unilateral measures of the previous US administration. This is now an opportunity for healthier competition in the mobile application ecosystem for the industry and for society as a whole. As the world becomes more connected we must grow together to bring digital to all and leave no one behind. Any new app dynamic that can help liberate developers and app users is a clear benefit to all.

<!-- Quick App showcase (if pertinent): no more than 2 apps -->
<!-- Write a short introduction to your 1 or 2 apps. Explain why you showcase them -->
<!-- For each app provide image, name, where people find it,  link to YouTube video if available + 1 sentence to explain why it is interesting -->
<!-- Above all, BE REASONABLE, this is not a marketing website ! -->

<!-- Standardized organisation information -->

Country of incorporation/registration: Brussels, Belgium (HQ in Shenzhen, China)

Type of organisation: Multi-national enterprise <!-- choose from: public service, non-profit, SME, large enterprise, multi-national, other (specify) -->

Domain(s) of activity: Technology Innovation, Telecommunications, Smart Devices <!-- short list/keywords here -->

Website: https://huawei.eu/ <!-- website URL; normally the same as the logo click target -->

QAI representative:
* Christian PATERSON (initiative chairperson)
* Martin ALVAREZ-ESPINAR (W3C Liaison for the initiative)


<!-- any other contact information that you would like to show -->
